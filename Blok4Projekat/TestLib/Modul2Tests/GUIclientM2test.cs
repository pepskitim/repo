﻿using CommonLibM2;
using CommonModulLib;
using CommonModulLib.DataModel;
using GUIclientM2;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace TestLib
{
    [TestFixture]
    [Apartment(ApartmentState.STA)]
    public class GUIclientM2test
    {
        private WCFclientM2 clientProxyTest;
        private DateTime dateOfBirth = new DateTime(1994, 4, 4);
        private List<string> termList = new List<string>() { "t", "t2" };
        private MemberPackageRel memberPackRel = new MemberPackageRel();
        private MemberPackageRel memberPackRel2 = new MemberPackageRel();
        private FitnessCenterManager fcManager = new FitnessCenterManager("a", "a", "a", "a", "a", "a");
        private FitnessCenterManager fcManager2 = new FitnessCenterManager("a2", "a2", "a2", "a2", "a2", "a2");
        private Coach coach = new Coach("a", "a", "a", "a", "a", "a", new DateTime(1, 1, 1), "a");
        private Coach coach2 = new Coach("a1", "a1", "a1", "a1", "a1", "a1", new DateTime(1, 1, 1), "a1");
        private Member member = new Member("a", "a", "a", "a", "a", 200, 200, new DateTime(1, 1, 1));
        private Member member2 = new Member("a1", "a1", "a1", "a1", "a1", 200, 200, new DateTime(1, 1, 1));
        private TrainingPackage trainingPackage = new TrainingPackage(new List<string>() { "t", "t2" }, 120, "package");
        private TrainingPackage trainingPackage2 = new TrainingPackage(new List<string>() { "t", "t2" }, 120, "package2");
        private Time time = new Time(1, 0, 2, 0);
        private Term term = new Term("t", "t", new Time(1, 0, 2, 0), "t", Term.TrainingLevel.BEGGINER, "t", 1);
        private Term term2 = new Term("t1", "t1", new Time(1, 0, 2, 0), "t", Term.TrainingLevel.BEGGINER, "t1", 2);

        private MemberPackageRel mpRel = new MemberPackageRel();
        private MemberPackageRel mpRel2 = new MemberPackageRel();
        private List<MemberPackageRel> mpRels = new List<MemberPackageRel>();

        private MemberTermRel mtRel = new MemberTermRel();
        private MemberTermRel mtRel2 = new MemberTermRel();
        private List<MemberTermRel> mtRels = new List<MemberTermRel>();


        private CanceledTraining canceledTraining = new CanceledTraining();
        private CanceledTraining canceledTrainingBad = new CanceledTraining();

        private IModul1Contract proxyForM1;

        [OneTimeSetUp]
        public void SetupTest()
        {
            clientProxyTest = new WCFclientM2();

            clientProxyTest.factory = Substitute.For<IdbContract>();

            ConnectClassM2.clientProxy = Substitute.For<IdbContract>();
            proxyForM1 = Substitute.For<IModul1Contract>();

            mpRels.Add(mpRel);
            mpRels.Add(mpRel2);

            mtRels.Add(mtRel);
            mtRels.Add(mtRel2);

            clientProxyTest.factory.Login("coach", "coach").Returns(AuthenticationAndAuthorization.COACH);
            clientProxyTest.factory.Login("fm", "fm").Returns(AuthenticationAndAuthorization.FITNESS_CENTER_MANAGER);
            clientProxyTest.factory.Login("member", "member").Returns(AuthenticationAndAuthorization.MEMBER);
            clientProxyTest.factory.Login("wrong", "wrong").Returns(AuthenticationAndAuthorization.NONE);

            clientProxyTest.factory.AddNewCoach("coach", "coach", "coach", "coach", "coach", "coach", dateOfBirth, "coach").Returns(true);
            clientProxyTest.factory.AddNewCoach("coach1", "coach1", "coach1", "coach1", "coach1", "coach1", dateOfBirth, "coach1").Returns(false);

            clientProxyTest.factory.AddNewMember("member", "member", "member", "member", "member", 100, 100, dateOfBirth).Returns(true);
            clientProxyTest.factory.AddNewMember("member1", "member1", "member1", "member1", "member1", 100, 100, dateOfBirth).Returns(false);

            clientProxyTest.factory.AddNewTerm("term", "term", 1, 0, 2, 0, "term", CommonModulLib.DataModel.Term.TrainingLevel.BEGGINER, "term", 100).Returns(true);
            clientProxyTest.factory.AddNewTerm("term1", "term1", 1, 0, 2, 0, "term1", CommonModulLib.DataModel.Term.TrainingLevel.BEGGINER, "term", 100).Returns(false);

            clientProxyTest.factory.AddNewTrainingPackage(termList, 100, "p1").Returns(true);
            clientProxyTest.factory.AddNewTrainingPackage(termList, 120, "p2").Returns(false);

            clientProxyTest.factory.DeleteCoach("coach").Returns(true);
            clientProxyTest.factory.DeleteCoach("coach2").Returns(false);

            clientProxyTest.factory.DeleteMember("member").Returns(true);
            clientProxyTest.factory.DeleteMember("member2").Returns(false);

            clientProxyTest.factory.DeleteTerm("title").Returns(true);
            clientProxyTest.factory.DeleteTerm("title2").Returns(false);

            clientProxyTest.factory.DeletePackage(1).Returns(true);
            clientProxyTest.factory.DeletePackage(2).Returns(false);

            clientProxyTest.factory.EditPersonalFCMdata(fcManager).Returns(true);
            clientProxyTest.factory.EditPersonalFCMdata(fcManager2).Returns(false);

            clientProxyTest.factory.GetAllCoaches().Returns(new List<Coach>() { new Coach("tr1", "tr1", "tr1", "tr1", "tr1", "tr1", dateOfBirth, "fc") });

            clientProxyTest.factory.GetAllFitnessCenterByManagerName("coach").Returns(new List<string> { "fc", "fc2", "fc3" });

            clientProxyTest.factory.GetAllFitnessCenters().Returns(new List<FitnessCenter>() {
                new FitnessCenter("fc", "fc", "fc", 3, "man", time),
                new FitnessCenter("fc1", "fc1", "fc1", 3, "man", time) });

            clientProxyTest.factory.GetAllHallIds("fc").Returns(new List<string>() { "12", "13" });

            clientProxyTest.factory.GetAllHalls("fc").Returns(new List<Hall>() {
                new Hall("id", TypeOfHall.GYM, "fc"),
                new Hall("id2", TypeOfHall.YOGA, "fc") });

            clientProxyTest.factory.GetAllPackageTerms("package").Returns(new List<string> { "term1", "term2", "term3" });

            clientProxyTest.factory.GetAllTerms().Returns(new List<Term>() { new Term("term1", "Tuesday", time, "sala1", Term.TrainingLevel.ADVANCED, "tr1", 1000),
                                                                             new Term("term2", "Tuesday", time, "sala2", Term.TrainingLevel.ADVANCED, "tr1", 1000) });

            clientProxyTest.factory.GetAllTraininingPackages().Returns(new List<TrainingPackage>() { new TrainingPackage(termList, 1000, "package"),
                                                                                                     new TrainingPackage(termList, 1500, "package2") });

            clientProxyTest.factory.GetAllTraininingPackagesForManagerUsername("manager").Returns(new List<TrainingPackage>() { new TrainingPackage(termList, 1000, "package"),
                                                                                                     new TrainingPackage(termList, 1500, "package2") });

            clientProxyTest.factory.GetCoach("coach").Returns(new Coach("tr1", "tr1", "tr1", "tr1", "tr1", "tr1", dateOfBirth, "fc"));

            clientProxyTest.factory.GetFitnessCenterManagerByUserName("man").Returns(new FitnessCenterManager("man", "man", "man", "man", "man", "123123123"));

            clientProxyTest.factory.GetMembersForGivenPackage("package").Returns(new List<Member>() { new Member("mem", "mem", "mem", "mem", "mem", 200, 200, dateOfBirth),
                                                                                                      new Member("mem2", "mem2", "mem2", "mem2", "mem2", 200, 200, dateOfBirth) });

            proxyForM1.ConnectToModul1("fc", "192.168.1.1").Returns(true);
            proxyForM1.ConnectToModul1("fc2", "192.168.1.1").Returns(false);

            clientProxyTest.factory.UpdateCoach(coach).Returns(true);
            clientProxyTest.factory.UpdateCoach(coach2).Returns(false);

            clientProxyTest.factory.UpdateMember(member).Returns(true);
            clientProxyTest.factory.UpdateMember(member2).Returns(false);

            clientProxyTest.factory.UpdatePackage(trainingPackage).Returns(true);
            clientProxyTest.factory.UpdatePackage(trainingPackage2).Returns(false);

            clientProxyTest.factory.UpdateTerm(term).Returns(true);
            clientProxyTest.factory.UpdateTerm(term2).Returns(false);

            //ConnectClassM2.clientProxy.GetTermsByCocachUsername(string.Empty).Returns(new List<Term>());
            ConnectClassM2.clientProxy.GetAllTerms().Returns(new List<Term>());
            ConnectClassM2.clientProxy.GetFitnessCenterManagerByUserName(string.Empty).Returns(new FitnessCenterManager());
            ConnectClassM2.clientProxy.GetAllTraininingPackagesForManagerUsername(string.Empty).Returns(new List<TrainingPackage>());
            ConnectClassM2.clientProxy.GetNotificationsForMember(string.Empty).Returns(new List<string>());
            ConnectClassM2.clientProxy.GetCoach(string.Empty).Returns(new Coach());
            ConnectClassM2.clientProxy.GetAllHallIds(string.Empty).Returns(new List<string>());
            ConnectClassM2.clientProxy.GetAllCoaches().Returns(new List<Coach>());
            ConnectClassM2.clientProxy.GetAllMTRels().Returns(new List<MemberTermRel>());
            ConnectClassM2.clientProxy.GetAllTraininingPackages().Returns(new List<TrainingPackage>());
            ConnectClassM2.clientProxy.GetAllMPRels().Returns(new List<MemberPackageRel>());
            ConnectClassM2.clientProxy.GetCoach(null).Returns(new Coach());

            clientProxyTest.factory.GetAllMPRels().Returns(mpRels);
            clientProxyTest.factory.GetAllMTRels().Returns(mtRels);

            //clientProxyTest.factory.GetMembersForGivenPackage("package").Returns(new List<Member>(){
            //    new Member("a", "a", "a", "a", "a", 200, 200, dateOfBirth),
            //    new Member("a1", "a1", "a1", "a1", "a1", 200, 200, dateOfBirth)});

            //clientProxyTest.factory.GetMembersForGivenTerm("term").Returns(new List<Member>(){
            //      new Member("a", "a", "a", "a", "a", 200, 200, dateOfBirth),
            //    new Member("a1", "a1", "a1", "a1", "a1", 200, 200, dateOfBirth)});

            clientProxyTest.LoginFitnessCenterManager("a", "a").Returns(true);
            clientProxyTest.LoginFitnessCenterManager("b", "b").Returns(false);

            clientProxyTest.GetTermsByCocachUsername("coach").Returns(new List<Term>() { new Term("term1", "Tuesday", time, "sala1", Term.TrainingLevel.ADVANCED, "tr1", 1000),
                                                                                         new Term("term2", "Tuesday", time, "sala2", Term.TrainingLevel.ADVANCED, "tr1", 1000) });

            clientProxyTest.GetTermsByManagerUsername("manager").Returns(new List<Term>() { new Term("term1", "Tuesday", time, "sala1", Term.TrainingLevel.ADVANCED, "tr1", 1000),
                                                                                             new Term("term2", "Tuesday", time, "sala2", Term.TrainingLevel.ADVANCED, "tr1", 1000) });

            clientProxyTest.GetTermsForGivenMember("member").Returns(new List<Term>() { new Term("term1", "Tuesday", time, "sala1", Term.TrainingLevel.ADVANCED, "tr1", 1000),
                                                                                             new Term("term2", "Tuesday", time, "sala2", Term.TrainingLevel.ADVANCED, "tr1", 1000) });

            clientProxyTest.PayMPRel("mem", "pack").Returns(true);
            clientProxyTest.PayMPRel("mem2", "pack2").Returns(false);

            clientProxyTest.PayMTRel("mem", "term").Returns(true);
            clientProxyTest.PayMTRel("mem2", "term2").Returns(false);

            clientProxyTest.SetCanceledTraining(canceledTraining).Returns(true);
            clientProxyTest.SetCanceledTraining(canceledTrainingBad).Returns(false);

            clientProxyTest.GetNotificationsForMember("member").Returns(new List<string>() { "notif", "notif1" });

            clientProxyTest.GetTerm("term1").Returns(new Term("term1", "Tuesday", time, "sala1", Term.TrainingLevel.ADVANCED, "tr1", 1000));

            clientProxyTest.SetMPRel(mpRel).Returns(true);
            clientProxyTest.SetMPRel(mpRel2).Returns(false);

            clientProxyTest.SetMTRel(mtRel).Returns(true);
            clientProxyTest.SetMTRel(mtRel2).Returns(false);

            //clientProxyTest.GetPackagesForGivenMember("member").Returns(new List<TrainingPackage>() { new TrainingPackage(termList, 1000, "package"),
            //                                                                                          new TrainingPackage(termList, 1500, "package2")});

            clientProxyTest.DeleteMPRel(mpRel).Returns(true);
            clientProxyTest.DeleteMPRel(mpRel2).Returns(false);

            clientProxyTest.DeleteMTRel(mtRel).Returns(true);
            clientProxyTest.DeleteMTRel(mtRel2).Returns(false);

            clientProxyTest.NotifyMembersForPackageChange("package", "notification").Returns(true);
            clientProxyTest.NotifyMembersForPackageChange("package2", "notification2").Returns(false);

            clientProxyTest.NotifyMembersForTermChange("term", "notification").Returns(true);
            clientProxyTest.NotifyMembersForTermChange("term2", "notification2").Returns(false);
        }

        [Test]
        public void LoginCoachOK()
        {
            AuthenticationAndAuthorization res = clientProxyTest.Login("coach", "coach");
            Assert.AreEqual(res, AuthenticationAndAuthorization.COACH);
        }

        [Test]
        public void LoginFCManagerManagerOK()
        {
            AuthenticationAndAuthorization res = clientProxyTest.Login("fm", "fm");
            Assert.AreEqual(res, AuthenticationAndAuthorization.FITNESS_CENTER_MANAGER);
        }

        [Test]
        public void LoginMemberOK()
        {
            AuthenticationAndAuthorization res = clientProxyTest.Login("member", "member");
            Assert.AreEqual(res, AuthenticationAndAuthorization.MEMBER);
        }

        [Test]
        public void LoginFail()
        {
            AuthenticationAndAuthorization res = clientProxyTest.Login("wrong", "wrong");
            Assert.AreEqual(res, AuthenticationAndAuthorization.NONE);
        }

        [Test]
        public void AddNewCoachOK()
        {
            bool res = clientProxyTest.AddNewCoach("coach", "coach", "coach", "coach", "coach", "coach", dateOfBirth, "coach");
            Assert.AreEqual(true, res);
        }

        [Test]
        public void AddNewCoachFail()
        {
            bool res = clientProxyTest.AddNewCoach("coach1", "coach1", "coach1", "coach1", "coach1", "coach1", dateOfBirth, "coach1");
            Assert.AreEqual(false, res);
        }

        [Test]
        public void AddNewMemberOK()
        {
            bool res = clientProxyTest.AddNewMember("member", "member", "member", "member", "member", 100, 100, dateOfBirth);
            Assert.AreEqual(true, res);
        }

        [Test]
        public void AddNewMemberFail()
        {
            bool res = clientProxyTest.AddNewMember("member1", "member1", "member1", "member1", "member1", 100, 100, dateOfBirth);
            Assert.AreEqual(false, res);
        }

        [Test]
        public void AddNewTermOK()
        {
            bool res = clientProxyTest.AddNewTerm("term", "term", 1, 0, 2, 0, "term", CommonModulLib.DataModel.Term.TrainingLevel.BEGGINER, "term", 100);
            Assert.AreEqual(true, res);
        }

        [Test]
        public void AddNewTermFail()
        {
            bool res = clientProxyTest.AddNewTerm("term1", "term1", 1, 0, 2, 0, "term1", CommonModulLib.DataModel.Term.TrainingLevel.BEGGINER, "term", 100);
            Assert.AreEqual(false, res);
        }

        [Test]
        public void AddNewTPackageOK()
        {
            bool res = clientProxyTest.AddNewTrainingPackage(termList, 100, "p1");
            Assert.AreEqual(true, res);
        }

        [Test]
        public void AddNewTPackageFail()
        {
            bool res = clientProxyTest.AddNewTrainingPackage(termList, 120, "p2");
            Assert.AreEqual(false, res);
        }

        [Test]
        public void DeleteCoachTestOk()
        {
            bool result = clientProxyTest.DeleteCoach("coach");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void DeleteCoachTestFail()
        {
            bool result = clientProxyTest.DeleteCoach("coach2");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void DeleteMemberTestOk()
        {
            bool result = clientProxyTest.DeleteMember("member");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void DeleteMemberTestFail()
        {
            bool result = clientProxyTest.DeleteMember("member2");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void DeleteTermTestOk()
        {
            bool result = clientProxyTest.DeleteTerm("title");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void DeleteTermTestFail()
        {
            bool result = clientProxyTest.DeleteTerm("title2");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void DeletePackageOk()
        {
            bool result = clientProxyTest.DeletePackage(1);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void DeletePackageFail()
        {
            bool result = clientProxyTest.DeletePackage(2);
            Assert.AreEqual(false, result);
        }

        //[Test]
        //public void DeleteMemberPackRelOk()
        //{
        //    bool result = clientProxyTest.DeleteMPRel(memberPackRel);
        //    Assert.AreEqual(true, result);
        //}

        //[Test]
        //public void DeleteMemberPackRelFail()
        //{
        //    bool result = clientProxyTest.DeleteMPRel(memberPackRel2);
        //    Assert.AreEqual(false, result);
        //}

        [Test]
        public void EditFCManagerDataOk()
        {
            bool result = clientProxyTest.EditPersonalFCMdata(fcManager);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void EditFCManagerDataFail()
        {
            bool result = clientProxyTest.EditPersonalFCMdata(fcManager2);
            Assert.AreEqual(false, result);
        }

        [Test]
        public void GetAllCoachesTest()
        {
            List<Coach> result = clientProxyTest.GetAllCoaches();
            Assert.AreEqual("tr1", result[0].Username);
            Assert.AreEqual("tr1", result[0].Password);
            Assert.AreEqual("tr1", result[0].FirstName);
            Assert.AreEqual("tr1", result[0].LastName);
            Assert.AreEqual("tr1", result[0].Address);
            Assert.AreEqual("tr1", result[0].UniversityDegree);
            Assert.AreEqual(dateOfBirth, result[0].DateOfBirth);
            Assert.AreEqual("fc", result[0].FitnessCenter);
        }

        [Test]
        public void GetAllFitnessCenterByManagerNameTest()
        {
            List<string> result = clientProxyTest.GetAllFitnessCenterByManagerName("coach");
            Assert.AreEqual("fc", result[0]);
            Assert.AreEqual("fc2", result[1]);
            Assert.AreEqual("fc3", result[2]);
        }

        [Test]
        public void GetFitnessCentersTest()
        {
            List<FitnessCenter> result = clientProxyTest.GetAllFitnessCenters();
            Assert.AreEqual("fc", result[0].Name);
            Assert.AreEqual("fc", result[0].Address);
            Assert.AreEqual("fc", result[0].Telephone);
            Assert.AreEqual(3, result[0].NumOfHalls);
            Assert.AreEqual(time, result[0].WorkingTime);
            Assert.AreEqual("man", result[0].FcManagerUsername);

            Assert.AreEqual("fc1", result[1].Name);
            Assert.AreEqual("fc1", result[1].Address);
            Assert.AreEqual("fc1", result[1].Telephone);
            Assert.AreEqual(3, result[0].NumOfHalls);
            Assert.AreEqual(time, result[1].WorkingTime);
            Assert.AreEqual("man", result[1].FcManagerUsername);
        }

        [Test]
        public void GetAllHallIdsTest()
        {
            List<string> result = clientProxyTest.GetAllHallIds("fc");
            Assert.AreEqual("12", result[0]);
            Assert.AreEqual("13", result[1]);
        }

        [Test]
        public void GetAllHalsTest()
        {
            List<Hall> result = clientProxyTest.GetAllHalls("fc");
            Assert.AreEqual("id", result[0].Id);
            Assert.AreEqual(TypeOfHall.GYM, result[0].Type);
            Assert.AreEqual("fc", result[0].FitnessCenterName);
            Assert.AreEqual("id2", result[1].Id);
            Assert.AreEqual(TypeOfHall.YOGA, result[1].Type);
            Assert.AreEqual("fc", result[1].FitnessCenterName);
        }

        [Test]
        public void GetAllPackageTermsTest()
        {
            List<string> result = clientProxyTest.GetAllPackageTerms("package");
            Assert.AreEqual("term1", result[0]);
            Assert.AreEqual("term2", result[1]);
            Assert.AreEqual("term3", result[2]);
        }

        [Test]
        public void GetAllTermsTest()
        {
            List<Term> result = clientProxyTest.GetAllTerms();
            Assert.AreEqual("term1", result[0].Title);
            Assert.AreEqual("Tuesday", result[0].Day);
            Assert.AreEqual("sala1", result[0].ApointedHall);
            Assert.AreEqual(time, result[0].TimeRange);
            Assert.AreEqual(Term.TrainingLevel.ADVANCED, result[0].TrainingLvl);
            Assert.AreEqual("tr1", result[0].ApointedCoach);

            Assert.AreEqual("term2", result[1].Title);
            Assert.AreEqual("Tuesday", result[1].Day);
            Assert.AreEqual("sala2", result[1].ApointedHall);
            Assert.AreEqual(time, result[1].TimeRange);
            Assert.AreEqual(Term.TrainingLevel.ADVANCED, result[1].TrainingLvl);
            Assert.AreEqual("tr1", result[1].ApointedCoach);
        }

        [Test]
        public void GetAllTraininingPackagesTest()
        {
            List<TrainingPackage> result = clientProxyTest.GetAllTraininingPackages();
            Assert.AreEqual(termList, result[0].Terms);
            Assert.AreEqual(1000, result[0].MonthPrice);
            Assert.AreEqual("package", result[0].PackageName);

            Assert.AreEqual(termList, result[1].Terms);
            Assert.AreEqual(1500, result[1].MonthPrice);
            Assert.AreEqual("package2", result[1].PackageName);
        }

        [Test]
        public void GetAllTPForManagerUsernameTest() 
        {
            List<TrainingPackage> result = clientProxyTest.GetAllTraininingPackagesForManagerUsername("manager");
            Assert.AreEqual(termList, result[0].Terms);
            Assert.AreEqual(1000, result[0].MonthPrice);
            Assert.AreEqual("package", result[0].PackageName);

            Assert.AreEqual(termList, result[1].Terms);
            Assert.AreEqual(1500, result[1].MonthPrice);
            Assert.AreEqual("package2", result[1].PackageName);
        }

        [Test]
        public void GetCoachTest()
        {
            Coach result = clientProxyTest.GetCoach("coach");
            Assert.AreEqual("tr1", result.Username);
            Assert.AreEqual("tr1", result.Password);
            Assert.AreEqual("tr1", result.FirstName);
            Assert.AreEqual("tr1", result.LastName);
            Assert.AreEqual("tr1", result.Address);
            Assert.AreEqual("tr1", result.UniversityDegree);
            Assert.AreEqual(dateOfBirth, result.DateOfBirth);
            Assert.AreEqual("fc", result.FitnessCenter);
        }

        [Test]
        public void GetFCMByUserNameTest()
        {
            FitnessCenterManager result = clientProxyTest.GetFitnessCenterManagerByUserName("man");
            Assert.AreEqual("man", result.Username);
            Assert.AreEqual("man", result.Password);
            Assert.AreEqual("man", result.FirstName);
            Assert.AreEqual("man", result.LastName);
            Assert.AreEqual("man", result.Address);
            Assert.AreEqual("123123123", result.Telephone);
        }

        //[Test]
        //public void GetMembersForGivenPackageTest() 
        //{
        //    List<Member> result = clientProxyTest.GetMembersForGivenPackage("package");
        //    Assert.AreEqual("mem", result[0].Username);
        //    Assert.AreEqual("mem", result[0].Password);
        //    Assert.AreEqual("mem", result[0].FirstName);
        //    Assert.AreEqual("mem", result[0].LastName);
        //    Assert.AreEqual("mem", result[0].Address);
        //    Assert.AreEqual(200, result[0].Weight);
        //    Assert.AreEqual(200, result[0].Height);
        //    Assert.AreEqual(dateOfBirth, result[0].DateOfBirth);

        //    Assert.AreEqual("mem2", result[0].Username);
        //    Assert.AreEqual("mem2", result[0].Password);
        //    Assert.AreEqual("mem2", result[0].FirstName);
        //    Assert.AreEqual("mem2", result[0].LastName);
        //    Assert.AreEqual("mem2", result[0].Address);
        //    Assert.AreEqual(200, result[0].Weight);
        //    Assert.AreEqual(200, result[0].Height);
        //    Assert.AreEqual(dateOfBirth, result[0].DateOfBirth);
        //}

        //[Test]
        //public void GetMembersForGivenTermTest()
        //{

        //}

        [Test]
        public void ConnectToModul1OK()
        {
            bool result = proxyForM1.ConnectToModul1("fc", "192.168.1.1");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void ConnectToModul1Fail()
        {
            bool result = proxyForM1.ConnectToModul1("fc2", "192.168.1.1");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void UpdateCoachOk()
        {
            bool result = clientProxyTest.UpdateCoach(coach);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void UpdateCoachFail()
        {
            bool result = clientProxyTest.UpdateCoach(coach2);
            Assert.AreEqual(false, result);
        }

        [Test]
        public void UpdateMemberOk()
        {
            bool result = clientProxyTest.UpdateMember(member);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void UpdateMemberFail()
        {
            bool result = clientProxyTest.UpdateMember(member2);
            Assert.AreEqual(false, result);
        }

        [Test]
        public void UpdatePackageOk()
        {
            bool result = clientProxyTest.UpdatePackage(trainingPackage);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void UpdatePackageFail()
        {
            bool result = clientProxyTest.UpdatePackage(trainingPackage2);
            Assert.AreEqual(false, result);
        }

        [Test]
        public void UpdateTermOk()
        {
            bool result = clientProxyTest.UpdateTerm(term);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void UpdateTermFail()
        {
            bool result = clientProxyTest.UpdateTerm(term2);
            Assert.AreEqual(false, result);
        }

        [Test]
        public void CoachWindowContructor()
        {
            Assert.DoesNotThrow(() => new CoachWindow(string.Empty));
        }

        [Test]
        public void CoachPersonalWindowContructor()
        {
            Assert.DoesNotThrow(() => new CoachPersonalWindow(string.Empty));
        }

        [Test]
        public void CreatePackageWindowContructor()
        {
            Assert.DoesNotThrow(() => new CreatePackageWindow(new ListBox()));
        }

        [Test]
        public void EditPackageWindowContructor()
        {
            Assert.DoesNotThrow(() => new EditPackageWindow(new TrainingPackage(), new DataGrid(), new ListBox(), string.Empty));
        }

        [Test]
        public void FCMngPersonalWindowContructor()
        {
            Assert.DoesNotThrow(() => new FCMngPersonalWindow(string.Empty));
        }

        [Test]
        public void LoginWindowContructor()
        {
            Assert.DoesNotThrow(() => new LoginWindow());
        }

        [Test]
        public void MainWindowContructor()
        {
            Assert.DoesNotThrow(() => new MainWindow(string.Empty));
        }

        [Test]
        public void MemberPersonalWindowContructor()
        {
            Assert.DoesNotThrow(() => new MemberPersonalWindow());
        }

        [Test]
        public void MemberindowContructor()
        {
            Assert.DoesNotThrow(() => new MemberWindow(string.Empty));
        }

        [Test]
        public void TermCreateWindowContructor()
        {
            Assert.DoesNotThrow(() => new TermCreateWindow(new DataGrid(), string.Empty));
        }

        [Test]
        public void TermEditWindowContructor()
        {
            Assert.DoesNotThrow(() => new TermEditWindow(new Term(string.Empty, "Tuesday", new Time(), "sala1", Term.TrainingLevel.ADVANCED, "tr1", 2000), string.Empty, new DataGrid()));
        }

        [Test]
        public void GetAllMPRelsTest()
        {
            List<MemberPackageRel> result = clientProxyTest.GetAllMPRels();
            Assert.AreSame(mpRel, result[0]);
            Assert.AreSame(mpRel2, result[1]);
        }

        [Test]
        public void GetAllMTRelsTest()
        {
            List<MemberTermRel> result = clientProxyTest.GetAllMTRels();
            Assert.AreSame(mtRel, result[0]);
            Assert.AreSame(mtRel2, result[1]);
        }

        [Test]
        public void LoginFitnessCenterManagerOk()
        {
            bool result = clientProxyTest.LoginFitnessCenterManager("a", "a");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void LoginFitnessCenterManagerFail()
        {
            bool result = clientProxyTest.LoginFitnessCenterManager("b", "b");
            Assert.AreEqual(false, result);
        }

        //[Test]
        //public void GetMembersForGivenPackagesTest()
        //{
        //    List<Member> result = clientProxyTest.GetMembersForGivenPackage("package");
        //    Assert.AreEqual("a", result[0].FirstName);
        //    Assert.AreEqual("a", result[0].LastName);
        //    Assert.AreEqual("a", result[0].Username);
        //    Assert.AreEqual("a", result[0].Password);
        //    Assert.AreEqual("a", result[0].Address);
        //    Assert.AreEqual(200, result[0].Weight);
        //    Assert.AreEqual(200, result[0].Height);
        //    Assert.AreEqual(dateOfBirth, result[0].DateOfBirth);

        //    Assert.AreEqual("a1", result[1].FirstName);
        //    Assert.AreEqual("a1", result[1].LastName);
        //    Assert.AreEqual("a1", result[1].Username);
        //    Assert.AreEqual("a1", result[1].Password);
        //    Assert.AreEqual("a1", result[1].Address);
        //    Assert.AreEqual(200, result[1].Weight);
        //    Assert.AreEqual(200, result[1].Height);
        //    Assert.AreEqual(dateOfBirth, result[1].DateOfBirth);
        //}

        [Test]
        public void GetTermsByCocachUsernameTest()
        {
            List<Term> result = clientProxyTest.GetTermsByCocachUsername("coach");
            Assert.AreEqual("term1", result[0].Title);
            Assert.AreEqual("Tuesday", result[0].Day);
            Assert.AreEqual(time, result[0].TimeRange);
            Assert.AreEqual("sala1", result[0].ApointedHall);
            Assert.AreEqual(Term.TrainingLevel.ADVANCED, result[0].TrainingLvl);
            Assert.AreEqual("tr1", result[0].ApointedCoach);
            Assert.AreEqual(1000, result[0].Price);

            Assert.AreEqual("term2", result[1].Title);
            Assert.AreEqual("Tuesday", result[1].Day);
            Assert.AreEqual(time, result[1].TimeRange);
            Assert.AreEqual("sala2", result[1].ApointedHall);
            Assert.AreEqual(Term.TrainingLevel.ADVANCED, result[1].TrainingLvl);
            Assert.AreEqual("tr1", result[1].ApointedCoach);
            Assert.AreEqual(1000, result[1].Price);
        }

        [Test]
        public void GetTermsByManagerUsernameTest()
        {
            List<Term> result = clientProxyTest.GetTermsByManagerUsername("manager");
            Assert.AreEqual("term1", result[0].Title);
            Assert.AreEqual("Tuesday", result[0].Day);
            Assert.AreEqual(time, result[0].TimeRange);
            Assert.AreEqual("sala1", result[0].ApointedHall);
            Assert.AreEqual(Term.TrainingLevel.ADVANCED, result[0].TrainingLvl);
            Assert.AreEqual("tr1", result[0].ApointedCoach);
            Assert.AreEqual(1000, result[0].Price);

            Assert.AreEqual("term2", result[1].Title);
            Assert.AreEqual("Tuesday", result[1].Day);
            Assert.AreEqual(time, result[1].TimeRange);
            Assert.AreEqual("sala2", result[1].ApointedHall);
            Assert.AreEqual(Term.TrainingLevel.ADVANCED, result[1].TrainingLvl);
            Assert.AreEqual("tr1", result[1].ApointedCoach);
            Assert.AreEqual(1000, result[1].Price);
        }

        [Test]
        public void GetTermsForGivenMemberTest()
        {
            List<Term> result = clientProxyTest.GetTermsForGivenMember("member");
            Assert.AreEqual("term1", result[0].Title);
            Assert.AreEqual("Tuesday", result[0].Day);
            Assert.AreEqual(time, result[0].TimeRange);
            Assert.AreEqual("sala1", result[0].ApointedHall);
            Assert.AreEqual(Term.TrainingLevel.ADVANCED, result[0].TrainingLvl);
            Assert.AreEqual("tr1", result[0].ApointedCoach);
            Assert.AreEqual(1000, result[0].Price);

            Assert.AreEqual("term2", result[1].Title);
            Assert.AreEqual("Tuesday", result[1].Day);
            Assert.AreEqual(time, result[1].TimeRange);
            Assert.AreEqual("sala2", result[1].ApointedHall);
            Assert.AreEqual(Term.TrainingLevel.ADVANCED, result[1].TrainingLvl);
            Assert.AreEqual("tr1", result[1].ApointedCoach);
            Assert.AreEqual(1000, result[1].Price);
        }

        [Test]
        public void PayMPRelOK()
        {
            bool res = clientProxyTest.PayMPRel("mem", "pack");
            Assert.AreEqual(true, res);
        }

        [Test]
        public void PayMPRelFail()
        {
            bool res = clientProxyTest.PayMPRel("mem2", "pack2");
            Assert.AreEqual(false, res);
        }

        [Test]
        public void PayMTRelOK()
        {
            bool res = clientProxyTest.PayMTRel("mem", "term");
            Assert.AreEqual(true, res);
        }

        [Test]
        public void PayMTRelFail()
        {
            bool res = clientProxyTest.PayMTRel("mem2", "term2");
            Assert.AreEqual(false, res);
        }

        [Test]
        public void SetCanceledTrainingOK()
        {
            bool res = clientProxyTest.SetCanceledTraining(canceledTraining);
            Assert.AreEqual(true, res);
        }

        [Test]
        public void SetCanceledTrainingFail()
        {
            bool res = clientProxyTest.SetCanceledTraining(canceledTrainingBad);
            Assert.AreEqual(false, res);
        }

        [Test]
        public void GetNotificationsForMemberTest()
        {
            List<string> res = clientProxyTest.GetNotificationsForMember("member");
            Assert.AreEqual("notif", res[0]);
            Assert.AreEqual("notif1", res[1]);
        }

        [Test]
        public void GetTermTest()
        {
            Term result = clientProxyTest.GetTerm("term1");
            Assert.AreEqual("term1", result.Title);
            Assert.AreEqual("Tuesday", result.Day);
            Assert.AreEqual(time, result.TimeRange);
            Assert.AreEqual("sala1", result.ApointedHall);
            Assert.AreEqual(Term.TrainingLevel.ADVANCED, result.TrainingLvl);
            Assert.AreEqual("tr1", result.ApointedCoach);
            Assert.AreEqual(1000, result.Price);
        }

        [Test]
        public void SetMPRelOK()
        {
            bool res = clientProxyTest.SetMPRel(mpRel);
            Assert.AreEqual(true, res);
        }

        [Test]
        public void SetMPRelFail()
        {
            bool res = clientProxyTest.SetMPRel(mpRel2);
            Assert.AreEqual(false, res);
        }

        [Test]
        public void SetMTRelOK()
        {
            bool res = clientProxyTest.SetMTRel(mtRel);
            Assert.AreEqual(true, res);
        }

        [Test]
        public void SetMTRelFail()
        {
            bool res = clientProxyTest.SetMTRel(mtRel2);
            Assert.AreEqual(false, res);
        }

        //[Test]
        //public void GetPackagesForGivenMemberTest()
        //{
        //    List<TrainingPackage> result = clientProxyTest.GetPackagesForGivenMember("member");
        //    Assert.AreEqual(termList, result[0].Terms);
        //    Assert.AreEqual(1000, result[0].MonthPrice);
        //    Assert.AreEqual("package", result[0].PackageName);

        //    Assert.AreEqual(termList, result[1].Terms);
        //    Assert.AreEqual(1500, result[1].MonthPrice);
        //    Assert.AreEqual("package2", result[1].PackageName);
        //}

        [Test]
        public void DeleteMPRelOk()
        {
            bool result = clientProxyTest.DeleteMPRel(mpRel);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void DeleteMPRelFail()
        {
            bool result = clientProxyTest.DeleteMPRel(mpRel2);
            Assert.AreEqual(false, result);
        }

        [Test]
        public void DeleteMTRelmOk()
        {
            bool result = clientProxyTest.DeleteMTRel(mtRel);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void DeleteMTRelFail()
        {
            bool result = clientProxyTest.DeleteMTRel(mtRel2);
            Assert.AreEqual(false, result);
        }

        [Test]
        public void NotifyMemberPackageOk()
        {
            bool result = clientProxyTest.NotifyMembersForPackageChange("package", "notification");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void NotifyMemberPackageFail()
        {
            bool result = clientProxyTest.NotifyMembersForPackageChange("package2", "notification2");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void NotifyMemberTermOk()
        {
            bool result = clientProxyTest.NotifyMembersForTermChange("term", "notification");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void NotifyMemberTermFail()
        {
            bool result = clientProxyTest.NotifyMembersForTermChange("term2", "notification2");
            Assert.AreEqual(false, result);
        }
    }
}

