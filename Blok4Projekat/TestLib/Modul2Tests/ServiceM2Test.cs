﻿using CommonLibM2;
using CommonModulLib;
using CommonModulLib.DataModel;
using Modul2Service;
using Modul2Service.Access;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib
{
    [TestFixture]
    public class ServiceM2Test
    {
        #region Declarations

        private LoginManager serviceUnderTest;
        private Coach coachOK;
        private Coach coachBAD;
        private Member memberOK;
        private Member memberBAD;
        private Term termOK;
        private Term termBAD;
        private TrainingPackage trainingPackage;
        private TrainingPackage trainingPackageOK;
        private TrainingPackage trainingPackageBAD;
        private MemberPackageRel memberPackageRelOK;
        private MemberPackageRel memberPackageRelBAD;
        private MemberTermRel memberTermRelOK;
        private MemberTermRel memberTermRelBAD;
        private CanceledTraining canceledTrainingOK;
        private CanceledTraining canceledTrainingBAD;
        private Time time;
        private string testOK = "testOK";
        private string testBAD = "testBAD";
        private DateTime dateTime = new DateTime(2017, 1, 1);
        private List<Coach> coaches = new List<Coach>();
        private List<Term> terms = new List<Term>();
        private List<TrainingPackage> trainingPackages = new List<TrainingPackage>();
        private List<MemberPackageRel> memberPackageRels = new List<MemberPackageRel>();
        private List<MemberTermRel> memberTermRels = new List<MemberTermRel>();
        private List<Member> members = new List<Member>();
        private List<string> strings = new List<string>();
        private double doubleTest = 1;
        private int intTest = 1;

        private IModul1Contract proxy;
        private FitnessCenterManager fcManager;
        private TrainingPackage badPackage;
        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        public void SetupTest()
        {
            serviceUnderTest = new LoginManager();
            DBManager.Instance = Substitute.For<IDBManager>();
            coachOK = new Coach(testOK, testOK, testOK, testOK, testOK, testOK, dateTime, testOK);
            coachBAD = new Coach(testBAD, testBAD, testBAD, testBAD, testBAD, testBAD, dateTime, testBAD);
            memberOK = new Member(testOK, testOK, testOK, testOK, testOK, doubleTest, doubleTest, dateTime);
            memberBAD = new Member(testBAD, testBAD, testBAD, testBAD, testBAD, doubleTest, doubleTest, dateTime);
            termOK = new Term(testOK, testOK, time, testOK, Term.TrainingLevel.BEGGINER, testOK, doubleTest);
            termBAD = new Term(testBAD, testBAD, time, testBAD, Term.TrainingLevel.NONE, testBAD, doubleTest);
            trainingPackageOK = new TrainingPackage(new List<string>(), doubleTest, testOK);
            trainingPackageBAD = new TrainingPackage(new List<string>(), doubleTest, testBAD);
            memberPackageRelOK = new MemberPackageRel();
            memberPackageRelOK.Id = 1;
            memberPackageRelOK.memberName = testOK;
            memberPackageRelOK.packageName = testOK;
            memberPackageRelOK.Paid = true;
            memberPackageRelBAD = new MemberPackageRel();
            memberPackageRelBAD.Id = 0;
            memberPackageRelBAD.memberName = testBAD;
            memberPackageRelBAD.packageName = testBAD;
            memberPackageRelBAD.Paid = false;
            memberTermRelOK = new MemberTermRel();
            memberTermRelOK.Id = 1;
            memberTermRelOK.memberName = testOK;
            memberTermRelOK.termTitle = testOK;
            memberTermRelOK.Paid = true;
            memberTermRelBAD = new MemberTermRel();
            memberTermRelBAD.Id = 0;
            memberTermRelBAD.memberName = testBAD;
            memberTermRelBAD.termTitle = testBAD;
            memberTermRelBAD.Paid = false;
            canceledTrainingOK = new CanceledTraining();
            canceledTrainingOK.Id = 1;
            canceledTrainingOK.FitnessCenter = testOK;
            canceledTrainingOK.Training = testOK;
            canceledTrainingBAD = new CanceledTraining();
            canceledTrainingBAD.Id = 0;
            canceledTrainingBAD.FitnessCenter = testBAD;
            canceledTrainingBAD.Training = testBAD;
            coaches.Add(coachOK);
            coaches.Add(coachBAD);
            terms.Add(termOK);
            terms.Add(termBAD);
            trainingPackages.Add(trainingPackageOK);
            trainingPackages.Add(trainingPackageBAD);
            memberPackageRels.Add(memberPackageRelOK);
            memberPackageRels.Add(memberPackageRelBAD);
            members.Add(memberOK);
            members.Add(memberBAD);
            time = new Time(1, 1, 1, 1);

            proxy = Substitute.For<IModul1Contract>();
            fcManager = new FitnessCenterManager("fcm", "fcm", "fcm", "fcm", "fcm 19", "567890");
            ConnectClassForM1.clientProxy = proxy;
            trainingPackage = new TrainingPackage(new List<string>() { "prviTermin", "drugiTermin" }, 210, "pg1" );
            badPackage = new TrainingPackage(new List<string>(), 2000, "novi paket");

            #region LoginSetup
            DBManager.Instance.Login("coach", "coach").Returns(AuthenticationAndAuthorization.COACH);
            DBManager.Instance.Login("member", "member").Returns(AuthenticationAndAuthorization.MEMBER);
            DBManager.Instance.Login("fcm", "fcm").Returns(AuthenticationAndAuthorization.FITNESS_CENTER_MANAGER);
            DBManager.Instance.Login("wrong", "wrong").Returns(AuthenticationAndAuthorization.NONE);
            #endregion LoginSetup

            #region CoachSetup
            DBManager.Instance.SetCoach(testOK, testOK, testOK, testOK, testOK, testOK, dateTime, testOK).Returns(true);
            DBManager.Instance.SetCoach(testBAD, testBAD, testBAD, testBAD, testBAD, testBAD, dateTime, testBAD).Returns(false);
            DBManager.Instance.UpdateCoach(coachOK).Returns(true);
            DBManager.Instance.UpdateCoach(coachBAD).Returns(false);
            DBManager.Instance.DeleteCoach(testOK).Returns(true);
            DBManager.Instance.DeleteCoach(testBAD).Returns(false);
            DBManager.Instance.GetCoach(testOK).Returns(coachOK);
            DBManager.Instance.GetCoach(testBAD).Returns(coachBAD);
            DBManager.Instance.GetAllCoaches().Returns(coaches);
            #endregion CoachSetup

            #region MemberSetup
            DBManager.Instance.SetMember(testOK, testOK, testOK, testOK, testOK, doubleTest, doubleTest, dateTime).Returns(true);
            DBManager.Instance.SetMember(testBAD, testBAD, testBAD, testBAD, testBAD, doubleTest, doubleTest, dateTime).Returns(false);
            DBManager.Instance.UpdateMember(memberOK).Returns(true);
            DBManager.Instance.UpdateMember(memberBAD).Returns(false);
            DBManager.Instance.DeleteMember(testOK).Returns(true);
            DBManager.Instance.DeleteMember(testBAD).Returns(false);
            DBManager.Instance.GetMember(testOK).Returns(memberOK);
            DBManager.Instance.GetMember(testBAD).Returns(memberBAD);
            #endregion MemberSetup

            #region TermSetup
            DBManager.Instance.SetTerm(testOK, testOK, intTest, intTest, intTest, intTest, testOK, Term.TrainingLevel.BEGGINER, testOK, doubleTest).Returns(true);
            DBManager.Instance.SetTerm(testBAD, testBAD, intTest, intTest, intTest, intTest, testBAD, Term.TrainingLevel.NONE, testBAD, doubleTest).Returns(false);
            DBManager.Instance.UpdateTerm(termOK).Returns(true);
            DBManager.Instance.UpdateTerm(termBAD).Returns(false);
            DBManager.Instance.DeleteTerm(testOK).Returns(true);
            DBManager.Instance.DeleteTerm(testBAD).Returns(false);
            DBManager.Instance.GetTerm(testOK).Returns(termOK);
            DBManager.Instance.GetTerm(testBAD).Returns(termBAD);
            DBManager.Instance.GetAllTerms().Returns(terms);
            #endregion TermSetup

            #region ToModul1
            proxy.GetAllHalls("fitnessCenter").Returns(new List<Hall>() { new Hall("hall1", TypeOfHall.FITNESS, "fitnessCenter"), new Hall("hall2", TypeOfHall.YOGA, "fitnessCenter") });

            proxy.GetAllFitnessCenters().Returns(new List<FitnessCenter>() { new FitnessCenter("fitnessCenter", "add 13", "78319", 3, "fcm", time) });

            proxy.EditPersonalFCMdata(fcManager).Returns(true);

            proxy.GetAllFitnessCenterByManagerName(coachOK.Username).Returns(new List<string>() { "fitnessCenter" });

            proxy.LoginFitnessCenterManager("fcm", "fcm").Returns(true);

            proxy.GetFitnessCenterManagerByUserName("fcm").Returns(fcManager);

            proxy.ConnectToModul1("fitnessCenter", "testIP").Returns(true);

            #endregion ToModul1

            #region PackageSetup
            DBManager.Instance.SetTrainingPackage(strings, doubleTest, testOK).Returns(true);
            DBManager.Instance.SetTrainingPackage(strings, doubleTest, testBAD).Returns(false);
            DBManager.Instance.DeleteTrainingPackage(trainingPackageOK.Id).Returns(true);
            DBManager.Instance.UpdateTrainingPackage(trainingPackageOK).Returns(true);
            DBManager.Instance.UpdateTrainingPackage(trainingPackageBAD).Returns(false);
            DBManager.Instance.GetAllTPackages().Returns(trainingPackages);
            #endregion PackageSetup

            #region MemberPackageRelationshipSetup
            DBManager.Instance.GetAllMPRels().Returns(memberPackageRels);
            DBManager.Instance.SetMPRel(memberPackageRelOK).Returns(true);
            DBManager.Instance.SetMPRel(memberPackageRelBAD).Returns(false);
            DBManager.Instance.DeleteMPRel(memberPackageRelOK).Returns(true);
            DBManager.Instance.DeleteMPRel(memberPackageRelBAD).Returns(false);
            DBManager.Instance.PayMPRel(memberPackageRelOK.memberName, memberPackageRelOK.packageName).Returns(true);
            DBManager.Instance.PayMPRel(memberPackageRelBAD.memberName, memberPackageRelBAD.packageName).Returns(false);
            #endregion MemberPackageRelationshipSetup

            #region MemberTermRelationshipSetup
            DBManager.Instance.GetAllMTRels().Returns(memberTermRels);
            //DBManager.Instance.GetMembersForGivenTerm(testOK, testOK).Returns(members);
            //DBManager.Instance.GetMembersForGivenTerm(testBAD, testBAD).Returns(members);
            DBManager.Instance.PayMTRel(memberTermRelOK.memberName, memberTermRelOK.termTitle).Returns(true);
            DBManager.Instance.PayMTRel(memberTermRelBAD.memberName, memberTermRelBAD.termTitle).Returns(false);
            DBManager.Instance.SetMTRel(memberTermRelOK).Returns(true);
            DBManager.Instance.SetMTRel(memberTermRelBAD).Returns(false);
            DBManager.Instance.DeleteMTRel(memberTermRelOK).Returns(true);
            DBManager.Instance.DeleteMTRel(memberTermRelBAD).Returns(false);
            #endregion MemberTermRelationshipSetup

            #region CanceledTrainingSetup

            DBManager.Instance.SetCanceledTraining(canceledTrainingOK).Returns(true);
            DBManager.Instance.SetCanceledTraining(canceledTrainingBAD).Returns(false);

            #endregion CanceledTrainingSetup

            #region NotifyMembersSetup

            DBManager.Instance.NotifyMembersForPackageChange("package", "notification").Returns(true);
            DBManager.Instance.NotifyMembersForPackageChange("package2", "notification2").Returns(false);

            DBManager.Instance.NotifyMembersForTermChange("term", "notification").Returns(true);
            DBManager.Instance.NotifyMembersForTermChange("term2", "notification2").Returns(false);

            #endregion NotifyMembersSetup

        }

        #endregion setup

        #region tests

        #region LoginTests
        [Test]
        public void LoginTestCoachOK()
        {
            AuthenticationAndAuthorization result = serviceUnderTest.Login("coach", "coach");
            Assert.AreEqual(AuthenticationAndAuthorization.COACH, result);
        }
        [Test]
        public void LoginTestCoachFailed()
        {
            AuthenticationAndAuthorization result = serviceUnderTest.Login("coach", "badpass");
            Assert.AreNotEqual(AuthenticationAndAuthorization.COACH, result);
        }
        [Test]
        public void LoginTestMemberOK()
        {
            AuthenticationAndAuthorization result = serviceUnderTest.Login("member", "member");
            Assert.AreEqual(AuthenticationAndAuthorization.MEMBER, result);
        }
        [Test]
        public void LoginTestMemberFailed()
        {
            AuthenticationAndAuthorization result = serviceUnderTest.Login("baduser", "member");
            Assert.AreNotEqual(AuthenticationAndAuthorization.MEMBER, result);
        }
        [Test]
        public void LoginTestFCMOK()
        {
            AuthenticationAndAuthorization result = serviceUnderTest.Login("fcm", "fcm");
            Assert.AreEqual(AuthenticationAndAuthorization.FITNESS_CENTER_MANAGER, result);
        }
        [Test]
        public void LoginTestFailed()
        {
            AuthenticationAndAuthorization result = serviceUnderTest.Login("wrong", "wrong");
            Assert.AreEqual(AuthenticationAndAuthorization.NONE, result);
        }
        #endregion LoginTests

        #region CoachTests
        [Test]
        public void SetCoachTestOK()
        {
            bool result = serviceUnderTest.AddNewCoach(testOK, testOK, testOK, testOK, testOK, testOK, dateTime, testOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void SetCoachTestBAD()
        {
            bool result = serviceUnderTest.AddNewCoach(testBAD, testBAD, testBAD, testBAD, testBAD, testBAD, dateTime, testBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void UpdateCoachTestOK()
        {
            bool result = serviceUnderTest.UpdateCoach(coachOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void UpdateCoachTestBAD()
        {
            bool result = serviceUnderTest.UpdateCoach(coachBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void DeleteCoachTestOK()
        {
            bool result = serviceUnderTest.DeleteCoach(testOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void DeleteCoachTestBAD()
        {
            bool result = serviceUnderTest.DeleteCoach(testBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void GetCoachTestOK()
        {
            Coach result = serviceUnderTest.GetCoach(testOK);
            Assert.AreEqual(coachOK, result);
        }
        [Test]
        public void GetCoachTestBAD()
        {
            Coach result = serviceUnderTest.GetCoach(testBAD);
            Assert.AreEqual(coachBAD, result);
        }
        [Test]
        public void GetAllCoachesTestOK()
        {
            List<Coach> result = serviceUnderTest.GetAllCoaches();
            Assert.AreEqual(coaches, result);
        }
        #endregion CoachTest

        #region MemberTests
        [Test]
        public void SetMemberTestOK()
        {
            bool result = serviceUnderTest.AddNewMember(testOK, testOK, testOK, testOK, testOK, doubleTest, doubleTest, dateTime);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void SetMemberTestBAD()
        {
            bool result = serviceUnderTest.AddNewMember(testBAD, testBAD, testBAD, testBAD, testBAD, doubleTest, doubleTest, dateTime);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void UpdateMemberTestOK()
        {
            bool result = serviceUnderTest.UpdateMember(memberOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void UpdateMemberTestBAD()
        {
            bool result = serviceUnderTest.UpdateMember(memberBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void DeleteMemberTestOK()
        {
            bool result = serviceUnderTest.DeleteMember(testOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void DeleteMemberTestBAD()
        {
            bool result = serviceUnderTest.DeleteMember(testBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void GetMemberTestOK()
        {
            Member result = serviceUnderTest.GetMember(testOK);
            Assert.AreEqual(memberOK, result);
        }
        [Test]
        public void GetMemberTestBAD()
        {
            Member result = serviceUnderTest.GetMember(testBAD);
            Assert.AreEqual(memberBAD, result);
        }
        #endregion MemberTests

        #region TermTests
        [Test]
        public void SetTermTestOK()
        {
            bool result = serviceUnderTest.AddNewTerm(testOK, testOK, intTest, intTest, intTest, intTest, testOK, Term.TrainingLevel.BEGGINER, testOK, doubleTest);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void SetTermTestBAD()
        {
            bool result = serviceUnderTest.AddNewTerm(testBAD, testBAD, intTest, intTest, intTest, intTest, testBAD, Term.TrainingLevel.NONE, testBAD, doubleTest);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void UpdateTermTestOK()
        {
            bool result = serviceUnderTest.UpdateTerm(termOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void UpdateTermTestBAD()
        {
            bool result = serviceUnderTest.UpdateTerm(termBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void DeleteTermTestOK()
        {
            bool result = serviceUnderTest.DeleteTerm(testOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void DeleteTermTestBAD()
        {
            bool result = serviceUnderTest.DeleteTerm(testBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void GetTermTestOK()
        {
            Term result = serviceUnderTest.GetTerm(testOK);
            Assert.AreEqual(termOK, result);
        }
        [Test]
        public void GetTermTestBAD()
        {
            Term result = serviceUnderTest.GetTerm(testBAD);
            Assert.AreEqual(termBAD, result);
        }
        [Test]
        public void GetAllTermsTestOK()
        {
            List<Term> result = serviceUnderTest.GetAllTerms();
            Assert.AreEqual(terms, result);
        }
        #endregion TermTests

        #region ToModul1Tests

        [Test]
        public void GetAllHallsTestOK()
        {
            List<Hall> expected = new List<Hall>() { new Hall("hall1", TypeOfHall.FITNESS, "fitnessCenter"), new Hall("hall2", TypeOfHall.YOGA, "fitnessCenter") };
            List<Hall> actual = serviceUnderTest.GetAllHalls("fitnessCenter");
            Assert.AreEqual(expected[0].Id, actual[0].Id);
            Assert.AreEqual(expected[1].Id, actual[1].Id);

        }

        [Test]
        public void GetAllHallsTestFailed()
        {
            List<Hall> expected = new List<Hall>() { new Hall("hall3", TypeOfHall.FITNESS, "fitnessCenter"), new Hall("hall4", TypeOfHall.YOGA, "fitnessCenter") };
            List<Hall> actual = serviceUnderTest.GetAllHalls("fitnessCenter");
            Assert.AreNotEqual(expected[0].Id, actual[0].Id);
            Assert.AreNotEqual(expected[1].Id, actual[1].Id);
        }

        [Test]
        public void GetAllFitnessCentersOK()
        {
            List<FitnessCenter> expected = new List<FitnessCenter>() { new FitnessCenter("fitnessCenter", "add 13", "78319", 3, "fcm", time) };
            List<FitnessCenter> actual = serviceUnderTest.GetAllFitnessCenters();
            Assert.AreEqual(expected[0].Name, actual[0].Name);
        }

        [Test]
        public void GetAllFitnessCentersFailed()
        {
            List<FitnessCenter> expected = new List<FitnessCenter>() { new FitnessCenter("fc1", "add 13", "78319", 3, "fcm", time) };
            List<FitnessCenter> actual = serviceUnderTest.GetAllFitnessCenters();
            Assert.AreNotEqual(expected[0].Name, actual[0].Name);
        }

        [Test]
        public void EditPersonalFCMdataOK()
        {
            bool result = serviceUnderTest.EditPersonalFCMdata(fcManager);
            Assert.AreEqual(result, true);
        }

        [Test]
        public void EditPersonalFCMdataFailed()
        {
            FitnessCenterManager newFCM = new FitnessCenterManager("fcm2", "fcm2", "fcm2", "fcm2", "fcm 192", "5678902");
            bool result = serviceUnderTest.EditPersonalFCMdata(newFCM);
            Assert.AreEqual(result, false);
        }

        [Test]
        public void GetAllFitnessCenterByManagerNameOK()
        {
            List<string> expexted = new List<string>() { "fitnessCenter" };
            List<string> actual = serviceUnderTest.GetAllFitnessCenterByManagerName(coachOK.Username);
            Assert.AreEqual(expexted[0], actual[0]);
        }

        [Test]
        public void GetAllFitnessCenterByManagerNameFailed()
        {
            List<string> expexted = new List<string>() { "fc1" };
            List<string> actual = serviceUnderTest.GetAllFitnessCenterByManagerName(coachOK.Username);
            Assert.AreNotEqual(expexted[0], actual[0]);
        }

        [Test]
        public void LoginFitnessCenterManagerOK()
        {
            bool result = serviceUnderTest.LoginFitnessCenterManager("fcm", "fcm");
            Assert.AreEqual(result, true);
        }

        [Test]
        public void LoginFitnessCenterManagerManager()
        {
            bool result = serviceUnderTest.LoginFitnessCenterManager("fcm1", "fcm1");
            Assert.AreEqual(result, false);
        }

        [Test]
        public void GetFitnessCenterManagerByUserNameOK()
        { 
            FitnessCenterManager result = serviceUnderTest.GetFitnessCenterManagerByUserName("fcm");
            Assert.AreEqual(result.Username, fcManager.Username);
        }

        [Test]
        public void GetFitnessCenterManagerByUserNameFailed()
        {
            FitnessCenterManager newFCM = new FitnessCenterManager("fcm2", "fcm2", "fcm2", "fcm2", "fcm 192", "5678902");
            FitnessCenterManager result = serviceUnderTest.GetFitnessCenterManagerByUserName("fcm");
            Assert.AreNotEqual(result.Username, newFCM.Username);
        }

        [Test]
        public void ConnectToModul1OK()
        {
            bool result = serviceUnderTest.ConnectToModul1("fitnessCenter", "testIP");
            Assert.AreEqual(result, true);
        }

        [Test]
        public void ConnectToModul1Failed()
        {
            bool result = serviceUnderTest.ConnectToModul1("fc1", "newTestIP");
            Assert.AreEqual(result, false);
        }


        #endregion ToModul1Tests

        #region PackageTests
        [Test]
        public void SetPackageTestOK()
        {
            bool result = serviceUnderTest.AddNewTrainingPackage(strings, doubleTest, testOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void SetPackageTestBAD()
        {
            bool result = serviceUnderTest.AddNewTrainingPackage(strings, doubleTest, testBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void DeletePackageOK()
        {
            bool result = serviceUnderTest.DeletePackage(trainingPackage.Id);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void DeletePackageFailed()
        {
            bool result = serviceUnderTest.DeletePackage(999);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void UpdateTrainingPackageTestOK()
        {
            bool result = serviceUnderTest.UpdatePackage(trainingPackageOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void UpdateTrainingPackageTestBAD()
        {
            bool result = serviceUnderTest.UpdatePackage(trainingPackageBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void GetAllTraininingPackagesTestOK()
        {
            List<TrainingPackage> result = serviceUnderTest.GetAllTraininingPackages();
            Assert.AreEqual(trainingPackages, result);
        }
        #endregion PackageTests

        #region MemberPackageRelationshipTests
        [Test]
        public void SetMPRelTestOK()
        {
            bool result = serviceUnderTest.SetMPRel(memberPackageRelOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void SetMPRelTestBAD()
        {
            bool result = serviceUnderTest.SetMPRel(memberPackageRelBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void DeleteMPRelTestOK()
        {
            bool result = serviceUnderTest.DeleteMPRel(memberPackageRelOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void DeleteMPRelTestBAD()
        {
            bool result = serviceUnderTest.DeleteMPRel(memberPackageRelBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void PayMPRelTestOK()
        {
            bool result = serviceUnderTest.PayMPRel(memberPackageRelOK.memberName, memberPackageRelOK.packageName);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void PayMPRelTestBAD()
        {
            bool result = serviceUnderTest.PayMPRel(memberPackageRelBAD.memberName, memberPackageRelBAD.packageName);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void GetAllMPRelsTestOK()
        {
            List<MemberPackageRel> result = serviceUnderTest.GetAllMPRels();
            Assert.AreEqual(memberPackageRels, result);
        }
        #endregion MemberPackageRelationshipTests

        #region MemberTermRelationshipTests
        [Test]
        public void SetMTRelTestOK()
        {
            bool result = serviceUnderTest.SetMTRel(memberTermRelOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void SetMTRelTestBAD()
        {
            bool result = serviceUnderTest.SetMTRel(memberTermRelBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void DeleteMTRelTestOK()
        {
            bool result = serviceUnderTest.DeleteMTRel(memberTermRelOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void DeleteMTRelTestBAD()
        {
            bool result = serviceUnderTest.DeleteMTRel(memberTermRelBAD);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void PayMTRelTestOK()
        {
            bool result = serviceUnderTest.PayMPRel(memberTermRelOK.memberName, memberTermRelOK.termTitle);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void PayMTRelTestBAD()
        {
            bool result = serviceUnderTest.PayMPRel(memberTermRelBAD.memberName, memberTermRelBAD.termTitle);
            Assert.AreEqual(false, result);
        }
        [Test]
        public void GetAllMTRelsTestOK()
        {
            List<MemberTermRel> result = serviceUnderTest.GetAllMTRels();
            Assert.AreEqual(memberTermRels, result);
        }
        //[Test]
        //public void GetMembersForGivenTermTestOK()
        //{
        //    List<Member> result = serviceUnderTest.GetMembersForGivenTerm(testOK, testOK);
        //    Assert.AreEqual(members, result);
        //}
        //[Test]
        //public void GetMembersForGivenTermTestBAD()
        //{
        //    List<Member> result = serviceUnderTest.GetMembersForGivenTerm(testBAD, testBAD);
        //    Assert.AreEqual(members, result);
        //}
        #endregion MemberTermRelationshipTests

        #region CanceledTrainingTests

        [Test]
        public void SetCanceledTrainingTestOK()
        {
            bool result = serviceUnderTest.SetCanceledTraining(canceledTrainingOK);
            Assert.AreEqual(true, result);
        }
        [Test]
        public void SetCanceledTrainingTestBAD()
        {
            bool result = serviceUnderTest.SetCanceledTraining(canceledTrainingBAD);
            Assert.AreEqual(false, result);
        }

        #endregion CanceledTrainingTests

        #region NotifyMembersTests

        [Test]
        public void NotifyMemberPackageOK()
        {
            bool result = serviceUnderTest.NotifyMembersForPackageChange("package", "notification");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void NotifyMemberPackageBAD()
        {
            bool result = serviceUnderTest.NotifyMembersForPackageChange("package2", "notification2");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void NotifyMemberTermOK()
        {
            bool result = serviceUnderTest.NotifyMembersForTermChange("term", "notification");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void NotifyMemberTermBAD()
        {
            bool result = serviceUnderTest.NotifyMembersForTermChange("term2", "notification2");
            Assert.AreEqual(false, result);
        }

        #endregion NotifyMembersTests

        #endregion tests
    }
}
