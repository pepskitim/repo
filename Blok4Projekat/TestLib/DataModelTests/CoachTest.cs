﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class CoachTest
    {
        #region Declarations

        private Coach coachUnderTest;
        private string username = "test";
        private string password = "test";
        private string name = "test";
        private string lastName = "test";
        private string address = "test";
        private string universityDegree = "test";
        private DateTime dateOfBirth = DateTime.Now;
        private string fitnessCenter = "test";
        private bool employed = false;

        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.coachUnderTest = new Coach();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new Coach());
        }

        [Test]
        public void ConstructorBaseTest()
        {
            Assert.DoesNotThrow(() => new Coach(username, password, name, lastName, address, universityDegree, dateOfBirth, fitnessCenter));
        }

        [Test]
        public void UniversityDegreeTest()
        {
            coachUnderTest.UniversityDegree = universityDegree;

            Assert.AreEqual(universityDegree, coachUnderTest.UniversityDegree);
        }

        [Test]
        public void DateOfBirtheTest()
        {
            coachUnderTest.DateOfBirth = dateOfBirth;

            Assert.AreEqual(dateOfBirth, coachUnderTest.DateOfBirth);
        }

        [Test]
        public void EmployedTest()
        {
            coachUnderTest.Employed = employed;

            Assert.AreEqual(employed, coachUnderTest.Employed);
        }

        [Test]
        public void FitnessCenterTest()
        {
            coachUnderTest.FitnessCenter = fitnessCenter;

            Assert.AreEqual(fitnessCenter, coachUnderTest.FitnessCenter);
        }

        [Test]
        public void ToStringTest()
        {
            Assert.DoesNotThrow(() => coachUnderTest.ToString());
        }

        #endregion tests
    }
}
