﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class TrainingPackageTest
    {
        #region Declarations
        private TrainingPackage tpackageUnderTest;
        private List<string> terms = new List<string> { "term1", "term2" };
        private double monthPrice = 500;
        private string packageName = "package1";
        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.tpackageUnderTest = new TrainingPackage();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new TrainingPackage());
        }

        [Test]
        public void IdTest()
        {
            int id = 1;
            tpackageUnderTest.Id = id;

            Assert.AreEqual(id, tpackageUnderTest.Id);
        }

        [Test]
        public void TermsTest()
        {
            tpackageUnderTest.Terms = terms;
          
            Assert.AreEqual(terms, tpackageUnderTest.Terms);
        }

        [Test]
        public void MonthPriceTest()
        {
            tpackageUnderTest.MonthPrice = monthPrice;

            Assert.AreEqual(monthPrice, tpackageUnderTest.MonthPrice);
        }


        [Test]
        public void PackageNameTest()
        {
            tpackageUnderTest.PackageName = packageName;

            Assert.AreEqual(packageName, tpackageUnderTest.PackageName);
        }

        [Test]
        public void ToStringTest()
        {
            Assert.DoesNotThrow(() => tpackageUnderTest.ToString());
        }

        [Test]
        public void ConstructorWithParametersTest()
        {
            Assert.DoesNotThrow(() => new TrainingPackage(new List<string>() { "term" }, 100, "package"));
        }

        #endregion tests
    }
}
