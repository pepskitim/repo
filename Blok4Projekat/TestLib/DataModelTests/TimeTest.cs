﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class TimeTest
    {
        #region Declarations

        private Time timeUnderTest;
        private int startingHour = 1;
        private int startingMinute = 0;
        private int endingHour = 2;
        private int endingMinute = 0;

        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.timeUnderTest = new Time();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new Time());
        }
        
        [Test]
        public void StartingHourTest()
        {
            timeUnderTest.StartingHour = startingHour;

            Assert.AreEqual(startingHour, timeUnderTest.StartingHour);
        }

        [Test]
        public void StartingMinuteTest()
        {
            timeUnderTest.StartingMinute = startingMinute;

            Assert.AreEqual(startingMinute, timeUnderTest.StartingMinute);
        }


        [Test]
        public void EndingHourTest()
        {
            timeUnderTest.EndingHour = endingHour;

            Assert.AreEqual(endingHour, timeUnderTest.EndingHour);
        }

        [Test]
        public void EndingMinuteTest()
        {
            timeUnderTest.EndingMinute = endingMinute;

            Assert.AreEqual(endingMinute, timeUnderTest.EndingMinute);
        }

        [Test]
        public void ToStringTest()
        {
            Assert.DoesNotThrow(() => timeUnderTest.ToString());
        }

        #endregion tests
    }
}
