﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class HallTest
    {
        #region Declarations
        private Hall hallUnderTest;
        private TypeOfHall type = TypeOfHall.GYM;
        private string fitnessCenterName = "fc1";
        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.hallUnderTest = new Hall();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new Hall());
        }

        [Test]
        public void ConstructorTest2()
        {
            string id = "5";
            Assert.DoesNotThrow(() => new Hall(id, type, fitnessCenterName));
        }

        [Test]
        public void IdTest()
        {
            string id = "5";
            hallUnderTest.Id = id;

            Assert.AreEqual(id, hallUnderTest.Id);
        }

        [Test]
        public void TypeTest()
        {
            hallUnderTest.Type = type;

            Assert.AreEqual(type, hallUnderTest.Type);
        }

        [Test]
        public void FitnessCenterNameTest()
        {
            hallUnderTest.FitnessCenterName = fitnessCenterName;

            Assert.AreEqual(fitnessCenterName, hallUnderTest.FitnessCenterName);
        }

        #endregion tests
    }
}
