﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class MemberInboxRelsTest
    {
        #region Declarations 
        private MemberInboxRels miUnderTest;
        private string memberName = "m1";
        private string notification = "n1";
        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.miUnderTest = new MemberInboxRels();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new MemberInboxRels());
        }

        [Test]
        public void IdTest()
        {
            int id = 1;
            miUnderTest.Id = id;

            Assert.AreEqual(id, miUnderTest.Id);
        }

        [Test]
        public void NotificationTest()
        {
            miUnderTest.Notification = notification;

            Assert.AreEqual(notification, miUnderTest.Notification);
        }

        [Test]
        public void MemberNameTest()
        {
            miUnderTest.MemberName = memberName;

            Assert.AreEqual(memberName, miUnderTest.MemberName);
        }
        #endregion tests
    }
}
