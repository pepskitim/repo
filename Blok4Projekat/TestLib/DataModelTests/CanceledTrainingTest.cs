﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class CanceledTrainingTest
    {
        #region Declarations

        private CanceledTraining ctUnderTest;
        private String fitnessCenter = "test";
        private String training = "test";

        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.ctUnderTest = new CanceledTraining();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new CanceledTraining());
        }

        [Test]
        public void IdTest()
        {
            int id = 1;
            ctUnderTest.Id = id;

            Assert.AreEqual(id, ctUnderTest.Id);
        }

        [Test]
        public void FitnessCenterTest()
        {
            ctUnderTest.FitnessCenter = fitnessCenter;

            Assert.AreEqual(fitnessCenter, ctUnderTest.FitnessCenter);
        }

        [Test]
        public void TrainingTest()
        {
            ctUnderTest.Training = training;

            Assert.AreEqual(training, ctUnderTest.Training);
        }

        #endregion tests
    }
}
