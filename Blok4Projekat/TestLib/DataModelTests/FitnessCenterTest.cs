﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class FitnessCenterTest
    {
        #region Declarations

        private FitnessCenter fcUnderTest;
        private string name = "test";
        private string address = "test";
        private string telephone = "test";
        private int numOfHalls = 1;
        private string fcManagerUsername = "test";
        private Time workingTime = new Time(1, 0, 2, 0);

        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.fcUnderTest = new FitnessCenter();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new FitnessCenter());
        }
        
        [Test]
        public void NameTest()
        {
            fcUnderTest.Name = name;

            Assert.AreEqual(name, fcUnderTest.Name);
        }

        [Test]
        public void AddressTest()
        {
            fcUnderTest.Address = address;

            Assert.AreEqual(address, fcUnderTest.Address);
        }

        [Test]
        public void TelephoneTest()
        {
            fcUnderTest.Telephone = telephone;

            Assert.AreEqual(telephone, fcUnderTest.Telephone);
        }

        [Test]
        public void NumOfHallsTest()
        {
            fcUnderTest.NumOfHalls = numOfHalls;

            Assert.AreEqual(numOfHalls, fcUnderTest.NumOfHalls);
        }

        [Test]
        public void FcManagerUsernameTest()
        {
            fcUnderTest.FcManagerUsername = fcManagerUsername;

            Assert.AreEqual(fcManagerUsername, fcUnderTest.FcManagerUsername);
        }

        #endregion tests
    }
}
