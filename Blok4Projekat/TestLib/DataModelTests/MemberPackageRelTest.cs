﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class MemberPackageRelTest
    {
        #region Declarations 
        private MemberPackageRel mpUnderTest;
        private string memberName = "t1";
        private string packageName = "p1";
        private bool paid = false;
        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.mpUnderTest = new MemberPackageRel();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new MemberPackageRel());
        }

        [Test]
        public void IdTest()
        {
            int id = 1;
            mpUnderTest.Id = id;

            Assert.AreEqual(id, mpUnderTest.Id);
        }

        [Test]
        public void PackageNameTest()
        {
            mpUnderTest.packageName = packageName;

            Assert.AreEqual(packageName, mpUnderTest.packageName);
        }

        [Test]
        public void MemberNameTest()
        {
            mpUnderTest.memberName = memberName;

            Assert.AreEqual(memberName, mpUnderTest.memberName);
        }

        [Test]
        public void PaidTest()
        {
            mpUnderTest.Paid = paid;

            Assert.AreEqual(paid, mpUnderTest.Paid);
        }
        #endregion tests
    }
}
