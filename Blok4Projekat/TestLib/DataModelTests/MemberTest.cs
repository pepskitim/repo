﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class MemberTest
    {
        #region Declarations
        private Member memberUnderTest;
        private string username = "test";
        private string password = "test";
        private string name = "test";
        private string lastName = "test";
        private string address = "test";
        private double height = 180;
        private double weight = 90;
        private DateTime dateOfBirth = new DateTime(1994, 3, 3);
        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.memberUnderTest = new Member();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new Member());
        }

        [Test]
        public void ConstructorBaseTest()
        {
            Assert.DoesNotThrow(() => new Member(username, password, name, lastName, address, height, weight, dateOfBirth));
        }

        [Test]
        public void HeightTest()
        {
            memberUnderTest.Height = height;

            Assert.AreEqual(height, memberUnderTest.Height);
        }

        [Test]
        public void WeightTest()
        {
            memberUnderTest.Weight = weight;

            Assert.AreEqual(weight, memberUnderTest.Weight);
        }

        [Test]
        public void DateOfBirthTest()
        {
            memberUnderTest.DateOfBirth = dateOfBirth;

            Assert.AreEqual(dateOfBirth, memberUnderTest.DateOfBirth);
        }
        #endregion tests
    }
}
