﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class FitnessCenterManagerTest
    {
        #region Declarations

        private FitnessCenterManager fcmUnderTest;
        private string username = "test";
        private string password = "test";
        private string name = "test";
        private string lastName = "test";
        private string address = "test";
        private string telephone = "test";

        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.fcmUnderTest = new FitnessCenterManager();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new FitnessCenterManager());
        }

        [Test]
        public void ConstructorBaseTest()
        {
            Assert.DoesNotThrow(() => new FitnessCenterManager(username, password, name, lastName, address, telephone));
        }

        [Test]
        public void TelephoneTest()
        {
            fcmUnderTest.Telephone = telephone;

            Assert.AreEqual(telephone, fcmUnderTest.Telephone);
        }

        #endregion tests
    }
}
