﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class FitnessCenterNameAndServiceIPTest
    {
        #region Declarations

        private FitnessCenterNameAndServiceIp fcnasUnderTest;
        private string fitnessCenterName = "test";
        private string serviceIp = "test";

        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.fcnasUnderTest = new FitnessCenterNameAndServiceIp();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new FitnessCenterNameAndServiceIp());
        }

        [Test]
        public void FitnessCenterNameTest()
        {
            fcnasUnderTest.FitnessCenterName = fitnessCenterName;

            Assert.AreEqual(fitnessCenterName, fcnasUnderTest.FitnessCenterName);
        }

        [Test]
        public void ServiceIpTest()
        {
            fcnasUnderTest.ServiceIp = serviceIp;

            Assert.AreEqual(serviceIp, fcnasUnderTest.ServiceIp);
        }
        [Test]
        public void ConstructorWithParametersTest()
        {
            Assert.DoesNotThrow(() => new FitnessCenterNameAndServiceIp("name", "ip"));
        }

        #endregion tests
    }
}
