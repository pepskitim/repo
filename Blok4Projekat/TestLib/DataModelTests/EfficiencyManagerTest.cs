﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class EfficiencyManagerTest
    {
        #region Declarations

        private EfficiencyManager emUnderTest;
        private string username = "test";
        private string password = "test";
        private string name = "test";
        private string lastName = "test";
        private string address = "test";
        private string telephone = "test";

        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.emUnderTest = new EfficiencyManager();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new EfficiencyManager());
        }

        [Test]
        public void ConstructorBaseTest()
        {
            Assert.DoesNotThrow(() => new EfficiencyManager(username, password, name, lastName, address, telephone));
        }

        [Test]
        public void TelephoneTest()
        {
            emUnderTest.Telephone = telephone;

            Assert.AreEqual(telephone, emUnderTest.Telephone);
        }

        [Test]
        public void ToStringTest()
        {
            Assert.DoesNotThrow(() => emUnderTest.ToString());
        }

        #endregion tests
    }
}
