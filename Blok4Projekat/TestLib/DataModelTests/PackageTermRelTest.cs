﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class PackageTermRelTest
    {
        #region Declarations 
        private PackageTermRel ptUnderTest;
        private string packageName = "p1";
        private string termTitle = "t1";
        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.ptUnderTest = new PackageTermRel();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new PackageTermRel());
        }

        [Test]
        public void IdTest()
        {
            int id = 1;
            ptUnderTest.Id = id;

            Assert.AreEqual(id, ptUnderTest.Id);
        }

        [Test]
        public void PackageNameTest()
        {
            ptUnderTest.packageName = packageName;

            Assert.AreEqual(packageName, ptUnderTest.packageName);
        }

        [Test]
        public void TermTitleTest()
        {
            ptUnderTest.termTitle = termTitle;

            Assert.AreEqual(termTitle, ptUnderTest.termTitle);
        }
        #endregion tests
    }
}
