﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class MemberTermRelTest
    {
        #region Declarations 
        private MemberTermRel mtUnderTest;
        private string memberName = "m1";
        private string termTitle = "t1";
        private bool paid = false;
        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.mtUnderTest = new MemberTermRel();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new MemberTermRel());
        }

        [Test]
        public void IdTest()
        {
            int id = 1;
            mtUnderTest.Id = id;

            Assert.AreEqual(id, mtUnderTest.Id);
        }

        [Test]
        public void MemberNameTest()
        {
            mtUnderTest.memberName = memberName;

            Assert.AreEqual(memberName, mtUnderTest.memberName);
        }

        [Test]
        public void TermTitleTest()
        {
            mtUnderTest.termTitle = termTitle;

            Assert.AreEqual(termTitle, mtUnderTest.termTitle);
        }

        [Test]
        public void PaidTest()
        {
            mtUnderTest.Paid = paid;

            Assert.AreEqual(paid, mtUnderTest.Paid);
        }
        #endregion tests
    }
}
