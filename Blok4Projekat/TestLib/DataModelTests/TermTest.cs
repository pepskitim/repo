﻿using CommonModulLib.DataModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CommonModulLib.DataModel.Term;

namespace TestLib.DataModelTests
{
    [TestFixture]
    public class TermTest
    {
        #region Declarations
        private Term termUnderTest;
        private string title = "term1";
        private string day = "Monday";
        private Time timeRange = new Time(1, 0, 2, 0);
        private string apointedHall = "hall1";
        private TrainingLevel trainingLvl = TrainingLevel.BEGGINER;
        private string appointedCoach = "coach1";
        private double price = 300;
        #endregion Declarations

        #region setup

        [OneTimeSetUp]
        //[TestFixtureSetUp]
        public void SetupTest()
        {
            this.termUnderTest = new Term();
        }

        #endregion setup

        #region tests

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new Term());
        }

        [Test]
        public void TitleTest()
        {
            termUnderTest.Title = title;

            Assert.AreEqual(title, termUnderTest.Title);
        }

        [Test]
        public void DayTest()
        {
            termUnderTest.Day = day;

            Assert.AreEqual(day, termUnderTest.Day);
        }

        [Test]
        public void TimeRangeTest()
        {
            termUnderTest.TimeRange = timeRange;

            Assert.AreEqual(timeRange, termUnderTest.TimeRange);
        }


        [Test]
        public void AppointedHallTest()
        {
            termUnderTest.ApointedHall = apointedHall;

            Assert.AreEqual(apointedHall, termUnderTest.ApointedHall);
        }

        [Test]
        public void TrainingLvlTestTest()
        {
            termUnderTest.TrainingLvl = trainingLvl;

            Assert.AreEqual(trainingLvl, termUnderTest.TrainingLvl);
        }

        [Test]
        public void AppointedCoachTest()
        {
            termUnderTest.ApointedCoach = appointedCoach;

            Assert.AreEqual(appointedCoach, termUnderTest.ApointedCoach);
        }

        [Test]
        public void PriceTest()
        {
            termUnderTest.Price = price;

            Assert.AreEqual(price, termUnderTest.Price);
        }
        #endregion tests
    }
}
