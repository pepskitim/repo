﻿using CommonLib;
using CommonModulLib;
using CommonModulLib.DataModel;
using GUIClient;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace TestLib
{
    [TestFixture]
    [Apartment(ApartmentState.STA)]
    public class GuiClientModul1Test
    {
        private WCFClient clientProxyTest;
        private Time time;
        private DateTime date;
        private Coach coach;

        [OneTimeSetUp]
        public void SetupTest()
        {
            clientProxyTest = new WCFClient();
            time = new Time(1, 1, 1, 1);
            date = new DateTime();
            coach = new Coach();

            ConnectClass.clientProxy = Substitute.For<IContract>();

            clientProxyTest.factory = Substitute.For<IContract>();

            clientProxyTest.factory.Login("admin", "admin").Returns(AuthenticationAndAuthorization.ADMIN);
            clientProxyTest.factory.Login("ef", "ef").Returns(AuthenticationAndAuthorization.EFF_MANAGER);
            clientProxyTest.factory.Login("wrong", "wrong").Returns(AuthenticationAndAuthorization.NONE);

            clientProxyTest.factory.AddNewEfficiencyManager("ef", "ef", "ef", "ef", "ef", "123456789").Returns(true);
            clientProxyTest.factory.AddNewEfficiencyManager("ef1", "ef1", "ef1", "ef1", "ef1", "123456789").Returns(false); //pretpostavka da postoji

            clientProxyTest.factory.AddNewFitnessManager("man", "man", "man", "man", "man", "123123123").Returns(true);
            clientProxyTest.factory.AddNewFitnessManager("man1", "man1", "man1", "man1", "man1", "123123123").Returns(false);

            clientProxyTest.factory.AddNewFitnessCenter("fc", "fc", "fc", 3, "man", time).Returns(true);
            clientProxyTest.factory.AddNewFitnessCenter("fc1", "fc1", "fc1", 3, "man", time).Returns(false);

            clientProxyTest.factory.GetFitnessCenterManagerUsernames().Returns(new List<string>() { "man", "man1" });

            clientProxyTest.factory.GetFitnessCenters().Returns(new List<FitnessCenter>() {
                new FitnessCenter("fc", "fc", "fc", 3, "man", time),
                new FitnessCenter("fc1", "fc1", "fc1", 3, "man", time) });

            clientProxyTest.factory.EditFitnessCenter("fc", "fc2", "fc2", 3, "man").Returns(true);
            clientProxyTest.factory.EditFitnessCenter("fc2", "fc2", "fc2", 3, "man").Returns(false);

            clientProxyTest.factory.GetFitnessCenterNames().Returns(new List<string>() { "fc", "fc1" });

            clientProxyTest.factory.AddNewHall("sala1", TypeOfHall.FITNESS, "fc").Returns(true);
            clientProxyTest.factory.AddNewHall("sala2", TypeOfHall.FITNESS, "fc").Returns(false);

            clientProxyTest.factory.GetHallsBySelectedFitnessCenter("fc").Returns(new List<Hall>() {
                new Hall("sala1", TypeOfHall.FITNESS, "fc"),
                new Hall("sala2", TypeOfHall.FITNESS, "fc") });

            clientProxyTest.factory.DeleteHall("sala1").Returns(true);
            clientProxyTest.factory.DeleteHall("sala3").Returns(false);

            clientProxyTest.factory.EditHall("sala1", TypeOfHall.GYM, "fc").Returns(true);
            clientProxyTest.factory.EditHall("sala3", TypeOfHall.GYM, "fc").Returns(false);

            clientProxyTest.factory.GetFitnessCenterManagers().Returns(new List<FitnessCenterManager>() {
                new FitnessCenterManager("man", "man", "man", "man", "man", "123123123"),
                new FitnessCenterManager("man1", "man1", "man1", "man1", "man1", "123123123") });

            clientProxyTest.factory.EditEfficinencyManager("ef", "ef", "ef", "ef", "123123123").Returns(true);
            clientProxyTest.factory.EditEfficinencyManager("ef3", "ef", "ef", "ef", "123123123").Returns(false);

            clientProxyTest.factory.GetEfficinencyManagerByUsername("ef").Returns(new EfficiencyManager("ef", "ef", "ef", "ef", "ef", "123456789"));

            clientProxyTest.factory.GetEfficiencyManagers().Returns(new List<EfficiencyManager>() {
                new EfficiencyManager("ef", "ef", "ef", "ef", "ef", "123456789"),
                new EfficiencyManager("ef1", "ef1", "ef1", "ef1", "ef1", "123456789") });

            clientProxyTest.factory.GetFitnessCenterByName("fc").Returns(new FitnessCenter("fc", "fc", "fc", 3, "man", time));

            clientProxyTest.factory.GetCoachesByFitnessCenterName("fc").Returns(new List<Coach>() { new Coach("tr1", "tr1", "tr1", "tr1", "tr1", "tr1", date, "fc") });

            clientProxyTest.factory.GetTermsByFitnessCenterName("fc").Returns(new List<Term>() { new Term("term1", "Tuesday", time, "sala1", Term.TrainingLevel.ADVANCED, "tr1", 1000) });

            clientProxyTest.factory.GetAllCoachUsernames().Returns(new List<string>() { "tr1" });

            clientProxyTest.factory.GetTermsByCocachUsername("tr1").Returns(new List<Term>() { new Term("term1", "Tuesday", time, "sala1", Term.TrainingLevel.ADVANCED, "tr1", 1000) });

            clientProxyTest.factory.GetCoachesWaitForJob().Returns(new List<Coach>() { new Coach("tr2", "tr2", "tr2", "tr2", "tr2", "tr2", date, "fc") });

            clientProxyTest.factory.ApprovalCoachJob(coach, true).Returns(true);

            clientProxyTest.factory.GetCanceledTrainings().Returns("fc=3;fc1=0");

            clientProxyTest.factory.GetCoachesActivity().Returns("tr1=1");

            clientProxyTest.factory.GetFitnessCentersProfits().Returns("fc=1000;fc1=2000");

            clientProxyTest.factory.GetTrainingPackagePopularity().Returns("paket1=1");

            clientProxyTest.factory.CanDeleteHall("sala2").Returns(true);
            clientProxyTest.factory.CanDeleteHall("sala1").Returns(false);

            ConnectClass.clientProxy.GetFitnessCenters().Returns(new List<FitnessCenter>());
            ConnectClass.clientProxy.GetFitnessCenterNames().Returns(new List<string>());
            ConnectClass.clientProxy.GetEfficiencyManagers().Returns(new List<EfficiencyManager>());
            ConnectClass.clientProxy.GetFitnessCenterManagers().Returns(new List<FitnessCenterManager>());
            ConnectClass.clientProxy.GetEfficinencyManagerByUsername(string.Empty).Returns(new EfficiencyManager());
            ConnectClass.clientProxy.GetFitnessCenterManagerUsernames().Returns(new List<string>());
            ConnectClass.clientProxy.GetAllCoachUsernames().Returns(new List<string>());
            ConnectClass.clientProxy.GetCoachesWaitForJob().Returns(new List<Coach>());
        }

        [Test]
        public void LoginAdminOK()
        {
            AuthenticationAndAuthorization res = clientProxyTest.Login("admin", "admin");
            Assert.AreEqual(res, AuthenticationAndAuthorization.ADMIN);
        }

        [Test]
        public void LoginEfficiencyManagerOK()
        {
            AuthenticationAndAuthorization res = clientProxyTest.Login("ef", "ef");
            Assert.AreEqual(res, AuthenticationAndAuthorization.EFF_MANAGER);
        }

        [Test]
        public void LoginFail()
        {
            AuthenticationAndAuthorization res = clientProxyTest.Login("wrong", "wrong");
            Assert.AreEqual(res, AuthenticationAndAuthorization.NONE);
        }

        [Test]
        public void AddNewEfficiencyManagerOK()
        {
            bool res = clientProxyTest.AddNewEfficiencyManager("ef", "ef", "ef", "ef", "ef", "123456789");
            Assert.IsTrue(res);
        }

        [Test]
        public void AddNewEfficiencyManagerFail()
        {
            bool res = clientProxyTest.AddNewEfficiencyManager("ef1", "ef1", "ef1", "ef1", "ef1", "123456789");
            Assert.IsFalse(res);
        }

        [Test]
        public void AddNewFitnessManagerOK()
        {
            bool res = clientProxyTest.AddNewFitnessManager("man", "man", "man", "man", "man", "123123123");
            Assert.IsTrue(res);
        }

        [Test]
        public void AddNewFitnessManagerFail()
        {
            bool res = clientProxyTest.AddNewFitnessManager("man1", "man1", "man1", "man1", "man1", "123123123");
            Assert.IsFalse(res);
        }

        [Test]
        public void AddNewFitnessCenterOK()
        {
            bool res = clientProxyTest.AddNewFitnessCenter("fc", "fc", "fc", 3, "man", time);
            Assert.IsTrue(res);
        }

        [Test]
        public void AddNewFitnessCenterFail()
        {
            bool res = clientProxyTest.AddNewFitnessCenter("fc1", "fc1", "fc1", 3, "man", time);
            Assert.IsFalse(res);
        }

        [Test]
        public void GetFitnessCenterManagerUsernamesTest()
        {
            List<string> res = clientProxyTest.GetFitnessCenterManagerUsernames();
            Assert.AreEqual("man", res[0]);
            Assert.AreEqual("man1", res[1]);
        }

        [Test]
        public void GetFitnessCentersTest()
        {
            List<FitnessCenter> result = clientProxyTest.GetFitnessCenters();
            Assert.AreEqual("fc", result[0].Name);
            Assert.AreEqual("fc", result[0].Address);
            Assert.AreEqual("fc", result[0].Telephone);
            Assert.AreEqual(3, result[0].NumOfHalls);
            Assert.AreEqual(time, result[0].WorkingTime);
            Assert.AreEqual("man", result[0].FcManagerUsername);

            Assert.AreEqual("fc1", result[1].Name);
            Assert.AreEqual("fc1", result[1].Address);
            Assert.AreEqual("fc1", result[1].Telephone);
            Assert.AreEqual(3, result[0].NumOfHalls);
            Assert.AreEqual(time, result[1].WorkingTime);
            Assert.AreEqual("man", result[1].FcManagerUsername);
        }

        [Test]
        public void EditFitnessCenterTestOK()
        {
            bool result = clientProxyTest.EditFitnessCenter("fc", "fc2", "fc2", 3, "man");
            Assert.IsTrue(result);
        }

        [Test]
        public void EditFitnessCenterTestFail()
        {
            bool result = clientProxyTest.EditFitnessCenter("fc2", "fc2", "fc2", 3, "man");
            Assert.IsFalse(result);
        }

        [Test]
        public void GetFitnessCenterNamesTest()
        {
            List<string> result = clientProxyTest.GetFitnessCenterNames();
            Assert.AreEqual("fc", result[0]);
            Assert.AreEqual("fc1", result[1]);
        }

        [Test]
        public void AddNewHallTestOK()
        {
            bool result = clientProxyTest.AddNewHall("sala1", TypeOfHall.FITNESS, "fc");
            Assert.IsTrue(result);
        }

        [Test]
        public void AddNewHallTestFail()
        {
            bool result = clientProxyTest.AddNewHall("sala2", TypeOfHall.FITNESS, "fc");
            Assert.IsFalse(result);
        }

        [Test]
        public void GetHallsBySelectedFitnessCenterTest()
        {
            List<Hall> result = clientProxyTest.GetHallsBySelectedFitnessCenter("fc");
            Assert.AreEqual("sala1", result[0].Id);
            Assert.AreEqual(TypeOfHall.FITNESS, result[0].Type);
            Assert.AreEqual("fc", result[0].FitnessCenterName);

            Assert.AreEqual("sala2", result[1].Id);
            Assert.AreEqual(TypeOfHall.FITNESS, result[1].Type);
            Assert.AreEqual("fc", result[1].FitnessCenterName);
        }

        public void DeleteHallTestOk()
        {
            bool result = clientProxyTest.DeleteHall("sala1");
            Assert.IsTrue(result);
        }

        public void DeleteHallTestFail()
        {
            bool result = clientProxyTest.DeleteHall("sala3");
            Assert.IsFalse(result);
        }

        [Test]
        public void EditHallestOK()
        {
            bool result = clientProxyTest.EditHall("sala1", TypeOfHall.GYM, "fc");
            Assert.IsTrue(result);
        }

        [Test]
        public void EditHallestFail()
        {
            bool result = clientProxyTest.EditHall("sala3", TypeOfHall.GYM, "fc");
            Assert.IsFalse(result);
        }

        [Test]
        public void GetFitnessCenterManagersTest()
        {
            List<FitnessCenterManager> result = clientProxyTest.GetFitnessCenterManagers();
            Assert.AreEqual("man", result[0].FirstName);
            Assert.AreEqual("man", result[0].LastName);
            Assert.AreEqual("man", result[0].Username);
            Assert.AreEqual("man", result[0].Password);
            Assert.AreEqual("man", result[0].Address);
            Assert.AreEqual("123123123", result[0].Telephone);

            Assert.AreEqual("man1", result[1].FirstName);
            Assert.AreEqual("man1", result[1].LastName);
            Assert.AreEqual("man1", result[1].Username);
            Assert.AreEqual("man1", result[1].Password);
            Assert.AreEqual("man1", result[1].Address);
            Assert.AreEqual("123123123", result[1].Telephone);
        }

        [Test]
        public void EditEfficinencyManagerTestOK()
        {
            bool result = clientProxyTest.EditEfficinencyManager("ef", "ef", "ef", "ef", "123123123");
            Assert.IsTrue(result);
        }

        [Test]
        public void EditEfficinencyManagerTestFail()
        {
            bool result = clientProxyTest.EditEfficinencyManager("ef3", "ef", "ef", "ef", "123123123");
            Assert.IsFalse(result);
        }

        [Test]
        public void GetEfficinencyManagerByUsernameTest()
        {
            EfficiencyManager result = clientProxyTest.GetEfficinencyManagerByUsername("ef");
            Assert.AreEqual("ef", result.Username);
            Assert.AreEqual("ef", result.Password);
            Assert.AreEqual("ef", result.FirstName);
            Assert.AreEqual("ef", result.LastName);
            Assert.AreEqual("ef", result.Address);
            Assert.AreEqual("123456789", result.Telephone);
        }

        [Test]
        public void GetEfficiencyManagersTest()
        {
            List<EfficiencyManager> result = clientProxyTest.GetEfficiencyManagers();
            Assert.AreEqual("ef", result[0].Username);
            Assert.AreEqual("ef", result[0].Password);
            Assert.AreEqual("ef", result[0].FirstName);
            Assert.AreEqual("ef", result[0].LastName);
            Assert.AreEqual("ef", result[0].Address);
            Assert.AreEqual("123456789", result[0].Telephone);

            Assert.AreEqual("ef1", result[1].Username);
            Assert.AreEqual("ef1", result[1].Password);
            Assert.AreEqual("ef1", result[1].FirstName);
            Assert.AreEqual("ef1", result[1].LastName);
            Assert.AreEqual("ef1", result[1].Address);
            Assert.AreEqual("123456789", result[1].Telephone);
        }

        [Test]
        public void GetFitnessCenterByNameTest()
        {
            FitnessCenter result = clientProxyTest.GetFitnessCenterByName("fc");
            Assert.AreEqual("fc", result.Name);
            Assert.AreEqual("fc", result.Address);
            Assert.AreEqual("fc", result.Telephone);
            Assert.AreEqual(3, result.NumOfHalls);
            Assert.AreEqual(time, result.WorkingTime);
            Assert.AreEqual("man", result.FcManagerUsername);
        }

        [Test]
        public void GetCoachesByFitnessCenterNameTest()
        {
            List<Coach> result = clientProxyTest.GetCoachesByFitnessCenterName("fc");
            Assert.AreEqual("tr1", result[0].Username);
            Assert.AreEqual("tr1", result[0].Password);
            Assert.AreEqual("tr1", result[0].FirstName);
            Assert.AreEqual("tr1", result[0].LastName);
            Assert.AreEqual("tr1", result[0].Address);
            Assert.AreEqual("tr1", result[0].UniversityDegree);
            Assert.AreEqual(date, result[0].DateOfBirth);
            Assert.AreEqual("fc", result[0].FitnessCenter);
        }

        [Test]
        public void GetTermsByFitnessCenterNameTest()
        {
            List<Term> result = clientProxyTest.GetTermsByCocachUsername("tr1");
            Assert.AreEqual("term1", result[0].Title);
            Assert.AreEqual("Tuesday", result[0].Day);
            Assert.AreEqual("sala1", result[0].ApointedHall);
            Assert.AreEqual(time, result[0].TimeRange);
            Assert.AreEqual(Term.TrainingLevel.ADVANCED, result[0].TrainingLvl);
            Assert.AreEqual("tr1", result[0].ApointedCoach);
        }

        [Test]
        public void GetAllCoachUsernamesTest()
        {
            List<string> result = clientProxyTest.GetAllCoachUsernames();
            Assert.AreEqual("tr1", result[0]);
        }

        [Test]
        public void GetTermsByCocachUsernameTest()
        {
            List<Term> result = clientProxyTest.GetTermsByCocachUsername("tr1");
            Assert.AreEqual("term1", result[0].Title);
            Assert.AreEqual("Tuesday", result[0].Day);
            Assert.AreEqual("sala1", result[0].ApointedHall);
            Assert.AreEqual(time, result[0].TimeRange);
            Assert.AreEqual(Term.TrainingLevel.ADVANCED, result[0].TrainingLvl);
            Assert.AreEqual("tr1", result[0].ApointedCoach);
        }

        [Test]
        public void GetCoachesWaitForJobTest()
        {
            List<Coach> result = clientProxyTest.GetCoachesWaitForJob();
            Assert.AreEqual("tr2", result[0].Username);
            Assert.AreEqual("tr2", result[0].Password);
            Assert.AreEqual("tr2", result[0].FirstName);
            Assert.AreEqual("tr2", result[0].LastName);
            Assert.AreEqual("tr2", result[0].Address);
            Assert.AreEqual("tr2", result[0].UniversityDegree);
            Assert.AreEqual(date, result[0].DateOfBirth);
            Assert.AreEqual("fc", result[0].FitnessCenter);
        }

        [Test]
        public void ApprovalCoachJobTestOK()
        {
            bool result = clientProxyTest.ApprovalCoachJob(coach, true);
            Assert.IsTrue(result);
        }

        [Test]
        public void GetCanceledTrainingsTest()
        {
            string result = clientProxyTest.GetCanceledTrainings();
            Assert.AreEqual("fc=3;fc1=0", result);
        }

        [Test]
        public void GetCoachesActivityTest()
        {
            string result = clientProxyTest.GetCoachesActivity();
            Assert.AreEqual("tr1=1", result);
        }

        [Test]
        public void GetFitnessCentersProfitsTest()
        {
            string result = clientProxyTest.GetFitnessCentersProfits();
            Assert.AreEqual("fc=1000;fc1=2000", result);
        }

        [Test]
        public void GetTrainingPackagePopularityTest()
        {
            string result = clientProxyTest.GetTrainingPackagePopularity();
            Assert.AreEqual("paket1=1", result);
        }

        [Test]
        public void CanDeleteHallTestOK()
        {
            bool result = clientProxyTest.CanDeleteHall("sala2");
            Assert.IsTrue(result);
        }

        [Test]
        public void CanDeleteHallTestFail()
        {
            bool result = clientProxyTest.CanDeleteHall("sala1");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void StringToDictionaryTest()
        {
            Dictionary<string, string> result = Parser.StringToDictionary("tre1=0;tre2=1");
            Assert.AreEqual(result["tre1"], "0");
            Assert.AreEqual(result["tre2"], "1");
        }

        [Test]
        public void StringForReportTest()
        {
            Assert.DoesNotThrow(() => Parser.StringForReport("tre1=0;tre2=1"));
        }

        [Test]
        
        public void AdminWindowContructor()
        {
            Assert.DoesNotThrow(() => new Admin());
        }

        [Test]
        public void EditEffManagerWindowContructor()
        {
            Assert.DoesNotThrow(() => new EditEffManagerWindow(string.Empty, new TextBox()));
        }

        [Test]
        public void EditFitnessCenterWindowConstructor()
        {
            Assert.DoesNotThrow(() => new EditFitnessCenterWindow(new ComboBox(), new DataGrid(), new FitnessCenter()));
        }

        [Test]
        public void EditHallWindowConstructor()
        {
            Assert.DoesNotThrow(() => new EditHallWindow(string.Empty, string.Empty, TypeOfHall.FITNESS));
        }

        [Test]
        public void EfficiencyManagerWindowConstructor()
        {
            Assert.DoesNotThrow(() => new EfficiencyManagerWindow(string.Empty));
        }

        [Test]
        public void LoginWindowConstructor()
        {
            Assert.DoesNotThrow(() => new LogInWindow());
        }

        [Test]
        public void NewFCWindowContructor()
        {
            Assert.DoesNotThrow(() => new NewFitnessCenterWindow(new ComboBox(), new DataGrid()));
        }

        [Test]
        public void NewHallWindowContructor()
        {
            Assert.DoesNotThrow(() => new NewHallWindow());
        }

        [Test]
        public void NewManagerWindowContructor()
        {
            Assert.DoesNotThrow(() => new NewManagerWindow(TypeOfManager.EfficiencyManager, new DataGrid()));
        }
    }
}
