﻿using CommonLib;
using CommonModulLib;
using CommonModulLib.DataModel;
using Modul1Service;
using Modul1Service.Access;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib
{
    [TestFixture]
    public class Modul1ServiceTest
    {
        private Contract serviceUnderTest;
        private Time fitnessCenterTime;
        private DateTime time;
        private Coach coach;
        private IModul2Contract proxy;

        [OneTimeSetUp]
        public void SetupTest()
        {
            serviceUnderTest = new Contract();
            Modul1DB.Instance = Substitute.For<IModul1DB>();
            fitnessCenterTime = new Time(1, 1, 1, 1);
            proxy = Substitute.For<IModul2Contract>();
            time = new DateTime();
            coach = new Coach("tr2", "tr2", "tr2", "tr2", "tr2", "tr2", time, "fc");
            ConnectClass.Modul2ClientProxies = new Dictionary<string, IModul2Contract>();
            ConnectClass.Modul2ClientProxies.Add("test", proxy);           

            Modul1DB.Instance.Login("admin", "admin").Returns(AuthenticationAndAuthorization.ADMIN);
            Modul1DB.Instance.Login("ef", "ef").Returns(AuthenticationAndAuthorization.EFF_MANAGER);
            Modul1DB.Instance.Login("wrong", "wrong").Returns(AuthenticationAndAuthorization.NONE);

            Modul1DB.Instance.AddNewEfficiencyManager("ef", "ef", "ef", "ef", "ef", "123456789").Returns(true);
            Modul1DB.Instance.AddNewEfficiencyManager("ef1", "ef1", "ef1", "ef1", "ef1", "123456789").Returns(false); //pretopostavka da postoji

            Modul1DB.Instance.AddNewFitnessManager("man", "man", "man", "man", "man", "123123123").Returns(true);
            Modul1DB.Instance.AddNewFitnessManager("man1", "man1", "man1", "man1", "man1", "123123123").Returns(false); //pretopostavka da postoji

            Modul1DB.Instance.AddNewFitnessCenter("fc", "fc", "fc", 3, "man", fitnessCenterTime).Returns(true);
            Modul1DB.Instance.AddNewFitnessCenter("fc1", "fc1", "fc1", 3, "man", fitnessCenterTime).Returns(false); //pretopostavka da postoji

            Modul1DB.Instance.GetFitnessCenterManagerUsernames().Returns(new List<string>() { "man", "man1" });

            Modul1DB.Instance.GetFitnessCenters().Returns(new List<FitnessCenter>() {
                new FitnessCenter("fc", "fc", "fc", 3, "man", fitnessCenterTime),
                new FitnessCenter("fc1", "fc1", "fc1", 3, "man", fitnessCenterTime) });

            Modul1DB.Instance.EditFitnessCenter("fc", "fc2", "fc2", 3, "man").Returns(true);
            Modul1DB.Instance.EditFitnessCenter("fc2", "fc2", "fc2", 3, "man").Returns(false); //pretopostavka da postoji

            Modul1DB.Instance.GetFitnessCenterNames().Returns(new List<string>() { "fc", "fc1" });

            Modul1DB.Instance.AddNewHall("sala1", TypeOfHall.FITNESS, "fc").Returns(true);
            Modul1DB.Instance.AddNewHall("sala2", TypeOfHall.FITNESS, "fc").Returns(false); //pretopostavka da postoji

            Modul1DB.Instance.GetHallsBySelectedFitnessCenter("fc").Returns(new List<Hall>() {
                new Hall("sala1", TypeOfHall.FITNESS, "fc"),
                new Hall("sala2", TypeOfHall.FITNESS, "fc") });

            Modul1DB.Instance.DeleteHall("sala1").Returns(true);
            Modul1DB.Instance.DeleteHall("sala3").Returns(false);

            Modul1DB.Instance.EditHall("sala1", TypeOfHall.GYM, "fc").Returns(true);
            Modul1DB.Instance.EditHall("sala3", TypeOfHall.GYM, "fc").Returns(false);

            Modul1DB.Instance.GetFitnessCenterManagers().Returns(new List<FitnessCenterManager>() {
                new FitnessCenterManager("man", "man", "man", "man", "man", "123123123"),
                new FitnessCenterManager("man1", "man1", "man1", "man1", "man1", "123123123") });

            Modul1DB.Instance.EditEfficinencyManager("ef", "ef", "ef", "ef", "123123123").Returns(true);
            Modul1DB.Instance.EditEfficinencyManager("ef3", "ef", "ef", "ef", "123123123").Returns(false);

            Modul1DB.Instance.GetEfficinencyManagerByUsername("ef").Returns(new EfficiencyManager("ef", "ef", "ef", "ef", "ef", "123456789"));

            Modul1DB.Instance.GetEfficiencyManagers().Returns(new List<EfficiencyManager>() {
                new EfficiencyManager("ef", "ef", "ef", "ef", "ef", "123456789"),
                new EfficiencyManager("ef1", "ef1", "ef1", "ef1", "ef1", "123456789") });

            Modul1DB.Instance.GetIpAddressByFitnessCenterName(coach.FitnessCenter).Returns("test");


            proxy.GetCanceledTrainings().Returns("fc=3;fc1=0");

            Modul1DB.Instance.GetFitnessCenterByName("fc").Returns(new FitnessCenter("fc", "fc", "fc", 3, "man", fitnessCenterTime)); //lose odavde

            proxy.GetCoachesByFitnessCenterName("fc").Returns(new List<Coach>() { new Coach("tr1", "tr1", "tr1", "tr1", "tr1", "tr1", time, "fc") });

            proxy.GetTermsByFitnessCenterName("fc").Returns(new List<Term>() { new Term("term1", "Tuesday", fitnessCenterTime, "sala1", Term.TrainingLevel.ADVANCED, "tr1", 1000) });

            proxy.GetAllCoachUsernames().Returns(new List<string>() { "tr1" });

            proxy.GetTermsByCocachUsername("tr1").Returns(new List<Term>() { new Term("term1", "Tuesday", fitnessCenterTime, "sala1", Term.TrainingLevel.ADVANCED, "tr1", 1000) });

            proxy.GetCoachesWaitForJob().Returns(new List<Coach>() { new Coach("tr2", "tr2", "tr2", "tr2", "tr2", "tr2", time, "fc") });

            proxy.ApprovalCoachJob(coach, true).Returns(true);

            proxy.GetCanceledTrainings().Returns("fc=3;fc1=0");

            proxy.GetCoachesActivity().Returns("tr1=1");

            proxy.GetFitnessCentersProfits().Returns("fc=1000;fc1=2000");

            proxy.GetTrainingPackagePopularity().Returns("paket1=1");

            proxy.CanDeleteHall("sala2").Returns(true);
            proxy.CanDeleteHall("sala1").Returns(false);
        }

        [Test]
        public void LoginTestOK()
        {
            AuthenticationAndAuthorization result = serviceUnderTest.Login("admin", "admin");
            Assert.AreEqual(AuthenticationAndAuthorization.ADMIN, result);
        }
        [Test]
        public void LoginTestFailed()
        {
            AuthenticationAndAuthorization result = serviceUnderTest.Login("wrong", "wrong");
            Assert.AreEqual(AuthenticationAndAuthorization.NONE, result);
        }

        [Test]
        public void AddNewEfficiencyManagerOK()
        {
            bool result = serviceUnderTest.AddNewEfficiencyManager("ef", "ef", "ef", "ef", "ef", "123456789");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void AddNewEfficiencyManagerFailed()
        {
            bool result = serviceUnderTest.AddNewEfficiencyManager("ef1", "ef1", "ef1", "ef1", "ef1", "123456789");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void AddNewFitnessManagerOK()
        {
            bool result = serviceUnderTest.AddNewFitnessManager("man", "man", "man", "man", "man", "123123123");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void AddNewFitnessManagerFailed()
        {
            bool result = serviceUnderTest.AddNewFitnessManager("man1", "man1", "man1", "man1", "man1", "123123123");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void AddNewFitnessCenterOK()
        {
            bool result = serviceUnderTest.AddNewFitnessCenter("fc", "fc", "fc", 3, "man", fitnessCenterTime);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void AddNewFitnessCenterFailed()
        {
            bool result = serviceUnderTest.AddNewFitnessCenter("fc1", "fc1", "fc1", 3, "man", fitnessCenterTime);
            Assert.AreEqual(false, result);
        }

        [Test]
        public void GetFitnessCenterManagerUsernamesOK()
        {
            List<string> expected = new List<string>() { "man", "man1" }; 
            List<string> actual = serviceUnderTest.GetFitnessCenterManagerUsernames();
            Assert.AreEqual(expected[0], actual[0]);
            Assert.AreEqual(expected[1], actual[1]);
        }

        [Test]
        public void GetFitnessCenterManagerUsernamesFailed()
        {
            List<string> expected = new List<string>() { "man13", "man1141" };
            List<string> actual = serviceUnderTest.GetFitnessCenterManagerUsernames();
            Assert.AreNotEqual(expected[0], actual[0]);
            Assert.AreNotEqual(expected[1], actual[1]);
        }

        [Test]
        public void GetFitnessCentersOK()
        {
            List<FitnessCenter> expected = new List<FitnessCenter>() {
                new FitnessCenter("fc", "fc", "fc", 3, "man", new Time(1, 1, 1, 1)),
                new FitnessCenter("fc1", "fc1", "fc1", 3, "man", new Time(1, 1, 1, 1)) };
            List<FitnessCenter> actual = serviceUnderTest.GetFitnessCenters();
            Assert.AreEqual(expected[0].Name, actual[0].Name);
            Assert.AreEqual(expected[1].Name, actual[1].Name);
        }

        [Test]
        public void GetFitnessCentersFailed()
        {
            List<FitnessCenter> expected = new List<FitnessCenter>() {
                new FitnessCenter("fc2", "fc2", "fc2", 3, "man2", new Time(1, 1, 1, 1)),
                new FitnessCenter("fc3", "fc3", "fc3", 3, "man4", new Time(1, 1, 1, 1)) };
            List<FitnessCenter> actual = serviceUnderTest.GetFitnessCenters();
            Assert.AreNotEqual(expected[0].Name, actual[0].Name);
            Assert.AreNotEqual(expected[1].Name, actual[1].Name);
        }

        [Test]
        public void EditFitnessCenterOK()
        {
            bool result = serviceUnderTest.EditFitnessCenter("fc", "fc2", "fc2", 3, "man");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void EditFitnessCenterFailed()
        {
            bool result = serviceUnderTest.EditFitnessCenter("fc2", "fc2", "fc2", 3, "man");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void GetFitnessCenterNamesOK()
        {
            List<string> expected = new List<string>() { "fc", "fc1" };
            List<string> actual = serviceUnderTest.GetFitnessCenterNames();

            Assert.AreEqual(expected[0], actual[0]);
            Assert.AreEqual(expected[1], actual[1]);
        }

        [Test]
        public void GetFitnessCenterNamesFailed()
        {
            List<string> expected = new List<string>() { "fc3", "fc14" };
            List<string> actual = serviceUnderTest.GetFitnessCenterNames();

            Assert.AreNotEqual(expected[0], actual[0]);
            Assert.AreNotEqual(expected[1], actual[1]);
        }

        [Test]
        public void AddNewHallOK()
        {
            bool result = serviceUnderTest.AddNewHall("sala1", TypeOfHall.FITNESS, "fc");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void AddNewHallFailed()
        {
            bool result = serviceUnderTest.AddNewHall("sala2", TypeOfHall.FITNESS, "fc");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void GetHallsBySelectedFitnessCenterOK()
        {
            List<Hall> expected = new List<Hall>() {
                new Hall("sala1", TypeOfHall.FITNESS, "fc"),
                new Hall("sala2", TypeOfHall.FITNESS, "fc") };
            List<Hall> actual = serviceUnderTest.GetHallsBySelectedFitnessCenter("fc");

            Assert.AreEqual(expected[0].Id, actual[0].Id);
            Assert.AreEqual(expected[1].Id, actual[1].Id);
        }

        [Test]
        public void GetHallsBySelectedFitnessCenterFailed()
        {
            List<Hall> expected = new List<Hall>() {
                new Hall("sala13", TypeOfHall.FITNESS, "fc"),
                new Hall("sala22", TypeOfHall.FITNESS, "fc") };
            List<Hall> actual = serviceUnderTest.GetHallsBySelectedFitnessCenter("fc");

            Assert.AreNotEqual(expected[0].Id, actual[0].Id);
            Assert.AreNotEqual(expected[1].Id, actual[1].Id);
        }

        [Test]
        public void DeleteHallOK()
        {
            bool result = serviceUnderTest.DeleteHall("sala1");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void DeleteHallFailed()
        {
            bool result = serviceUnderTest.DeleteHall("sala3");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void EditHallOK()
        {
            bool result = serviceUnderTest.EditHall("sala1", TypeOfHall.GYM, "fc");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void EditHallFailed()
        {
            bool result = serviceUnderTest.EditHall("sala3", TypeOfHall.GYM, "fc");
            Assert.AreEqual(false, result);
        }


        [Test]
        public void GetFitnessCenterManagersOK()
        {
            List<FitnessCenterManager> expected = new List<FitnessCenterManager>() {
                new FitnessCenterManager("man", "man", "man", "man", "man", "123123123"),
                new FitnessCenterManager("man1", "man1", "man1", "man1", "man1", "123123123") };
            List<FitnessCenterManager> actual = serviceUnderTest.GetFitnessCenterManagers();

            Assert.AreEqual(expected[0].Username, actual[0].Username);
            Assert.AreEqual(expected[1].Username, actual[1].Username);
        }

        [Test]
        public void GetFitnessCenterManagersFailed()
        {
            List<FitnessCenterManager> expected = new List<FitnessCenterManager>() {
                new FitnessCenterManager("man2", "man2", "man2", "man2", "man2", "123123123"),
                new FitnessCenterManager("man3", "man13", "man13", "man13", "man13", "123123123") };
            List<FitnessCenterManager> actual = serviceUnderTest.GetFitnessCenterManagers();

            Assert.AreNotEqual(expected[0].Username, actual[0].Username);
            Assert.AreNotEqual(expected[1].Username, actual[1].Username);
        }

        [Test]
        public void EditEfficinencyManagerrOK()
        {
            bool result = serviceUnderTest.EditEfficinencyManager("ef", "ef", "ef", "ef", "123123123");
            Assert.AreEqual(true, result);
        }

        [Test]
        public void EditEfficinencyManagerFailed()
        {
            bool result = serviceUnderTest.EditEfficinencyManager("ef3", "ef", "ef", "ef", "123123123");
            Assert.AreEqual(false, result);
        }

        [Test]
        public void GetEfficinencyManagerByUsernameOK()
        {
            EfficiencyManager actual = new EfficiencyManager("ef", "ef", "ef", "ef", "ef", "123456789");
            EfficiencyManager expected = serviceUnderTest.GetEfficinencyManagerByUsername("ef");

            Assert.AreEqual(expected.Username, actual.Username);
        }

        [Test]
        public void GetEfficinencyManagerByUsernameFailed()
        {
            EfficiencyManager actual = new EfficiencyManager("ef2", "ef2", "ef2", "ef2", "ef", "123456789");
            EfficiencyManager expected = serviceUnderTest.GetEfficinencyManagerByUsername("ef");

            Assert.AreNotEqual(expected.Username, actual.Username);
        }

        [Test]
        public void GetEfficiencyManagersOK()
        {
            List<EfficiencyManager> actual = new List<EfficiencyManager>() {
                new EfficiencyManager("ef", "ef", "ef", "ef", "ef", "123456789"),
                new EfficiencyManager("ef1", "ef1", "ef1", "ef1", "ef1", "123456789") };
            List<EfficiencyManager> expected = serviceUnderTest.GetEfficiencyManagers();

            Assert.AreEqual(expected[0].Username, actual[0].Username);
            Assert.AreEqual(expected[1].Username, actual[1].Username);
        }

        [Test]
        public void GetEfficiencyManagersFailed()
        {
            List<EfficiencyManager> expected = new List<EfficiencyManager>() {
                new EfficiencyManager("ef2", "ef2", "ef2", "ef2", "ef2", "123456789"),
                new EfficiencyManager("ef4", "ef3", "ef13", "ef13", "ef1", "123456789") };
            List<EfficiencyManager> actual = serviceUnderTest.GetEfficiencyManagers();

            Assert.AreNotEqual(expected[0].Username, actual[0].Username);
            Assert.AreNotEqual(expected[1].Username, actual[1].Username);
        }

        [Test]
        public void GetFitnessCenterByNameOK()
        {
            FitnessCenter expected = new FitnessCenter("fc", "fc", "fc", 3, "man", new Time(1, 1, 1, 1));
            FitnessCenter actual = serviceUnderTest.GetFitnessCenterByName("fc");

            Assert.AreEqual(expected.Name, actual.Name);
        }

        [Test]
        public void GetFitnessCenterByNameFailed()
        {
            FitnessCenter expected = new FitnessCenter("fc12", "fc12", "fc12", 3, "man", new Time(1, 1, 1, 1));
            FitnessCenter actual = serviceUnderTest.GetFitnessCenterByName("fc");

            Assert.AreNotEqual(expected.Name, actual.Name);
        }


        [Test]
        public void GetCoachesByFitnessCenterNameOK()
        {
            List<Coach> expected = new List<Coach>() { new Coach("tr1", "tr1", "tr1", "tr1", "tr1", "tr1", time, "fc") };
            List<Coach> actual = serviceUnderTest.GetCoachesByFitnessCenterName("fc");
            Assert.AreEqual(expected[0].Username, actual[0].Username);
        }

        [Test]
        public void GetCoachesByFitnessCenterNameFailed()
        {
            List<Coach> expected = new List<Coach>() { new Coach("tr2", "tr2", "tr2", "tr2", "tr2", "tr2", new DateTime(), "fc") };
            List<Coach> actual = serviceUnderTest.GetCoachesByFitnessCenterName("fc");
            Assert.AreNotEqual(expected[0].Username, actual[0].Username);
        }

        [Test]
        public void GetTermsByFitnessCenterNameOK()
        {
            List<Term> expected = new List<Term>() { new Term("term1", "Tuesday", new Time(1, 1, 1, 1), "sala1", Term.TrainingLevel.ADVANCED, "tr1", 1000) };
            List<Term> actual = serviceUnderTest.GetTermsByFitnessCenterName("fc");
            Assert.AreEqual(expected[0].Title, actual[0].Title);
        }

        [Test]
        public void GetTermsByFitnessCenterNameFailed()
        {
            List<Term> expected = new List<Term>() { new Term("term8", "Tuesday", new Time(1, 1, 1, 1), "sala2", Term.TrainingLevel.ADVANCED, "tr1", 1000) };
            List<Term> actual = serviceUnderTest.GetTermsByFitnessCenterName("fc");
            Assert.AreNotEqual(expected[0].Title, actual[0].Title);
        }

        [Test]
        public void GetAllCoachUsernamesOK()
        {
            List<string> expected = new List<string>() { "tr1" };
            List<string> actual = serviceUnderTest.GetAllCoachUsernames();
            Assert.AreEqual(actual[0], "tr1");
        }

        [Test]
        public void GetAllCoachUsernamesFailed()
        {
            List<string> expected = new List<string>() { "tr3" };
            List<string> actual = serviceUnderTest.GetAllCoachUsernames();
            Assert.AreNotEqual(expected[0], actual[0]);
        }

        [Test]
        public void GetTermsByCocachUsernameOK()
        {
            List<Term> expected = new List<Term>() { new Term("term1", "Tuesday", fitnessCenterTime, "sala1", Term.TrainingLevel.ADVANCED, "tr1", 1000) };
            List<Term> actual = serviceUnderTest.GetTermsByCocachUsername("tr1");
            Assert.AreEqual(expected[0].Title, actual[0].Title);
        }

        [Test]
        public void GetTermsByCocachUsernameFailed()
        {
            List<Term> expected = new List<Term>() { new Term("term3", "Tuesday", fitnessCenterTime, "sala1", Term.TrainingLevel.ADVANCED, "tr1", 1000) };
            List<Term> actual = serviceUnderTest.GetTermsByCocachUsername("tr1");
            Assert.AreNotEqual(expected[0].Title, actual[0].Title);
        }

        [Test]
        public void GetCoachesWaitForJobOK()
        {
            List<Coach> expected = new List<Coach>() { new Coach("tr2", "tr2", "tr2", "tr2", "tr2", "tr2", time, "fc") };
            List<Coach> actual = serviceUnderTest.GetCoachesWaitForJob();
            Assert.AreEqual(expected[0].Username, actual[0].Username);
        }

        [Test]
        public void GetCoachesWaitForJobFailed()
        {
            List<Coach> expected = new List<Coach>() { new Coach("tr4", "tr4", "tr4", "tr2", "tr2", "tr2", time, "fc") };
            List<Coach> actual = serviceUnderTest.GetCoachesWaitForJob();
            Assert.AreNotEqual(expected[0].Username, actual[0].Username);
        }

        [Test]
        public void ApprovalCoachJobOK()
        {
            bool result = serviceUnderTest.ApprovalCoachJob(coach, true);
            Assert.AreEqual(result, true);
        }

        [Test]
        public void ApprovalCoachJobFailed()
        {
            bool result = serviceUnderTest.ApprovalCoachJob(new Coach("tr4", "tr5", "tr2", "tr2", "tr2", "tr2", time, "fc"), true);
            Assert.AreEqual(result, false);
        }


        [Test]
        public void GetCanceledTrainingsOK()
        {
            string expected = "fc=3;fc1=0";
            string result = serviceUnderTest.GetCanceledTrainings();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetCanceledTrainingsFailed()
        {

            string expected = "fc=4;fc1=1";
            string result = serviceUnderTest.GetCanceledTrainings();

            Assert.AreNotEqual(expected, result);
        }

        [Test]
        public void GetCoachesActivityOK()
        {
            string expected = "tr1=1";
            string result = serviceUnderTest.GetCoachesActivity();

            Assert.AreEqual(expected, result);

        }

        [Test]
        public void GetCoachesActivityFailed()
        {
            string expected = "tr1=4";
            string result = serviceUnderTest.GetCoachesActivity();

            Assert.AreNotEqual(expected, result);
        }

        [Test]
        public void GetFitnessCentersProfitsOK()
        {
            string expected = "fc=1000;fc1=2000";
            string result = serviceUnderTest.GetFitnessCentersProfits();

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetFitnessCentersProfitsFailed()
        {
            string expected = "fc=3000;fc1=4000";
            string result = serviceUnderTest.GetFitnessCentersProfits();

            Assert.AreNotEqual(expected, result);
        }

        [Test]
        public void GetTrainingPackagePopularityOK()
        {
            string expected = "paket1=1";
            string result = serviceUnderTest.GetTrainingPackagePopularity();

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetTrainingPackagePopularityFailed()
        {
            string expected = "paket2=1";
            string result = serviceUnderTest.GetTrainingPackagePopularity();

            Assert.AreNotEqual(expected, result);
        }

        [Test]
        public void CanDeleteHallTestOK()
        {
            bool result = serviceUnderTest.CanDeleteHall("sala2");

            Assert.AreEqual(true, result);
        }

        [Test]
        public void CanDeleteHallTestFailed()
        {
            bool result = serviceUnderTest.CanDeleteHall("sala1");

            Assert.AreEqual(false, result);
        }
    }
}
