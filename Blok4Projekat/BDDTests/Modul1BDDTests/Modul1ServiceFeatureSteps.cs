﻿using CommonLib;
using CommonModulLib;
using CommonModulLib.DataModel;
using Modul1Service;
using Modul1Service.Access;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace BDDTests
{
    [Binding]
    public class Modul1ServiceFeatureSteps
    {

        private Contract contract;
        private IModul2Contract proxy;

        [Given(@"I have contract and database")]
        public void GivenIHaveContractAndDatabase()
        {
            contract = new Contract();
            Modul1DB.Instance = Substitute.For<IModul1DB>();
            proxy = Substitute.For<IModul2Contract>();

            ConnectClass.Modul2ClientProxies = new Dictionary<string, IModul2Contract>();
            ConnectClass.Modul2ClientProxies.Add("test", proxy);

            Modul1DB.Instance.Login("admin", "admin").Returns(AuthenticationAndAuthorization.ADMIN);
            Modul1DB.Instance.Login("ef", "ef").Returns(AuthenticationAndAuthorization.EFF_MANAGER);
            Modul1DB.Instance.Login("wrong", "wrong").Returns(AuthenticationAndAuthorization.NONE);

            Modul1DB.Instance.AddNewEfficiencyManager("ef", "ef", "ef", "ef", "ef", "064-123456").Returns(true);
            Modul1DB.Instance.AddNewEfficiencyManager("ef1", "ef1", "ef1", "ef1", "ef1", "064-123456").Returns(false);

            Modul1DB.Instance.AddNewFitnessManager("m", "m", "m", "m", "m", "064-123123").Returns(true);
            Modul1DB.Instance.AddNewFitnessManager("m1", "m1", "m1", "m1", "m1", "064-123123").Returns(false);

            Modul1DB.Instance.AddNewFitnessCenter("fc", "fc", "064-123123", 3, "man", null).Returns(true);
            Modul1DB.Instance.AddNewFitnessCenter("fc1", "fc1", "064-123123 ", 3, "man", null).Returns(false);

            Modul1DB.Instance.GetFitnessCenters().Returns(new List<FitnessCenter>());

            Modul1DB.Instance.EditFitnessCenter("fc", "fc2", "064-123123", 3, "man").Returns(true);
            Modul1DB.Instance.EditFitnessCenter("fc2", "fc2", "064-123123", 3, "man").Returns(false);

            Modul1DB.Instance.AddNewHall("sala1", TypeOfHall.FITNESS, "fc").Returns(true);
            Modul1DB.Instance.AddNewHall("sala2", TypeOfHall.FITNESS, "fc").Returns(false);

            Modul1DB.Instance.EditHall("sala1", TypeOfHall.GYM, "fc").Returns(true);
            Modul1DB.Instance.EditHall("sala3", TypeOfHall.GYM, "fc").Returns(false);

            Modul1DB.Instance.DeleteHall("sala1").Returns(true);
            Modul1DB.Instance.DeleteHall("sala3").Returns(false);

            Modul1DB.Instance.EditEfficinencyManager("ef", "ef", "ef", "ef", "ef").Returns(true);
            Modul1DB.Instance.EditEfficinencyManager("ef1", "ef1", "ef1", "ef1", "ef1").Returns(false);

            proxy.GetTermsByCocachUsername("coach1").Returns(new List<Term>() { new Term("term1", "Tuesday", new Time(1, 1, 1, 1), "sala1", Term.TrainingLevel.BEGGINER, "coach1", 200), new Term("term3", "Monday", new Time(1, 1, 1, 1), "sala2", Term.TrainingLevel.BEGGINER, "coach1", 300) });
            proxy.GetTermsByCocachUsername("coach2").Returns(new List<Term>() { new Term("term4", "Monday", new Time(1, 1, 1, 1), "sala1", Term.TrainingLevel.ADVANCED, "coach2", 300) });
        }

        [When(@"Username is (.*) and password is (.*)")]
        public void WhenUsernameIsAndPasswordIs(string p0, string p1)
        {
            var result = contract.Login(p0, p1);
            ScenarioContext.Current.Add("result", result);
        }

        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(AuthenticationAndAuthorization p0)
        {
            var result = ScenarioContext.Current.Get<AuthenticationAndAuthorization>("result");
            Assert.AreEqual(result, p0);
        }

        [When(@"EfficiencyManager Username is (.*) and password is (.*) and  first name is (.*) and last name is (.*) and address is (.*) and telephone is (.*)")]
        public void WhenEfficiencyManagerUsernameIsAndPasswordIsAndFirstNameEfAndLastNameEfAndAddressIsAndTelephoneIs(string p0, string p1, string p2, string p3, string p4, string p5)
        {
            var result = contract.AddNewEfficiencyManager(p0, p1, p2, p3, p4, p5);
            ScenarioContext.Current.Add("newEffManager", result);
        }

        [Then(@"The result will be (.*)")]
        public void ThenTheResultWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("newEffManager");
            Assert.AreEqual(result, p0);
        }

        [When(@"FitnessCenterManager username is (.*) and password is (.*) and first name is (.*) and last name is (.*) and address is (.*) and telephone is (.*)")]
        public void WhenFitnessCenterManagerUsernameIsAndPasswordIsAndFirstNameIsAndLastNameIsAndAddressIsAndTelephoneIs(string p0, string p1, string p2, string p3, string p4, string p5)
        {
            var result = contract.AddNewFitnessManager(p0, p1, p2, p3, p4, p5);
            ScenarioContext.Current.Add("newFitnessCenterManager", result);
        }

        [Then(@"Created FitnessCenterManager will be (.*)")]
        public void ThenCreatedFitnessCenterManagerWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("newFitnessCenterManager");
            Assert.AreEqual(result, p0);
        }

        [When(@"FitnessCenter name is (.*) and address is (.*) and telephone is (.*) and number of halls is (.*) and fitness center manager username is (.*) and time is (.*)")]
        public void WhenFitnessCenterNameIsAndAddressIsAndTelephoneIsAndNumberOfHallsIsAndFitnessCenterManagerUsernameIsAndTimeIs(string p0, string p1, string p2, int p3, string p4, string p5)
        {
            var result = contract.AddNewFitnessCenter(p0, p1, p2, p3, p4, null);
            ScenarioContext.Current.Add("newFitnessCenter", result);
        }

        [Then(@"Created FittnessCenter will be (.*)")]
        public void ThenCreatedFittnessCenterWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("newFitnessCenter");
            Assert.AreEqual(result, p0);
        }

        [When(@"I ask for all fitness centers")]
        public void WhenIAskForAllFitnessCenters()
        {
            var result = contract.GetFitnessCenters();
            ScenarioContext.Current.Add("getFitnessCenters", result);
        }

        [Then(@"I will receive all fitness centers")]
        public void ThenIWillReceiveAllFitnessCenters()
        {
            var result = ScenarioContext.Current.Get<List<FitnessCenter>>("getFitnessCenters");
            Assert.IsInstanceOf(typeof(List<FitnessCenter>), result);
            Assert.IsEmpty(result);
        }

        [When(@"Editing FitnessCenter name is (.*) and address is (.*) and telephone is (.*) and number of halls is (.*) and fitness center manager username is (.*)")]
        public void WhenEditingFitnessCenterNameIsFcAndAddressIsFcAndTelephoneIsAndNumberOfHallsIsAndFitnessCenterManagerUsernameIsMan(string p0, string p1, string p2, int p3, string p4)
        {
            var result = contract.EditFitnessCenter(p0, p1, p2, p3, p4);
            ScenarioContext.Current.Add("editFitnessCenter", result);
        }

        [Then(@"Edited FitnessCenter will be (.*)")]
        public void ThenEditedFitnessCenterWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("editFitnessCenter");
            Assert.AreEqual(result, p0);
        }
        [When(@"I add hall with id (.*) and type (.*) and it belongs to fitness center with name (.*)")]
        public void WhenIAddHallWithIdSalaAndTypeAndItBelongsToFitnessCenterWithName(string p0, TypeOfHall p1, string p2)
        {
            var result = contract.AddNewHall(p0, p1, p2);
            ScenarioContext.Current.Add("addNewHall", result);
        }

        [Then(@"Created new hall will be (.*)")]
        public void ThenCreatedNewHallWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("addNewHall");
            Assert.AreEqual(result, p0);
        }

        [When(@"I edit hall with id (.*) and type (.*) and it belongs to fitness center with name (.*)")]
        public void WhenIEditHallWithIdAndTypeAndItBelongsToFitnessCenterWithName(string p0, TypeOfHall p1, string p2)
        {
            var result = contract.EditHall(p0, p1, p2);
            ScenarioContext.Current.Add("editHall", result);
        }

        [Then(@"Edited hall will be (.*)")]
        public void ThenEditedHallWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("editHall");
            Assert.AreEqual(result, p0);
        }

        [When(@"I delete hall with id (.*)")]
        public void WhenIDeleteHallWithIdSala(string p0)
        {
            var result = contract.DeleteHall(p0);
            ScenarioContext.Current.Add("deleteHall", result);
        }

        [Then(@"Deleted hall will be (.*)")]
        public void ThenDeletedHallWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("deleteHall");
            Assert.AreEqual(result, p0);
        }

        [When(@"I try to get terms created by coach with username (.*)")]
        public void WhenITryToGetTermsCreatedByCoachWithUsername(string p0)
        {
            var result = contract.GetTermsByCocachUsername(p0);
            ScenarioContext.Current.Add("getTermByCoach", result);
        }

        [Then(@"I will receive all Terms created by that coach (.*)")]
        public void ThenIWillReceiveAllTermsCreatedByThatCoach(int p0)
        {
            var result = ScenarioContext.Current.Get<List<Term>>("getTermByCoach");
            Assert.AreEqual(result.Count, p0);
        }

        [When(@"I change my personal data when username is (.*)  and  first name is (.*) and last name is (.*) and address is (.*) and telephone is (.*)")]
        public void WhenIChangeMyPersonalDataWhenUsernameIsAndFirstNameIsAndLastNameIsAndAddressIsAndTelephoneIs(string p0, string p1, string p2, string p3, string p4)
        {
            var result = contract.EditEfficinencyManager(p0, p1, p2, p3, p4);
            ScenarioContext.Current.Add("editEfManager", result);
        }

        [Then(@"My personal data will be updated (.*)")]
        public void ThenMyPersonalDataWillBeUpdated(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("editEfManager");
            Assert.AreEqual(result, p0);
        }
    }
}
