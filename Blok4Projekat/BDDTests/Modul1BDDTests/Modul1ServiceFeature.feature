﻿Feature: Modul1ServiceFeature
	I want to test behavior of Modul1Service


Scenario Outline: Login
	Given I have contract and database
	When Username is <username> and password is <password>
	Then the result should be <result>
	Examples: 
	| username | password | result |
	| admin    | admin    | 0      |
	| ef       | ef       | 1      |
	| wrong    | wrong    | 2      |


Scenario Outline: Add new Efficiency Manager
	Given I have contract and database
	When EfficiencyManager Username is <username> and password is <password> and  first name is <firstName> and last name is <lastName> and address is <address> and telephone is <telephone>
	Then The result will be <result>
	Examples: 
	| username | password | firstName | lastName | address | telephone  | result |
	| ef       | ef       | ef        | ef       | ef      | 064-123456 | true   |
	| ef1      | ef1      | ef1       | ef1      | ef1     | 064-123456 | false  |

Scenario Outline: Add new Fitness Center Manager
	Given I have contract and database
	When FitnessCenterManager username is <username> and password is <password> and first name is <firstName> and last name is <lastName> and address is <address> and telephone is <telephone>
	Then Created FitnessCenterManager will be <result>
	Examples: 
	| username | password | firstName | lastName | address | telephone  | result |
	| m        | m        | m         | m        | m       | 064-123123 | true   |
	| m1       | m1       | m1        | m1       | m1      | 064-123456 | false  |

Scenario Outline: Add new Fitness Center
	Given I have contract and database
	When FitnessCenter name is <name> and address is <address> and telephone is <telephone> and number of halls is <numOfHalls> and fitness center manager username is <fCMUsername> and time is <time>
	Then Created FittnessCenter will be <result>
	Examples: 
	| name | address | telephone  | numOfHalls | fCMUsername | time | result |
	| fc   | fc      | 064-123123 | 3          | man         | null | true   |
	| fc1  | fc1     | 064-123123 | 3          | man         | null | false  |

Scenario: Get Fitness Centers
	Given I have contract and database
	When I ask for all fitness centers
	Then I will receive all fitness centers

Scenario Outline: Edit Fitness Center
	Given I have contract and database
	When Editing FitnessCenter name is <name> and address is <address> and telephone is <telephone> and number of halls is <numOfHalls> and fitness center manager username is <fCMUsername>
	Then Edited FitnessCenter will be <result>
	Examples: 
		| name | address | telephone  | numOfHalls | fCMUsername | result |
		| fc   | fc2     | 064-123123 | 3          | man         | true   |
		| fc2  | fc2     | 064-123123 | 3          | man         | false  |

Scenario Outline: Add new hall
	Given I have contract and database
	When I add hall with id <id> and type <type> and it belongs to fitness center with name <fitnessCenterName>
	Then Created new hall will be <result>
	Examples: 
	| id    | type | fitnessCenterName | result |
	| sala1 | 0    | fc                | true   |
	| sala2 | 0    | fc                | false  |

Scenario Outline: Edit hall
	Given I have contract and database
	When I edit hall with id <id> and type <type> and it belongs to fitness center with name <fitnessCenterName>
	Then Edited hall will be <result>
	Examples: 
		| id    | type | fitnessCenterName | result |
		| sala1 | 1    | fc                | true   |
		| sala3 | 1    | fc                | false  |

Scenario Outline: Delete hall
	Given I have contract and database
	When I delete hall with id <id>
	Then Deleted hall will be <result>
	Examples: 
		| id    | result |
		| sala1 | true   |
		| sala3 | false  |

Scenario Outline: Get terms by coach username
	Given I have contract and database
	When I try to get terms created by coach with username <username>
	Then I will receive all Terms created by that coach <result>
	Examples:
	| username | result |
	| coach1   | 2      |
	| coach2   | 1      |

Scenario Outline: Edit efficiency manager personal data
	Given I have contract and database
	When I change my personal data when username is <username>  and  first name is <firstName> and last name is <lastName> and address is <address> and telephone is <telephone>
	Then My personal data will be updated <result>
	Examples: 
	| username | firstName | lastName | address | telephone | result |
	| ef       | ef        | ef       | ef      | ef        | true   |
	| ef1      | ef1       | ef1      | ef1     | ef1       | false  |
