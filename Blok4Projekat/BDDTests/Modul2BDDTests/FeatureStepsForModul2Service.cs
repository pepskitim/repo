﻿using CommonLibM2;
using Modul2Service;
using Modul2Service.Access;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using static CommonModulLib.DataModel.Term;

namespace BDDTests.Modul2BDDTests
{
    [Binding]
    public class FeatureStepsForModul2Service
    {
        private LoginManager loginManager;
        private static DateTime time = new DateTime();
        [Given(@"I have defined contract and my database")]
        public void GivenIHaveContractAndDatabase()
        {
            loginManager = new LoginManager();

            DBManager.Instance = Substitute.For<IDBManager>();
            DBManager.Instance.Login("fcm", "fcm").Returns(AuthenticationAndAuthorization.FITNESS_CENTER_MANAGER);
            DBManager.Instance.Login("coach", "coach").Returns(AuthenticationAndAuthorization.COACH);
            DBManager.Instance.Login("mem", "mem").Returns(AuthenticationAndAuthorization.MEMBER);
            DBManager.Instance.Login("wrong", "wrong").Returns(AuthenticationAndAuthorization.NONE);
            DBManager.Instance.SetCoach("coach", "coach", "ch1", "ch1", "ch 78", "dif", time, "fc").Returns(true);
            DBManager.Instance.SetMember("member", "member", "mem1", "mem1", "mem 78", 185, 85, time).Returns(true);
            DBManager.Instance.SetTerm("term1", "Monday", 7, 30, 10, 30, "hall1", TrainingLevel.ADVANCED, "coach", 1000).Returns(true);
            DBManager.Instance.DeleteCoach("coach").Returns(true);
            DBManager.Instance.DeleteMember("member").Returns(true);
            DBManager.Instance.DeleteTerm("term").Returns(true);
            DBManager.Instance.DeleteTrainingPackage(1).Returns(true);
            DBManager.Instance.NotifyMembersForTermChange("term", "Term has been updated to new values").Returns(true);
            DBManager.Instance.NotifyMembersForPackageChange("package", "Package has been updated to new values").Returns(true);
            DBManager.Instance.PayMPRel("member1", "package1").Returns(true);
            DBManager.Instance.PayMTRel("member1", "term1").Returns(true);
        }

        [When(@"Modul2Login username is (.*) and password is (.*)")]
        public void WhenUsernameIsAndPasswordIs(string p0, string p1)
        {
            var result = loginManager.Login(p0, p1);
            ScenarioContext.Current.Add("result", result);
        }

        [Then(@"login user will be (.*)")]
        public void ThenLoginUserWillBe(AuthenticationAndAuthorization p0)
        {
            var result = ScenarioContext.Current.Get<AuthenticationAndAuthorization>("result");
            Assert.AreEqual(result, p0);
        }

        //Add coach
        [When(@"Modul2AddNewCoach username is (.*) and password is (.*) and name is (.*) and lastName is (.*) and address is (.*) and degree is (.*) and dateTime is (.*) and fintessCenter is (.*)")]
        public void Modul2AddNewCoachUsernameIsAndPasswordIsAndNameIsAndLastNameIsAndAddressIsAndDegreeIsAndDateTimeIsAndFintessCenterIs(string p0, string p1, string p2, string p3, string p4, string p5, string p6, string p7)
        {
            var result = loginManager.AddNewCoach(p0, p1, p2, p3, p4, p5, time, p7);
            ScenarioContext.Current.Add("newCoach", result);
        }

        [Then(@"Result of add new coach will be (.*)")]
        public void ThenResultOfAddNewCoachWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("newCoach");
            Assert.AreEqual(result, p0);
        }



        //Add member

        [When(@"Modul2CreateMember username is (.*) and password is (.*) and name is (.*) and lastName is (.*) and address is (.*) and height is (.*) and weight is (.*) and dateTime is (.*)")]
        public void Modul2CreateMemberUsernameIsAndPasswordIsAndNameIsAndLastNameIsAndAddressIsAndHeightIsAndWeightIsAndDateTimeIs(string p0, string p1, string p2, string p3, string p4, string p5, string p6, string p7)
        {
            var result = loginManager.AddNewMember(p0, p1, p2, p3, p4, Double.Parse(p5), Double.Parse(p6), time);
            ScenarioContext.Current.Add("newMember", result);
        }

        [Then(@"Result of create member will be (.*)")]
        public void ThenResultOfCreateMemberWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("newMember");
            Assert.AreEqual(result, p0);
        }

        //Add term
        [When(@"Modul2CreateTerm name is (.*) and day is (.*) and startedHour is (.*) and startedMinute is (.*) and endingHour is (.*) and endingMinute is (.*) and hallName is (.*) and trainingLevel is (.*) and coach is (.*) and price (.*)")]
        public void Modul2CreateTermNameIsAndDayIsAndStartedHourIsAndStartedMinuteIsAndEndingHourIsAndEndingMinuteIsAndHallNameIsAndTrainingLeveIsAndCoachIsAndPrice(string p0, string p1, string p2, string p3, string p4, string p5, string p6, string p7, string p8, string p9)
        {
            var result = loginManager.AddNewTerm(p0, p1, Int32.Parse(p2), Int32.Parse(p3), Int32.Parse(p4), Int32.Parse(p5), p6, TrainingLevel.ADVANCED, p8, Double.Parse(p9));
            ScenarioContext.Current.Add("newTerm", result);
        }

        [Then(@"Result of create term will be (.*)")]
        public void ThenResultOfCreateTermWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("newTerm");
            Assert.AreEqual(result, p0);
        }

        //Delete coach
        [When(@"Modul2DeleteCoach username is (.*)")]
        public void Modul2DeleteCoachUsernameIs(string p0)
        {
            var result = loginManager.DeleteCoach(p0);
            ScenarioContext.Current.Add("deletedCoach", result);
        }

        [Then(@"Result of delete coach will be (.*)")]
        public void ThenResultOfDeleteCoachWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("deletedCoach");
            Assert.AreEqual(result, p0);
        }

        //Delete member
        [When(@"Modul2DeleteMember username is (.*)")]
        public void Modul2DeleteMemberUsernameIs(string p0)
        {
            var result = loginManager.DeleteMember(p0);
            ScenarioContext.Current.Add("deletedMember", result);
        }

        [Then(@"Result of delete member will be (.*)")]
        public void ThenResultOfDeleteMemberWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("deletedMember");
            Assert.AreEqual(result, p0);
        }

        //Delete term
        [When(@"Modul2DeleteTerm username is (.*)")]
        public void Modul2DeleteTermUsernameIs(string p0)
        {
            var result = loginManager.DeleteTerm(p0);
            ScenarioContext.Current.Add("deletedTerm", result);
        }

        [Then(@"Result of delete term will be (.*)")]
        public void ThenResultOfDeleteTermWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("deletedTerm");
            Assert.AreEqual(result, p0);
        }

        //Delete package
        [When(@"Modul2DeletePackage id is (.*)")]
        public void Modul2DeletePackageIdIs(string p0)
        {
            var result = loginManager.DeletePackage(Int32.Parse(p0));
            ScenarioContext.Current.Add("deletedPackage", result);
        }

        [Then(@"Result of delete package will be (.*)")]
        public void ThenResultOfDeletePackageWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("deletedPackage");
            Assert.AreEqual(result, p0);
        }

        //notify members for term change
        [When(@"Modul2NotifyMembersForTermChange term name is (.*) and notification is (.*)")]
        public void WhenModulNotifyMembersForTermChangeTermNameIsTermAndNotificationIsTermHasBeenUpdatedToNewValues(string p0, string p1)
        {
            var result = loginManager.NotifyMembersForTermChange(p0, p1);
            ScenarioContext.Current.Add("newTermNotification", result);
        }

        [Then(@"Result of term notify will be (.*)")]
        public void ThenResultOfTermNotifyWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("newTermNotification");
            Assert.AreEqual(result, p0);
        }

        //notify members for package change
        [When(@"Modul2NotifyMembersForPackageChange package name is (.*) and notification is (.*)")]
        public void WhenModulNotifyMembersForPackageChangePackageNameIsPackageAndNotificationIsPackageHasBeenUpdatedToNewValues(string p0, string p1)
        {
            var result = loginManager.NotifyMembersForPackageChange(p0, p1);
            ScenarioContext.Current.Add("newPackageNotification", result);
        }

        [Then(@"Result of package notify will be (.*)")]
        public void ThenResultOfPackageNotifyWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("newPackageNotification");
            Assert.AreEqual(result, p0);
        }

        //pay mp 
        [When(@"Modul2PayMPRel member name is (.*) and package is (.*)")]
        public void WhenModul2PayMPRelMemberNameIsMemberAndPackageIsPackage(string p0, string p1)
        {
            var result = loginManager.PayMPRel(p0, p1);
            ScenarioContext.Current.Add("payMP", result);
        }

        [Then(@"Result of package payment will be (.*)")]
        public void ThenResultOfPackagePaymentWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("payMP");
            Assert.AreEqual(result, p0);
        }

        //pay mt releation
        [When(@"Modul2PayMTRel member name is (.*) and term is (.*)")]
        public void WhenModul2PayMPRelMemberNameIsMemberAndTermIsTerm(string p0, string p1)
        {
            var result = loginManager.PayMTRel(p0, p1);
            ScenarioContext.Current.Add("payMT", result);
        }

        [Then(@"Result of term payment will be (.*)")]
        public void ThenResultOfTermPaymentWillBe(bool p0)
        {
            var result = ScenarioContext.Current.Get<bool>("payMT");
            Assert.AreEqual(result, p0);
        }

    }
}
