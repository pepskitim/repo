﻿Feature: FeatureForModul2Service
	I want to test behavior of Modul1Service

Scenario Outline: Modul2ServiceLogin
	Given I have defined contract and my database
	When Modul2Login username is <username> and password is <password>
	Then login user will be <result>
	Examples: 
	| username | password | result |
	| fcm      | fcm      | 0      |
	| coach    | coach    | 1      |
	| mem      | mem      | 2      |
	| wrong    | wrong    | 3      |

Scenario Outline: Add new coach
	Given I have defined contract and my database
	When Modul2AddNewCoach username is <username> and password is <password> and name is <name> and lastName is <lastName> and address is <address> and degree is <degree> and dateTime is <dateTime> and fintessCenter is <fitnessCenter>
	Then Result of add new coach will be <result>
	Examples: 
	| username | password | name | lastName | address | degree | dateTime | fitnessCenter | result |
	| coach    | coach    | ch1  | ch1      | ch 78   | dif    | null     | fc            | true   |
	| ch2      | ch2      | ch2  | ch2      | ch2 78  | dif    | null     | fc            | false  |

Scenario Outline: Add new member
	Given I have defined contract and my database
	When Modul2CreateMember username is <username> and password is <password> and name is <name> and lastName is <lastName> and address is <address> and height is <height> and weight is <weight> and dateTime is <dateTime>
	Then Result of create member will be <result>
	Examples: 
	| username | password | name | lastName | address | height | weight | dateTime | result |
	| member   | member   | mem1 | mem1     | mem 78  | 185    | 85     | null     | true   |
	| member2  | member2  | mem2 | mem2     | mem 278 | 180    | 75     | null     | false  |

Scenario Outline: : Add new term
	Given I have defined contract and my database
	When  Modul2CreateTerm name is <name> and day is <day> and startedHour is <startedHour> and startedMinute is <startedMinute> and endingHour is <endingHour> and endingMinute is <endingMinute> and hallName is <hallName> and trainingLevel is <trainingLevel> and coach is <coach> and price <price>
	Then Result of create term will be <result>
	Examples:
	| name  | day    | startedHour | startedMinute | endingHour | endingMinute | hallName | trainingLevel | coach | price | result |
	| term1 | Monday | 7           | 30            | 10         | 30           | hall1    | 0             | coach | 1000  | true   |
	| term2 | Sunday | 7           | 30            | 10         | 30           | hall1    | 0             | coach | 1000  | false  |

Scenario Outline: Delete coach
	Given I have defined contract and my database
	When Modul2DeleteCoach username is <username>
	Then Result of delete coach will be <result>
	Examples: 
	| username | result |
	| coach    | true   |
	| coach1   | false  |

Scenario Outline: Delete member
	Given I have defined contract and my database
	When Modul2DeleteMember username is <username>
	Then Result of delete member will be <result>
	Examples: 
	| username | result |
	| member   | true   |
	| member1  | false  |

Scenario Outline: Delete term
	Given I have defined contract and my database
	When Modul2DeleteTerm username is <username>
	Then Result of delete term will be <result>
	Examples: 
	| username | result |
	| term     | true   |
	| term1    | false  |

Scenario Outline: Delete package
	Given I have defined contract and my database
	When Modul2DeletePackage id is <id>
	Then Result of delete package will be <result>
	Examples: 
	| id | result |
	| 1  | true   |
	| 0  | false  |

Scenario Outline: Add new notification for term change
	Given I have defined contract and my database
	When Modul2NotifyMembersForTermChange term name is <termName> and notification is <notification>
	Then Result of term notify will be <result>
	Examples:
	| termName | notification                        | result |
	| term     | Term has been updated to new values | true   |
	| term2    | Term has been updated               | false  |

Scenario Outline: Add new notification for package change
	Given I have defined contract and my database
	When Modul2NotifyMembersForPackageChange package name is <packageName> and notification is <notification>
	Then Result of package notify will be <result>
	Examples:
	| packageName | notification                           | result |
	| package     | Package has been updated to new values | true   |
	| package2    | Package has been updated               | false  |

Scenario Outline: Pay for package 
	Given I have defined contract and my database
	When Modul2PayMPRel member name is <memberName> and package is <packageName>
	Then Result of package payment will be <result>
	Examples:
	| memberName | packageName | result |
	| member1    | package1    | true   |
	| member2    | package2    | false  |

Scenario Outline: Pay for term 
	Given I have defined contract and my database
	When Modul2PayMTRel member name is <memberName> and term is <termName>
	Then Result of term payment will be <result>
	Examples:
	| memberName | termName | result |
	| member1    | term1    | true   |
	| member2    | term2    | false  |