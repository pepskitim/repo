﻿using CommonModulLib;
using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Service
{
    public class WCFModul2Client : ChannelFactory<IModul2Contract>, IModul2Contract, IDisposable
    {
       private IModul2Contract factory;

        public WCFModul2Client(NetTcpBinding binding, EndpointAddress address)
            : base(binding, address)
        {
            factory = this.CreateChannel();
        }

        public List<Coach> GetCoachesByFitnessCenterName(string fitnessCenterName)
        {
            return factory.GetCoachesByFitnessCenterName(fitnessCenterName);
        }

        public List<Term> GetTermsByFitnessCenterName(string fitnessCenterName)
        {
            return factory.GetTermsByFitnessCenterName(fitnessCenterName);
        }

        public List<string> GetAllCoachUsernames()
        {
            return factory.GetAllCoachUsernames();
        }

        public List<Term> GetTermsByCocachUsername(string coachUsername)
        {
            return factory.GetTermsByCocachUsername(coachUsername);
        }

        public List<Coach> GetCoachesWaitForJob()
        {
            return factory.GetCoachesWaitForJob();
        }

        public bool ApprovalCoachJob(Coach coach, bool approve)
        {
            return factory.ApprovalCoachJob(coach, approve);
        }

        public string GetCanceledTrainings()
        {
            return factory.GetCanceledTrainings();
        }

        public string GetCoachesActivity()
        {
            return factory.GetCoachesActivity();
        }

        public string GetFitnessCentersProfits()
        {
            return factory.GetFitnessCentersProfits();
        }

        public string GetTrainingPackagePopularity()
        {
            return factory.GetTrainingPackagePopularity();
        }

        public void Dispose()
        {
            if (factory != null)
            {
                factory = null;
            }

            this.Close();
        }

        public bool CanDeleteHall(string hallId)
        {
            return factory.CanDeleteHall(hallId);
        }
    }
}
