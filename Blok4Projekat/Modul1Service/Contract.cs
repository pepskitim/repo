﻿using System;
using System.Collections.Generic;
using CommonLib;
using CommonModulLib.DataModel;
using Modul1Service.Access;
using CommonModulLib;

namespace Modul1Service
{
    public class Contract : IContract
    {
        public AuthenticationAndAuthorization Login(string username, string password)
        {
            return Modul1DB.Instance.Login(username, password);
        }

        public bool AddNewEfficiencyManager(string username, string password, string firstName, string lastName, string address, string telephone)
        {
            return Modul1DB.Instance.AddNewEfficiencyManager(username, password, firstName, lastName, address, telephone);
        }

        public bool AddNewFitnessManager(string username, string password, string firstName, string lastName, string address, string telephone)
        {
            return Modul1DB.Instance.AddNewFitnessManager(username, password, firstName, lastName, address, telephone);
        }

        public bool AddNewFitnessCenter(string name, string address, string telephone, int numOfHalls, string fcManagerUsername, Time workingTime)
        {
            return Modul1DB.Instance.AddNewFitnessCenter(name, address, telephone, numOfHalls, fcManagerUsername, workingTime);
        }

        public List<string> GetFitnessCenterManagerUsernames()
        {
            return Modul1DB.Instance.GetFitnessCenterManagerUsernames();
        }

        public List<FitnessCenter> GetFitnessCenters()
        {
            return Modul1DB.Instance.GetFitnessCenters();
        }

        public bool EditFitnessCenter(string name, string address, string telephone, int numOfHalls, string fcManagerUsername)
        {
            return Modul1DB.Instance.EditFitnessCenter(name, address, telephone, numOfHalls, fcManagerUsername);
        }

        public List<string> GetFitnessCenterNames()
        {
            return Modul1DB.Instance.GetFitnessCenterNames();
        }

        public bool AddNewHall(string id, TypeOfHall type, string fitnessCenterName)
        {
            return Modul1DB.Instance.AddNewHall(id, type, fitnessCenterName);
        }

        public List<Hall> GetHallsBySelectedFitnessCenter(string fitnessCenterName)
        {
            return Modul1DB.Instance.GetHallsBySelectedFitnessCenter(fitnessCenterName);
        }

        public bool DeleteHall(string hallID)
        {
            return Modul1DB.Instance.DeleteHall(hallID);
        }

        public bool EditHall(string id, TypeOfHall hallType, string fitnessCenter)
        {
            return Modul1DB.Instance.EditHall(id, hallType, fitnessCenter);
        }

        public List<FitnessCenterManager> GetFitnessCenterManagers()
        {
            return Modul1DB.Instance.GetFitnessCenterManagers();
        }

        public bool EditEfficinencyManager(string username, string name, string lastName, string address, string telephone)
        {
            return Modul1DB.Instance.EditEfficinencyManager(username, name, lastName, address, telephone);
        }

        public EfficiencyManager GetEfficinencyManagerByUsername(string username)
        {
            return Modul1DB.Instance.GetEfficinencyManagerByUsername(username);
        }

        public List<EfficiencyManager> GetEfficiencyManagers()
        {
            return Modul1DB.Instance.GetEfficiencyManagers();
        }

        public FitnessCenter GetFitnessCenterByName(string name)
        {
            return Modul1DB.Instance.GetFitnessCenterByName(name);
        }

        public List<Coach> GetCoachesByFitnessCenterName(string fitnessCenterName)
        {
            List<Coach> result = new List<Coach>();
            foreach (IModul2Contract client in ConnectClass.Modul2ClientProxies.Values)
            {
                result.AddRange(client.GetCoachesByFitnessCenterName(fitnessCenterName));
            }

            return result;
        }

        public List<Term> GetTermsByFitnessCenterName(string fitnessCenterName)
        {
            List<Term> result = new List<Term>();
            foreach (IModul2Contract client in ConnectClass.Modul2ClientProxies.Values)
            {
                result.AddRange(client.GetTermsByFitnessCenterName(fitnessCenterName));
            }

            return result;
        }

        public List<string> GetAllCoachUsernames()
        {
            List<string> result = new List<string>();
            foreach (IModul2Contract client in ConnectClass.Modul2ClientProxies.Values)
            {
                result.AddRange(client.GetAllCoachUsernames());
            }

            return result;
        }

        public List<Term> GetTermsByCocachUsername(string coachUsername)
        {
            List<Term> result = new List<Term>();
            foreach (IModul2Contract client in ConnectClass.Modul2ClientProxies.Values)
            {
                result.AddRange(client.GetTermsByCocachUsername(coachUsername));
            }

            return result;
        }

        public List<Coach> GetCoachesWaitForJob()
        {
            List<Coach> result = new List<Coach>();
            foreach (IModul2Contract client in ConnectClass.Modul2ClientProxies.Values)
            {
                result.AddRange(client.GetCoachesWaitForJob());
            }

            return result;
        }

        public bool ApprovalCoachJob(Coach coach, bool approve)
        {
            string ipAddress = Modul1DB.Instance.GetIpAddressByFitnessCenterName(coach.FitnessCenter);

            return ConnectClass.Modul2ClientProxies[ipAddress].ApprovalCoachJob(coach, approve);
        }

        public string GetCanceledTrainings()
        {
            string result = string.Empty;
            foreach (IModul2Contract client in ConnectClass.Modul2ClientProxies.Values)
            {
                result += client.GetCanceledTrainings();
            }

            return result;
        }

        public string GetCoachesActivity()
        {
            string result = string.Empty;
            foreach (IModul2Contract client in ConnectClass.Modul2ClientProxies.Values)
            {
                result += client.GetCoachesActivity();
            }

            return result;
        }

        public string GetFitnessCentersProfits()
        {
            string result = string.Empty;
            foreach (IModul2Contract client in ConnectClass.Modul2ClientProxies.Values)
            {
                result += client.GetFitnessCentersProfits();
            }

            return result;
        }

        public string GetTrainingPackagePopularity()
        {
            string result = string.Empty;
            foreach (IModul2Contract client in ConnectClass.Modul2ClientProxies.Values)
            {
                result += client.GetTrainingPackagePopularity();
            }

            return result;
        }

        public bool CanDeleteHall(string hallId)
        {
            foreach (IModul2Contract client in ConnectClass.Modul2ClientProxies.Values)
            {
                if (!client.CanDeleteHall(hallId))
                {
                    return false;
                } 
            }

            return true;
        }
    }
}
