﻿using CommonLib;
using CommonModulLib;
using Modul1Service.Access;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Service
{
    public class Program
    {
       public static void Main(string[] args)
        {
            StartUpBase();

            NetTcpBinding bindingServiceCommunication = new NetTcpBinding();
            string addressServiceCommunication = "net.tcp://localhost:9998/IModul1Contract";
            ServiceHost hostServiceCommunication = new ServiceHost(typeof(Modul1Contract));

            hostServiceCommunication.AddServiceEndpoint(typeof(IModul1Contract), bindingServiceCommunication,
                addressServiceCommunication);

            hostServiceCommunication.Open();
            Console.WriteLine("Modul1 interservice communication service is opened.");

            NetTcpBinding bindingClient = new NetTcpBinding();
            string addressClient = "net.tcp://localhost:9999/Contract";

            ServiceHost hostClient = new ServiceHost(typeof(Contract));
            hostClient.AddServiceEndpoint(typeof(IContract), bindingClient, addressClient);

            hostClient.Open();
            Console.WriteLine("Modul1 Service is opened. Press <enter> to finish...");

            Console.ReadLine();

            hostClient.Close();
            hostServiceCommunication.Close();
        }

        private static void StartUpBase()
        {
            string executable = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string path = System.IO.Path.GetDirectoryName(executable);
            path = path.Substring(0, path.LastIndexOf("bin")) + "DB";
            AppDomain.CurrentDomain.SetData("DataDirectory", path);

            // update database
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AccessDB, Configuration>());
        }
    }
}
