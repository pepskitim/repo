﻿using CommonLib;
using CommonModulLib;
using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Service.Access
{
    public interface IModul1DB
    {
        string GetIpAddressByFitnessCenterName(string fitnessCenterName);

        List<Hall> GetAllHalls(string fitnessCenterName); // pokupi sve hale jer mi trebaju za prikaz u menadzeru fc

        List<FitnessCenter> GetAllFitnessCenters();

        bool EditPersonalFCMdata(FitnessCenterManager fcMenager); // za slanje editovanog objekta jer je snimljen u vasoj bazi

        List<string> GetAllFitnessCenterByManagerName(string coachUsername);

        List<string> GetAllHallIds(string fitnessCenterName);

        bool LoginFitnessCenterManager(string username, string password);

        FitnessCenterManager GetFitnessCenterManagerByUserName(string username);

        bool ConnectToModul1(string fitnessCenterName, string modul2IpAddress);

        List<Coach> GetCoachesByFitnessCenterName(string fitnessCenterName);

        List<Term> GetTermsByFitnessCenterName(string fitnessCenterName);

        List<string> GetAllCoachUsernames();

        List<Term> GetTermsByCocachUsername(string coachUsername);

        List<Coach> GetCoachesWaitForJob();

        bool ApprovalCoachJob(Coach coach, bool approve);

        string GetCanceledTrainings();

        string GetCoachesActivity();

        string GetFitnessCentersProfits();

        string GetTrainingPackagePopularity();

        bool CanDeleteHall(string hallId);

        AuthenticationAndAuthorization Login(string username, string password);

        bool AddNewEfficiencyManager(string username, string password, string firstName, string lastName, string address, string telephone);

        bool AddNewFitnessManager(string username, string password, string firstName, string lastName, string address, string telephone);

        bool AddNewFitnessCenter(string name, string address, string telephone, int numOfHalls, string fcManagerUsername, Time workingTime);

        List<string> GetFitnessCenterManagerUsernames();

        List<FitnessCenter> GetFitnessCenters();

        bool EditFitnessCenter(string name, string address, string telephone, int numOfHalls, string fcManagerUsername);

        List<string> GetFitnessCenterNames();

        bool AddNewHall(string id, TypeOfHall type, string fitnessCenterName);

        List<Hall> GetHallsBySelectedFitnessCenter(string fitnessCenterName);

        bool DeleteHall(string hallID);

        bool EditHall(string id, TypeOfHall hallType, string fitnessCenter);

        List<FitnessCenterManager> GetFitnessCenterManagers();

        bool EditEfficinencyManager(string username, string name, string lastName, string address, string telephone);

        EfficiencyManager GetEfficinencyManagerByUsername(string username);

        List<EfficiencyManager> GetEfficiencyManagers();

        FitnessCenter GetFitnessCenterByName(string name);
    }
}
