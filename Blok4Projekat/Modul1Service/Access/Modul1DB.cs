﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonModulLib.DataModel;
using System.Data.Entity.Infrastructure;
using CommonLib;
using CommonModulLib;
using System.Diagnostics;

namespace Modul1Service.Access
{
    public class Modul1DB : IModul1DB
    {
        private static IModul1DB myDB;

        public static IModul1DB Instance
        {
            get
            {
                if (myDB == null)
                {
                    myDB = new Modul1DB();
                }

                return myDB;
            }

            set
            {
                if (myDB == null)
                {
                    myDB = value;
                }           
            }
        }

        public AuthenticationAndAuthorization Login(string username, string password)
        {
            using (var access = new AccessDB())
            {
                if (username.Equals("admin") && password.Equals("admin"))
                {
                    return AuthenticationAndAuthorization.ADMIN;
                }
                else
                {
                    var user = access.EffManagers.Find(username);
                    if (user != null)
                    {
                        if (user.Password.Equals(password))
                        {
                            return AuthenticationAndAuthorization.EFF_MANAGER;
                        }
                    }

                    return AuthenticationAndAuthorization.NONE;
                }
            }
        }

        public bool AddNewEfficiencyManager(string username, string password, string firstName, string lastName, string address, string telephone)
        {
            using (var access = new AccessDB())
            {
                EfficiencyManager manager = new EfficiencyManager(username, password, firstName, lastName, address, telephone);
                try
                {
                    access.EffManagers.Add(manager);
                    access.SaveChanges();
                    Console.WriteLine("Added new EfficiencyManager with username: " + manager.Username);
                    return true;
                }
                catch (DbUpdateException ex)
                {
                    Trace.TraceInformation(ex.StackTrace);
                    Console.WriteLine("User with username: {0} already exists", manager.Username);
                    return false;
                }
            }
        }

        public bool AddNewFitnessManager(string username, string password, string firstName, string lastName, string address, string telephone)
        {
            using (var access = new AccessDB())
            {
                FitnessCenterManager manager = new FitnessCenterManager(username, password, firstName, lastName, address, telephone);
                try
                {
                    access.FitnessManagers.Add(manager);
                    access.SaveChanges();
                    Console.WriteLine("Added new FitnessManager with username: " + manager.Username);
                    return true;
                }
                catch (DbUpdateException ex)
                {
                    Trace.TraceInformation(ex.StackTrace);
                    Console.WriteLine("User with username: {0} already exists", manager.Username);
                    return false;
                }
            }
        }

        public bool AddNewFitnessCenter(string name, string address, string telephone, int numOfHalls, string fcManagerUsername, Time workingTime)
        {
            using (var access = new AccessDB())
            {
                FitnessCenter center = new FitnessCenter(name, address, telephone, numOfHalls, fcManagerUsername, workingTime);
                try
                {
                    access.FitnessCenters.Add(center);
                    access.SaveChanges();
                    Console.WriteLine("Added new FitnessCenter with name: " + center.Name);
                    return true;
                }
                catch (DbUpdateException ex)
                {
                    Trace.TraceInformation(ex.StackTrace);
                    Console.WriteLine("FitnessCenter with name: {0} already exists", center.Name);
                    return false;
                }
            }
        }

        public List<string> GetFitnessCenterManagerUsernames()
        {
            using (var access = new AccessDB())
            {
                return access.FitnessManagers.Select(u => u.Username).ToList();
            }
        }

        public List<FitnessCenter> GetFitnessCenters()
        {
            using (var access = new AccessDB())
            {
                return access.FitnessCenters.Where(u => u != null).ToList();
            }
        }

        public bool EditFitnessCenter(string name, string address, string telephone, int numOfHalls, string fcManagerUsername)
        {
            using (var access = new AccessDB())
            {
                var original = access.FitnessCenters.Find(name);
                if (original != null)
                {
                    original.Address = address;
                    original.Telephone = telephone;
                    original.NumOfHalls = numOfHalls;
                    original.FcManagerUsername = fcManagerUsername;
                    if (access.SaveChanges() > 0)
                    {
                        Console.WriteLine("Updated FitnessCenter with name: " + name);
                        return true;
                    }
                }

                return false;
            }
        }

        public List<string> GetFitnessCenterNames()
        {
            using (var access = new AccessDB())
            {
                return access.FitnessCenters.Select(u => u.Name).ToList();
            }
        }

        public bool AddNewHall(string id, TypeOfHall type, string fitnessCenterName)
        {
            using (var access = new AccessDB())
            {
                Hall hall = new Hall(id, type, fitnessCenterName);
                FitnessCenter fc = access.FitnessCenters.Find(fitnessCenterName);
                if (fc != null)
                {
                    if (access.Halls.Where(u => u.FitnessCenterName.Equals(fitnessCenterName)).ToList().Count != fc.NumOfHalls)
                    {
                        try
                        {
                            access.Halls.Add(hall);
                            access.SaveChanges();
                            Console.WriteLine("Added new Hall with id: " + hall.Id);
                            return true;
                        }
                        catch (DbUpdateException ex)
                        {
                            Console.WriteLine("FitnessCenter with name: {0} already exists", hall.Id);
                            return false;
                        }
                    }
                }

                return false;
            }
        }

        public List<Hall> GetHallsBySelectedFitnessCenter(string fitnessCenterName)
        {
            using (var access = new AccessDB())
            {
                return access.Halls.Where(u => u.FitnessCenterName.Equals(fitnessCenterName)).ToList();
            }
        }

        public bool DeleteHall(string hallID)
        {
            using (var access = new AccessDB())
            {
                try
                {
                    access.Halls.Remove(access.Halls.Find(hallID));

                    if (access.SaveChanges() > 0)
                    {
                        Console.WriteLine("Deleted Hall with id: " + hallID);
                        return true;
                    }

                    return false;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(ex.StackTrace);
                    Console.WriteLine("Can not delete Hall with ID: {0} ", hallID);
                    return false;
                }
            }
        }

        public bool EditHall(string id, TypeOfHall hallType, string fitnessCenter)
        {
            using (var access = new AccessDB())
            {
                var original = access.Halls.Find(id);
                if (original != null)
                {
                    original.FitnessCenterName = fitnessCenter;
                    original.Type = hallType;
                    if (access.SaveChanges() > 0)
                    {
                        Console.WriteLine("Updated Hall with id: " + id);
                        return true;
                    }

                    return false;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<FitnessCenterManager> GetFitnessCenterManagers()
        {
            using (var access = new AccessDB())
            {
                return access.FitnessManagers.Where(u => u != null).ToList();
            }
        }

        public bool EditEfficinencyManager(string username, string name, string lastName, string address, string telephone)
        {
            using (var access = new AccessDB())
            {
                var original = access.EffManagers.Find(username);
                if (original != null)
                {
                    original.Address = address;
                    original.Telephone = telephone;
                    original.FirstName = name;
                    original.LastName = lastName;
                    if (access.SaveChanges() > 0)
                    {
                        Console.WriteLine("Updated Efficiency Manager with name: " + name);
                        return true;
                    }
                }

                return false;
            }
        }

        public EfficiencyManager GetEfficinencyManagerByUsername(string username)
        {
            using (var access = new AccessDB())
            {
                return access.EffManagers.Find(username);
            }
        }

        public List<EfficiencyManager> GetEfficiencyManagers()
        {
            using (var access = new AccessDB())
            {
                return access.EffManagers.Where(u => u != null).ToList();
            }
        }

        public FitnessCenter GetFitnessCenterByName(string name)
        {
            using (var access = new AccessDB())
            {
                return access.FitnessCenters.Find(name);
            }
        }

        public List<Hall> GetAllHalls(string fitnessCenterName)
        {
            using (var access = new AccessDB())
            {
                return access.Halls.Where(u => u.FitnessCenterName.Equals(fitnessCenterName)).ToList();
            }
        }

        public List<FitnessCenter> GetAllFitnessCenters()
        {
            using (var access = new AccessDB())
            {
                return access.FitnessCenters.Where(u => u != null).ToList();
            }
        }

        public bool EditPersonalFCMdata(FitnessCenterManager fcMenager)
        {
            using (var access = new AccessDB())
            {
                var manager = access.FitnessManagers.Find(fcMenager.Username);
                if (manager != null)
                {
                    manager.FirstName = fcMenager.FirstName;
                    manager.LastName = fcMenager.LastName;
                    manager.Telephone = fcMenager.Telephone;
                    manager.Address = fcMenager.Address;
                    manager.Password = fcMenager.Password;
                    if (access.SaveChanges() > 0)
                    {
                        Console.WriteLine("Updated Fitness Center Manager with username: " + fcMenager.Username);

                        return true;
                    }
                }

                return false;
            }
        }

        public List<string> GetAllFitnessCenterByManagerName(string fcUsername)
        {
            using (var access = new AccessDB())
            {
                return access.FitnessCenters.Where(u => u.FcManagerUsername.Equals(fcUsername)).Select(u => u.Name).ToList();
            }
        }

        public List<string> GetAllHallIds(string fitnessCenterName)
        {
            using (var access = new AccessDB())
            {
                return access.Halls.Where(u => u.FitnessCenterName.Equals(fitnessCenterName)).Select(u => u.Id).ToList();
            }
        }

        public bool LoginFitnessCenterManager(string username, string password)
        {
            using (var access = new AccessDB())
            {
                var user = access.FitnessManagers.Find(username);
                if (user != null)
                {
                    if (user.Password.Equals(password))
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public FitnessCenterManager GetFitnessCenterManagerByUserName(string username)
        {
            using (var access = new AccessDB())
            {
                return access.FitnessManagers.Find(username);
            }
        }

        public bool ConnectToModul1(string fitnessCenterName, string modul2IpAddress)
        {
            using (var access = new AccessDB())
            {
                FitnessCenterNameAndServiceIp fcNameAndIp = access.FCNamesAndIps.Find(fitnessCenterName);
                if (fcNameAndIp != null)
                {
                    if (fcNameAndIp.ServiceIp.Equals(modul2IpAddress))
                    {
                        ConnectClass.AddNewModul2IpAddress(modul2IpAddress);

                        return true;
                    }

                    return false;
                }

                FitnessCenter fc = access.FitnessCenters.Find(fitnessCenterName);

                if (fc != null)
                {
                    access.FCNamesAndIps.Add(new FitnessCenterNameAndServiceIp(fitnessCenterName, modul2IpAddress));
                    access.SaveChanges();
                    ConnectClass.AddNewModul2IpAddress(modul2IpAddress);

                    return true;
                }

                return false;
            }
        }

        public string GetIpAddressByFitnessCenterName(string fitnessCenterName)
        {
            using (var access = new AccessDB())
            {
                return access.FCNamesAndIps.Find(fitnessCenterName).ServiceIp;
            }
        }

        //Nije potrebna implementacija nad bazom, potrebno izbrisati iz interfejsa, nedovoljno vremena
        public List<Coach> GetCoachesByFitnessCenterName(string fitnessCenterName)
        {
            throw new NotImplementedException();
        }

        public List<Term> GetTermsByFitnessCenterName(string fitnessCenterName)
        {
            throw new NotImplementedException();
        }

        public List<string> GetAllCoachUsernames()
        {
            throw new NotImplementedException();
        }

        public List<Term> GetTermsByCocachUsername(string coachUsername)
        {
            throw new NotImplementedException();
        }

        public List<Coach> GetCoachesWaitForJob()
        {
            throw new NotImplementedException();
        }

        public bool ApprovalCoachJob(Coach coach, bool approve)
        {
            throw new NotImplementedException();
        }

        public string GetCanceledTrainings()
        {
            throw new NotImplementedException();
        }

        public string GetCoachesActivity()
        {
            throw new NotImplementedException();
        }

        public string GetFitnessCentersProfits()
        {
            throw new NotImplementedException();
        }

        public string GetTrainingPackagePopularity()
        {
            throw new NotImplementedException();
        }

        public bool CanDeleteHall(string hallId)
        {
            throw new NotImplementedException();
        }
    }
}
