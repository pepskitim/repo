﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Service.Access
{
    public class AccessDB : DbContext
    {
        public AccessDB() : base("modul1DB")
        {
        }

        public DbSet<EfficiencyManager> EffManagers { get; set; }

        public DbSet<FitnessCenterManager> FitnessManagers { get; set; }

        public DbSet<FitnessCenter> FitnessCenters { get; set; }

        public DbSet<Hall> Halls { get; set; }

        public DbSet<FitnessCenterNameAndServiceIp> FCNamesAndIps { get; set; }
    }
}
