﻿using CommonModulLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonModulLib.DataModel;
using Modul1Service.Access;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Modul1Service
{
    public class Modul1Contract : IModul1Contract
    {
        public bool ConnectToModul1(string fitnessCenterName, string modul2IpAddress)
        {
            OperationContext oOperationContext = OperationContext.Current;
            MessageProperties oMessageProperties = oOperationContext.IncomingMessageProperties;
            RemoteEndpointMessageProperty oRemoteEndpointMessageProperty = (RemoteEndpointMessageProperty)oMessageProperties[RemoteEndpointMessageProperty.Name];
            string szAddress = oRemoteEndpointMessageProperty.Address;

            return Modul1DB.Instance.ConnectToModul1(fitnessCenterName, szAddress);
        }

        public bool EditPersonalFCMdata(FitnessCenterManager fcMenager)
        {
            return Modul1DB.Instance.EditPersonalFCMdata(fcMenager);
        }

        public List<string> GetAllFitnessCenterByManagerName(string fcUsername)
        {
            return Modul1DB.Instance.GetAllFitnessCenterByManagerName(fcUsername);
        }

        public List<FitnessCenter> GetAllFitnessCenters()
        {
            return Modul1DB.Instance.GetAllFitnessCenters();
        }

        public List<string> GetAllHallIds(string fitnessCenterName)
        {
            return Modul1DB.Instance.GetAllHallIds(fitnessCenterName);
        }

        public List<Hall> GetAllHalls(string fitnessCenterName)
        {
            return Modul1DB.Instance.GetAllHalls(fitnessCenterName);
        }

        public FitnessCenterManager GetFitnessCenterManagerByUserName(string username)
        {
            return Modul1DB.Instance.GetFitnessCenterManagerByUserName(username);
        }

        public bool LoginFitnessCenterManager(string username, string password)
        {
            OperationContext oOperationContext = OperationContext.Current;
            MessageProperties oMessageProperties = oOperationContext.IncomingMessageProperties;
            RemoteEndpointMessageProperty oRemoteEndpointMessageProperty = (RemoteEndpointMessageProperty)oMessageProperties[RemoteEndpointMessageProperty.Name];
            string szAddress = oRemoteEndpointMessageProperty.Address;

            return Modul1DB.Instance.LoginFitnessCenterManager(username, password);
        }
    }
}
