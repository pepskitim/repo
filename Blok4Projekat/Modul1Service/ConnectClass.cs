﻿using CommonModulLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Service
{
    public class ConnectClass
    {
        public static Dictionary<string, IModul2Contract> Modul2ClientProxies = new Dictionary<string, IModul2Contract>();

        public static void AddNewModul2IpAddress(string ipAddress)
        {
            if (!Modul2ClientProxies.ContainsKey(ipAddress))
            {
                Modul2ClientProxies.Add(ipAddress, new WCFModul2Client(new NetTcpBinding(),
        new EndpointAddress(new Uri("net.tcp://" + ipAddress + ":8000/IModul2Contract"))));
            }
        }
    }
}
