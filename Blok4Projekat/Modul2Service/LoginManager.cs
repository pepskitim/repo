﻿using CommonLibM2;
using CommonModulLib;
using CommonModulLib.DataModel;
using Modul2Service.Access;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul2Service
{
    public class LoginManager : IdbContract
    {
        public AuthenticationAndAuthorization Login(string username, string password)
        {
            return DBManager.Instance.Login(username, password);
        }

        public bool AddNewCoach(string username, string password, string firstName, string lastName, string address, string universityDegree, DateTime dateOfBirth, string fitnessCenter)
        {
            //Coach coach = new Coach(username, password, firstName, lastName, address, universityDegree, dateOfBirth, fitnessCenter);

            return DBManager.Instance.SetCoach(username, password, firstName, lastName, address, universityDegree, dateOfBirth, fitnessCenter);
        }

        public bool DeleteCoach(string username)
        {
            if (DBManager.Instance.DeleteCoach(username))

            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UpdateCoach(Coach coach)
        {
            if (DBManager.Instance.UpdateCoach(coach))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Coach GetCoach(string username)
        {
            return DBManager.Instance.GetCoach(username);
        }

        public List<Coach> GetAllCoaches()
        {
            return DBManager.Instance.GetAllCoaches();
        }

        public bool AddNewMember(string username, string password, string firstName, string lastName, string address, double height, double weight, DateTime dateOfBirth)
        {
            //Member member = new Member(username, password, firstName, lastName, address, height, weight, dateOfBirth);

            if (DBManager.Instance.SetMember(username, password, firstName, lastName, address, height, weight, dateOfBirth))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteMember(string username)
        {
            if (DBManager.Instance.DeleteMember(username))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddNewTerm(string name, string day, int startingHour, int startingMinute, int endingHour, int endingMinute, string hall, Term.TrainingLevel trainingLevel, string coach, double price)
        {
            //Time time = new Time(startingHour, startingMinute, endingHour, endingMinute);
            //Term term = new Term(name, day, time, hall, trainingLevel, coach, price);

            if (DBManager.Instance.SetTerm(name, day, startingHour, startingMinute, endingHour, endingMinute, hall, trainingLevel, coach, price))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddNewTrainingPackage(List<string> termTitles, double monthPrice, string packageName)
        {
            //TrainingPackage traininingPackage = new TrainingPackage(termTitles, monthPrice, packageName);

            if (DBManager.Instance.SetTrainingPackage(termTitles, monthPrice, packageName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Term> GetAllTerms()
        {
            return DBManager.Instance.GetAllTerms();
        }

        public List<TrainingPackage> GetAllTraininingPackages()
        {
            return DBManager.Instance.GetAllTPackages();
        }

        public bool UpdateMember(string username, string password, string firstName, string lastName, string address, double height, double weight, DateTime dateOfBirth)
        {
            return DBManager.Instance.UpdateMember(new Member(username, password, firstName, lastName, address, height, weight, dateOfBirth));
        }

        public Member GetMember(string username)
        {
            return DBManager.Instance.GetMember(username);
        }

        public List<string> GetAllPackageTerms(string packageName)
        {
            return DBManager.Instance.GetAllPackageTerms(packageName);
        }

        public List<Hall> GetAllHalls(string fitnessCenterName)
        {
            return ConnectClassForM1.clientProxy.GetAllHalls(fitnessCenterName);
        }

        public List<FitnessCenter> GetAllFitnessCenters()
        {
            return ConnectClassForM1.clientProxy.GetAllFitnessCenters();
        }

        public FitnessCenterManager GetFitnessCenterManagerByUserName(string username)
        {
            return ConnectClassForM1.clientProxy.GetFitnessCenterManagerByUserName(username);
        }

        public bool EditPersonalFCMdata(FitnessCenterManager fcMenager)
        {
            return ConnectClassForM1.clientProxy.EditPersonalFCMdata(fcMenager);
        }

        public List<string> GetAllFitnessCenterByManagerName(string fcUsername)
        {
            return ConnectClassForM1.clientProxy.GetAllFitnessCenterByManagerName(fcUsername);
        }

        public List<string> GetAllHallIds(string fitnessCenterName)
        {
            return ConnectClassForM1.clientProxy.GetAllHallIds(fitnessCenterName);
        }

        public bool LoginFitnessCenterManager(string username, string password)
        {
            return ConnectClassForM1.clientProxy.LoginFitnessCenterManager(username, password);
        }

        public bool UpdateTerm(Term term)
        {
            return DBManager.Instance.UpdateTerm(term);
        }

        public bool DeleteTerm(string title)
        {
            return DBManager.Instance.DeleteTerm(title);
        }

        public bool UpdateMember(Member member)
        {
            return DBManager.Instance.UpdateMember(member);
        }

        public List<Term> GetTermsByCocachUsername(string coachUsername)
        {
            return DBManager.Instance.GetTermsByCocachUsername(coachUsername);
        }
        #region MemberPackageRelationshipManager
        public List<MemberPackageRel> GetAllMPRels()
        {
            return DBManager.Instance.GetAllMPRels();
        }

        public List<Member> GetMembersForGivenPackage(string packageName)
        {
            return DBManager.Instance.GetMembersForGivenPackage(packageName);
        }

        public List<TrainingPackage> GetPackagesForGivenMember(string memberName)
        {
            throw new NotImplementedException();
        }

        public bool SetMPRel(MemberPackageRel mprel)
        {
            return DBManager.Instance.SetMPRel(mprel);
        }
        public bool PayMPRel(string memberName, string packageName)
        {
            return DBManager.Instance.PayMPRel(memberName, packageName);
        }

        public bool DeleteMPRel(MemberPackageRel mprel)
        {
            return DBManager.Instance.DeleteMPRel(mprel);
        }
        #endregion

        #region MemberTermRelationshipManager
        public List<MemberTermRel> GetAllMTRels()
        {
            return DBManager.Instance.GetAllMTRels();
        }

        public List<Member> GetMembersForGivenTerm(string termName)
        {
            return DBManager.Instance.GetMembersForGivenTerm(termName);
        }

        public List<Term> GetTermsForGivenMember(string memberName)
        {
            throw new NotImplementedException();
        }

        public bool SetMTRel(MemberTermRel mtrel)
        {
            return DBManager.Instance.SetMTRel(mtrel);
        }

        public bool PayMTRel(string memberName, string termName)
        {
            return DBManager.Instance.PayMTRel(memberName, termName);
        }

        public bool DeleteMTRel(MemberTermRel mtrel)
        {
            return DBManager.Instance.DeleteMTRel(mtrel);
        }

        public bool DeletePackage(int id)
        {
            return DBManager.Instance.DeleteTrainingPackage(id);
        }
        #endregion

        public bool SetCanceledTraining(CanceledTraining ctObject)
        {
            return DBManager.Instance.SetCanceledTraining(ctObject);
        }

        public bool UpdatePackage(TrainingPackage package)
        {
            return DBManager.Instance.UpdateTrainingPackage(package);
        }

        public Term GetTerm(string title)
        {
            return DBManager.Instance.GetTerm(title);
        }

        public List<Term> GetTermsByManagerUsername(string managerUserName)
        {
            return DBManager.Instance.GetTermsByManagerUsername(managerUserName);
        }

        public List<string> GetNotificationsForMember(string memberUsername)
        {
            return DBManager.Instance.GetNotificationsForMember(memberUsername);
        }

        public List<TrainingPackage> GetAllTraininingPackagesForManagerUsername(string managerUsername)
        {
            return DBManager.Instance.GetAllTraininingPackagesForManagerUsername(managerUsername);
        }

        public bool ConnectToModul1(string fitnessCenterName, string modul2IpAddress)
        {
            return ConnectClassForM1.clientProxy.ConnectToModul1(fitnessCenterName, modul2IpAddress);
        }

        public bool NotifyMembersForTermChange(string termName, string notification)
        {
            return DBManager.Instance.NotifyMembersForTermChange(termName, notification);
        }

        public bool NotifyMembersForPackageChange(string packageName, string notification)
        {
            return DBManager.Instance.NotifyMembersForPackageChange(packageName, notification);
        }
    }
}
