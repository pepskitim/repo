﻿using CommonLibM2;
using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul2Service.Access
{
    public class DBManager : IDBManager
    {
        #region Initialize
        private static IDBManager myDB;

        public static IDBManager Instance
        {
            get
            {
                if (myDB == null)
                {
                    myDB = new DBManager();
                }
                return myDB;
            }
            set
            {
                if (myDB == null)
                {
                    myDB = value;
                }
            }
        }
        #endregion

        #region Coach
        public Coach GetCoach(string username)
        {
            using (var access = new AccessDB())
            {
                return access.Coaches.Find(username);
            }
        }

        public List<Coach> GetAllCoaches()
        {
            using (var access = new AccessDB())
            {
                return access.Coaches.Where(u => u != null).ToList();
            }
        }

        public bool SetCoach(string username, string password, string firstName, string lastName, string address, string universityDegree, DateTime dateOfBirth, string fitnessCenter)
        {
            Coach coach = new Coach(username, password, firstName, lastName, address, universityDegree, dateOfBirth, fitnessCenter);
            using (var access = new AccessDB())
            {
                try
                {
                    access.Coaches.Add(coach);
                    access.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(ex.StackTrace);
                    return false;
                }
            }
        }

        public bool UpdateCoach(Coach coach)
        {
            using (var access = new AccessDB())
            {
                var original = access.Coaches.Find(coach.Username);

                if (original != null)
                {
                    original.FirstName = coach.FirstName;
                    original.LastName = coach.LastName;
                    original.Address = coach.Address;
                    original.UniversityDegree = coach.UniversityDegree;
                    original.DateOfBirth = coach.DateOfBirth;

                    if (access.SaveChanges() > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool DeleteCoach(string username)
        {
            using (var access = new AccessDB())
            {
                access.Coaches.Remove(access.Coaches.Attach(access.Coaches.Find(username)));

                if (access.SaveChanges() > 0)
                {
                    return true;
                }
                return false;
            }
        }
        #endregion

        #region Member
        public Member GetMember(string username)
        {
            using (var access = new AccessDB())
            {
                return access.Members.Find(username);
            }
        }

        public bool SetMember(string username, string password, string firstName, string lastName, string address, double height, double weight, DateTime dateOfBirth)
        {
            Member member = new Member(username, password, firstName, lastName, address, height, weight, dateOfBirth);
            using (var access = new AccessDB())
            {
                try
                {
                    access.Members.Add(member);
                    access.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(ex.StackTrace);
                    return false;
                }
            }
        }
        public bool UpdateMember(Member member)
        {
            using (var access = new AccessDB())
            {
                var original = access.Members.Find(member.Username);

                if (original != null)
                {
                    original.FirstName = member.FirstName;
                    original.LastName = member.LastName;
                    original.Address = member.Address;
                    original.DateOfBirth = member.DateOfBirth;
                    original.Height = member.Height;
                    original.Weight = member.Weight;

                    if (access.SaveChanges() > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool DeleteMember(string username)
        {
            using (var access = new AccessDB())
            {
                access.Members.Remove(access.Members.Attach(access.Members.Find(username)));

                if (access.SaveChanges() > 0)
                {
                    return true;
                }
                return false;
            }
        }
        #endregion

        #region Term
        public Term GetTerm(string title)
        {
            using (var access = new AccessDB())
            {
                return access.Terms.Find(title);
            }
        }

        public bool SetTerm(string name, string day, int startingHour, int startingMinute, int endingHour, int endingMinute, string hall, Term.TrainingLevel trainingLevel, string coach, double price)
        {
            Time time = new Time(startingHour, startingMinute, endingHour, endingMinute);
            Term term = new Term(name, day, time, hall, trainingLevel, coach, price);
            using (var access = new AccessDB())
            {
                try
                {
                    access.Terms.Add(term);
                    access.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(ex.StackTrace);
                    return false;
                }
            }
        }

        public bool UpdateTerm(Term term)
        {
            using (var access = new AccessDB())
            {
                var original = access.Terms.Find(term.Title);

                if (original != null)
                {
                    original.Day = term.Day;
                    original.TimeRange = term.TimeRange;
                    original.ApointedHall = term.ApointedHall;
                    original.TrainingLvl = term.TrainingLvl;
                    original.ApointedCoach = term.ApointedCoach;
                    original.Price = term.Price;

                    if (access.SaveChanges() > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool DeleteTerm(string title)
        {
            using (var access = new AccessDB())
            {
                access.Terms.Remove(access.Terms.Attach(access.Terms.Find(title)));

                if (access.SaveChanges() > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public List<Term> GetAllTerms()
        {
            using (var access = new AccessDB())
            {
                return access.Terms.Where(u => u != null).ToList();
            }
        }
        #endregion

        #region TrainingPackage
        public TrainingPackage GetTrainingPackage(int id)
        {
            using (var access = new AccessDB())
            {
                return access.TrainingPackages.Find(id);
            }
        }

        public bool SetTrainingPackage(List<string> termTitles, double monthPrice, string packageName)
        {
            TrainingPackage trainingPackage = new TrainingPackage(termTitles, monthPrice, packageName);
            using (var access = new AccessDB())
            {
                try
                {
                    if (access.TrainingPackages.Where(u => u.PackageName.Equals(trainingPackage.PackageName)).ToList().Count == 0)
                    {
                        access.TrainingPackages.Add(trainingPackage);
                        access.SaveChanges();

                        PackageTermRel packTermRel = new PackageTermRel();
                        packTermRel.packageName = trainingPackage.PackageName;

                        foreach (var term in trainingPackage.Terms)
                        {
                            packTermRel.termTitle = term;
                            access.PackageTermTable.Add(packTermRel);
                            access.SaveChanges();
                        }

                        access.SaveChanges();
                        return true;
                    }
                    return false;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(ex.StackTrace);
                    return false;
                }
            }
        }

        public List<string> GetAllPackageTerms(string packageName)
        {
            using (var access = new AccessDB())
            {
                return access.PackageTermTable.Where(t => t.packageName.Equals(packageName)).Select(u => u.termTitle).ToList();
            }
        }

        public bool UpdateTrainingPackage(TrainingPackage trainingPackage)
        {
            using (var access = new AccessDB())
            {
                try
                {
                    var original = access.TrainingPackages.Find(trainingPackage.Id); 

                    if (original != null)
                    {
                        List<PackageTermRel> packageTermPair = access.PackageTermTable.Where(u => u.packageName.Equals(trainingPackage.PackageName)).ToList();

                        foreach (var item in packageTermPair)
                        {
                            access.PackageTermTable.Remove(access.PackageTermTable.Attach(access.PackageTermTable.Find(item.Id)));
                            access.SaveChanges();
                        }

                        PackageTermRel packageTerm = new PackageTermRel();
                        packageTerm.packageName = trainingPackage.PackageName;

                        foreach (var item in trainingPackage.Terms)
                        {
                            packageTerm.termTitle = item;
                            access.PackageTermTable.Add(packageTerm);
                            access.SaveChanges();
                        }

                        if (original.MonthPrice == trainingPackage.MonthPrice)
                        {
                            return true;
                        }
                        else
                        {
                            original.MonthPrice = trainingPackage.MonthPrice;  //name ne, jer bi trebao biti to kljuc

                            if (access.SaveChanges() > 0)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        
                    }
                    return false;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(ex.StackTrace);
                    return false;
                }
            }
        }

        public bool DeleteTrainingPackage(int id)
        {
            using (var access = new AccessDB())
            {
                List<TrainingPackage> package = access.TrainingPackages.Where(u => u.Id == id).ToList<TrainingPackage>();
                string packageName = package[0].PackageName;
                List<PackageTermRel> packageTermPair = access.PackageTermTable.Where(t => t.packageName.Equals(packageName)).ToList();

                foreach (var item in packageTermPair)
                {
                    access.PackageTermTable.Remove(access.PackageTermTable.Attach(access.PackageTermTable.Find(item.Id)));
                    access.SaveChanges();
                }

                access.TrainingPackages.Remove(access.TrainingPackages.Attach(access.TrainingPackages.Find(id)));

                if (access.SaveChanges() > 0)
                {
                    return true;
                }
                return false;
            }
        }
        #endregion

        public List<TrainingPackage> GetAllTPackages()
        {
            using (var access = new AccessDB())
            {
                return access.TrainingPackages.Where(u => u != null).ToList();
            }
        }

        #region MemberPackageRelationshipManager
        public List<MemberPackageRel> GetAllMPRels()
        {
            using (var access = new AccessDB())
            {
                return access.MemberPackageTable.Where(u => u != null).ToList();
            }
        }

        public List<Member> GetMembersForGivenPackage(string packageName)
        {
            using (var access = new AccessDB())
            {
                List<Member> tempList = new List<Member>();
                var l2e = access.MemberPackageTable.Where(t => t.packageName.Equals(packageName)).ToList();

                foreach (var item in l2e)
                {
                    var member = access.Members.Where(t => t.Username.Equals(item.memberName)).ToList();
                    if (!tempList.Contains((Member)member[0]))
                    {
                        tempList.Add((Member)member[0]);
                    }
                }

                return tempList;
            }
        }

        public List<TrainingPackage> GetPackagesForGivenMember(string memberName)
        {
            using (var access = new AccessDB())
            {
                List<TrainingPackage> tempList = new List<TrainingPackage>();
                var l2e = access.MemberPackageTable.Where(t => t.memberName.Equals(memberName)).ToList();

                foreach (var item in l2e)
                {
                    var package = access.TrainingPackages.Where(t => t.PackageName.Equals(item.packageName));
                    if (!tempList.Contains((TrainingPackage)package))
                    {
                        tempList.Add((TrainingPackage)package);
                    }
                }

                return tempList;
            }
        }

        public bool SetMPRel(MemberPackageRel mprel)
        {
            using (var access = new AccessDB())
            {
                try
                {
                    access.MemberPackageTable.Add(mprel);

                    if (access.SaveChanges() > 0)
                    {
                        return true;
                    }
                    return false;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(ex.StackTrace);
                    return false;
                }
            }
        }

        public bool PayMPRel(string memberName, string packageName)
        {
            using (var access = new AccessDB())
            {
                var toBePaidIQ = access.MemberPackageTable.Where(t => t.memberName.Equals(memberName)
                                                      && t.packageName.Equals(packageName)
                                                        ).ToList();
                MemberPackageRel toBePaid = (MemberPackageRel)toBePaidIQ[0];

                var original = access.MemberPackageTable.Find(toBePaid.Id);

                if (original != null)
                {
                    original.Paid = true;

                    if (access.SaveChanges() > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool DeleteMPRel(MemberPackageRel mprel)
        {
            using (var access = new AccessDB())
            {
                access.MemberPackageTable.Remove(access.MemberPackageTable.Attach(access.MemberPackageTable.Find(mprel.Id)));

                if (access.SaveChanges() > 0)
                {
                    return true;
                }
                return false;
            }
        }
        #endregion

        #region MemberTermRelationshipManager
        public List<MemberTermRel> GetAllMTRels()
        {
            using (var access = new AccessDB())
            {
                return access.MemberTermTable.Where(u => u != null).ToList();
            }
        }

        public List<Member> GetMembersForGivenTerm(string termName)
        {
            using (var access = new AccessDB())
            {
                List<Member> tempList = new List<Member>();
                var l2e = access.MemberTermTable.Where(t => t.termTitle.Equals(termName)).ToList();

                foreach (var item in l2e)
                {
                    var member = access.Members.Where(t => t.Username.Equals(item.memberName)).ToList();

                    if (!tempList.Contains((Member)member[0]))
                    {
                        tempList.Add((Member)member[0]);
                    }
                }

                return tempList;
            }
        }

        public List<Term> GetTermsForGivenMember(string memberName)
        {
            using (var access = new AccessDB())
            {
                List<Term> tempList = new List<Term>();

                var l2e = access.MemberTermTable.Where(t => t.memberName.Equals(memberName)).ToList();

                foreach (var item in l2e)
                {
                    var term = access.Terms.Where(t => t.Title.Equals(item.termTitle));
                    if (!tempList.Contains((Term)term))
                    {
                        tempList.Add((Term)term);
                    }
                }

                return tempList;
            }
        }

        public bool SetMTRel(MemberTermRel mtrel)
        {
            using (var access = new AccessDB())
            {
                try
                {
                    access.MemberTermTable.Add(mtrel);

                    if (access.SaveChanges() > 0)
                    {
                        return true;
                    }
                    return false;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(ex.StackTrace);
                    return false;
                }
            }
        }

        public bool PayMTRel(string memberName, string termName)
        {
            using (var access = new AccessDB())
            {
                var toBePaidIQ = access.MemberTermTable.Where(t => t.memberName.Equals(memberName)
                                                      && t.termTitle.Equals(termName)
                                                        ).ToList();
                MemberTermRel toBePaid = (MemberTermRel)toBePaidIQ[0];

                var original = access.MemberTermTable.Find(toBePaid.Id);

                if (original != null)
                {
                    original.Paid = true;

                    if (access.SaveChanges() > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool DeleteMTRel(MemberTermRel mtrel)
        {
            using (var access = new AccessDB())
            {
                access.MemberTermTable.Remove(access.MemberTermTable.Attach(access.MemberTermTable.Find(mtrel.Id)));

                if (access.SaveChanges() > 0)
                {
                    return true;
                }
                return false;
            }
        }

        #endregion

        #region CanceledTraining
        public bool SetCanceledTraining(CanceledTraining ctObject)
        {
            using (var access = new AccessDB())
            {
                try
                {
                    access.CanceledTrainings.Add(ctObject);

                    if (access.SaveChanges() > 0)
                    {
                        return true;
                    }
                    return false;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(ex.StackTrace);
                    return false;
                }
            }
        }

        public AuthenticationAndAuthorization Login(string username, string password)
        {
            using (var access = new AccessDB())
            {
                var coach = access.Coaches.Find(username);
                if (coach != null)
                {
                    if (coach.Password.Equals(password) && coach.Employed)
                    {
                        return AuthenticationAndAuthorization.COACH;
                    }
                }
                var member = access.Members.Find(username);
                if (member != null)
                {
                    if (member.Password.Equals(password))
                    {
                        return AuthenticationAndAuthorization.MEMBER;
                    }
                }

                bool isFitnessManager = ConnectClassForM1.clientProxy.LoginFitnessCenterManager(username, password);
                if (isFitnessManager)
                {
                    return AuthenticationAndAuthorization.FITNESS_CENTER_MANAGER;
                }

                return AuthenticationAndAuthorization.NONE;
            }
        }

        #endregion
        public List<Term> GetTermsByCocachUsername(string coachUsername)
        {
            using (var access = new AccessDB())
            {
                return access.Terms.Where(u => u.ApointedCoach.Equals(coachUsername)).ToList();
            }
        }

        public List<Term> GetTermsByManagerUsername(string managerUserName)
        {
            using (var access = new AccessDB())
            {
                List<Term> terms = new List<Term>();
                List<string> fitnesCenterNames = ConnectClassForM1.clientProxy.GetAllFitnessCenterByManagerName(managerUserName);
                foreach (string fitnessCenter in fitnesCenterNames)
                {
                    List<Coach> coaches = access.Coaches.Where(u => u.FitnessCenter.Equals(fitnessCenter)).ToList();
                    foreach (Coach coach in coaches)
                    {
                        List<Term> termsByCoach = access.Terms.Where(u => u.ApointedCoach.Equals(coach.Username)).ToList();
                        terms.AddRange(termsByCoach);
                    }
                }
                return terms;
            }
        }

        public List<string> GetNotificationsForMember(string memberUsername)
        {
            using (var access = new AccessDB())
            {
                var l2e = access.MemberInboxTable.Where(u => u.MemberName.Equals(memberUsername)).ToList();
                List<string> returnList = new List<string>();

                foreach (var item in l2e)
                {
                    returnList.Add(item.Notification);
                }
                return returnList;
            }
        }

        public List<TrainingPackage> GetAllTraininingPackagesForManagerUsername(string managerUsername)
        {
            using (var access = new AccessDB())
            {
                List<TrainingPackage> result = new List<TrainingPackage>();
                List<string> fitnesCenterNames = ConnectClassForM1.clientProxy.GetAllFitnessCenterByManagerName(managerUsername);
                foreach (string fitnessCenter in fitnesCenterNames)
                {
                    List<Coach> coaches = access.Coaches.Where(u => u.FitnessCenter.Equals(fitnessCenter)).ToList();
                    foreach (Coach coach in coaches)
                    {
                        List<Term> termsByCoach = access.Terms.Where(u => u.ApointedCoach.Equals(coach.Username)).ToList();
                        foreach (Term term in termsByCoach)
                        {
                            List<string> packageNames = access.PackageTermTable.Where(u => u.termTitle.Equals(term.Title)).Select(u => u.packageName).ToList();
                            foreach (string packageName in packageNames)
                            {
                                List<TrainingPackage> packages = access.TrainingPackages.Where(u => u.PackageName.Equals(packageName)).ToList();
                                TrainingPackage package = packages[0];
                                if (!result.Contains(package))
                                {
                                    result.Add(package);
                                }
                            }
                        }
                    }
                }
                return result;
            }
        }

        public FitnessCenterNameAndServiceIp GetFitnessCenterNameAndModul1Ip()
        {
            using (var access = new AccessDB())
            {
                List<FitnessCenterNameAndServiceIp> fcNames = access.FCNameAndModul1Ip.Where(u => u != null).ToList();
                if (fcNames.Count != 0)
                {
                    return fcNames[0];
                }
                return null;
            }
        }

        public void AddFitnessCenterNameAndModul1Ip(string fcName, string modul1IpAddress)
        {
            using (var access = new AccessDB())
            {
                access.FCNameAndModul1Ip.Add(new FitnessCenterNameAndServiceIp(fcName, modul1IpAddress));
                access.SaveChanges();
            }
        }

        public bool NotifyMembersForTermChange(string termName, string notification)
        {
            using (var access = new AccessDB())
            {
                var l2e = access.MemberTermTable.Where(t => t.termTitle.Equals(termName)).ToList();

                foreach (var item in l2e)
                {
                    List<Member> member = access.Members.Where(t => t.Username.Equals(item.memberName)).ToList<Member>();

                    if (member.Count == 1)
                    {
                        MemberInboxRels memberInboxRel = new MemberInboxRels();
                        memberInboxRel.MemberName = member[0].Username;
                        memberInboxRel.Notification = notification;
                        access.MemberInboxTable.Add(memberInboxRel);

                        if (access.SaveChanges() < 0)
                        {
                            return false;
                        }
                    }
                }
              
                return true;
            }
        }

        public bool NotifyMembersForPackageChange(string packageName, string notification)
        {
            using (var access = new AccessDB())
            {
                var l2e = access.MemberPackageTable.Where(t => t.packageName.Equals(packageName)).ToList();

                foreach (var item in l2e)
                {
                    List<Member> member = access.Members.Where(t => t.Username.Equals(item.memberName)).ToList<Member>();

                    if (member.Count == 1)
                    {
                        MemberInboxRels memberInboxRel = new MemberInboxRels();
                        memberInboxRel.MemberName = member[0].Username;
                        memberInboxRel.Notification = notification;
                        access.MemberInboxTable.Add(memberInboxRel);

                        if (access.SaveChanges() < 0)
                        {
                            return false;
                        }
                    }
                }
             
                return true;
            }
        }
    }
}
