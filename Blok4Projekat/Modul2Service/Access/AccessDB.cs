﻿using CommonLibM2;
using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul2Service.Access
{
    public class AccessDB : DbContext
    {
        public AccessDB() : base("modul2DB") { }

        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Term> Terms { get; set; }
        public DbSet<TrainingPackage> TrainingPackages { get; set; }
        public DbSet<Hall> Halls { get; set; }
        public DbSet<PackageTermRel> PackageTermTable { get; set; }
        public DbSet<MemberPackageRel> MemberPackageTable { get; set; }
        public DbSet<MemberTermRel> MemberTermTable { get; set; }
        public DbSet<CanceledTraining> CanceledTrainings { get; set; }
        public DbSet<MemberInboxRels> MemberInboxTable { get; set; }
        public DbSet<FitnessCenterNameAndServiceIp> FCNameAndModul1Ip { get; set; }
    }
}
