﻿using CommonModulLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonModulLib.DataModel;

namespace Modul2Service.Access
{
    public class Modul2Contract : IModul2Contract
    {
        #region Initialize
        private static IModul2Contract myM2C;

        public static IModul2Contract Instance
        {
            get
            {
                if (myM2C == null)
                {
                    myM2C = new Modul2Contract();
                }

                return myM2C;
            }
            set
            {
                if (myM2C == null)
                {
                    myM2C = value;
                }
            }
        }
        #endregion

        public bool ApprovalCoachJob(Coach coach, bool approve)
        {
            using (var access = new AccessDB())
            {
                try
                {
                    var original = access.Coaches.Find(coach.Username);

                    if (original != null)
                    {
                        original.Employed = approve;

                        access.SaveChanges();
                        return true;
                    }
                    return false;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public bool CanDeleteHall(string hallId)
        {
            using (var access = new AccessDB())
            {
                List<Term> terms = access.Terms.Where(u => u.ApointedHall.Equals(hallId)).ToList();
                return terms.Count == 0;
            }
        }

        public List<string> GetAllCoachUsernames()
        {
            using (var access = new AccessDB())
            {
                return access.Coaches.Select(u => u.Username).ToList();
            }
        }

        public string GetCanceledTrainings()
        {
            string ctReport = "";
            using (var access = new AccessDB())
            {
                foreach (var item in access.CanceledTrainings)
                {
                    ctReport += item.FitnessCenter + "=" + item.Training + ";";
                }
                return ctReport;
            }
        }

        public string GetCoachesActivity()
        {
            string caReport = "";
            Dictionary<string, int> activity = new Dictionary<string, int>();
            using (var access = new AccessDB())
            {
                foreach (var item in access.Coaches)
                {
                    activity.Add(item.Username, 0);
                }
                foreach (var item in access.Terms)
                {
                    activity[item.ApointedCoach]++;
                }
                foreach (var item in activity)
                {
                    caReport += item.Key + "=" + item.Value + ";";
                }
                return caReport;
            }
        }

        public List<Coach> GetCoachesByFitnessCenterName(string fitnessCenterName)
        {
            using (var access = new AccessDB())
            {
                return access.Coaches.Where(u => u.FitnessCenter.Equals(fitnessCenterName) && u.Employed).ToList();
            }
        }

        public List<Coach> GetCoachesWaitForJob()
        {
            using (var access = new AccessDB())
            {
                return access.Coaches.Where(u => !u.Employed).ToList();
            }
        }

        public string GetFitnessCentersProfits()
        {
            string fcReport = "";
            string fcName = "";
            double profit = 0;
            Dictionary<string, double> profits = new Dictionary<string, double>();
            using (var access = new AccessDB())
            {
                foreach (var item in access.TrainingPackages)
                {
                    profits.Add(item.PackageName, item.MonthPrice);
                }
                foreach (var item in access.Terms)
                {
                    profits.Add(item.Title, item.Price);
                }
                foreach (var item in access.MemberPackageTable)
                {
                    profit += profits[item.packageName];
                }
                foreach (var item in access.MemberTermTable)
                {
                    if (item.Paid)
                    {
                        profit += profits[item.termTitle];
                    }
                }
                foreach (var coach in access.Coaches)
                {
                    fcName += coach.FitnessCenter;
                    break;
                }
                fcReport += fcName + "=" + profit.ToString() + ";";
                return fcReport;
            }
        }

        public List<Term> GetTermsByCocachUsername(string coachUsername)
        {
            using (var access = new AccessDB())
            {
                return access.Terms.Where(u => u.ApointedCoach.Equals(coachUsername)).ToList();
            }
        }

        public List<Term> GetTermsByFitnessCenterName(string fitnessCenterName)
        {
            using (var access = new AccessDB())
            {
                List<Term> filteredTerms = new List<Term>();
                foreach (var item in access.Terms.Where(u => u != null).ToList())
                {
                    foreach (var coach in access.Coaches.Where(u => u != null).ToList())
                    {
                        if (item.ApointedCoach.Equals(coach.Username) && coach.FitnessCenter.Equals(fitnessCenterName))
                        {
                            filteredTerms.Add(item);
                            break;
                        }
                    }
                }
                return filteredTerms;
            }
        }

        public string GetTrainingPackagePopularity()
        {
            string tppReport = "";
            Dictionary<string, int> popularity = new Dictionary<string, int>();
            using (var access = new AccessDB())
            {
                foreach (var item in access.TrainingPackages)
                {
                    popularity.Add(item.PackageName, 0);
                }
                foreach (var item in access.MemberPackageTable)
                {
                    popularity[item.packageName]++;
                }
                foreach (var item in popularity)
                {
                    tppReport += item.Key + "=" + item.Value + ";";
                }
                return tppReport;
            }
        }
    }
}
