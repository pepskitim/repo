﻿using CommonLibM2;
using CommonModulLib;
using CommonModulLib.DataModel;
using Modul2Service.Access;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Modul2Service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            StartUpBase();

            if (ConnectToModul1())
            {
                NetTcpBinding binding = new NetTcpBinding();
                string address = "net.tcp://localhost:9000/IdbContract";

                ServiceHost host = new ServiceHost(typeof(LoginManager));
                host.AddServiceEndpoint(typeof(IdbContract), binding, address);

                //InterModul Communication
                NetTcpBinding binding1 = new NetTcpBinding();
                string address1 = "net.tcp://localhost:8000/IModul2Contract";

                ServiceHost host1 = new ServiceHost(typeof(Modul2Contract));
                host1.AddServiceEndpoint(typeof(IModul2Contract), binding1, address1);


                host.Open();
                host1.Open();
                Console.WriteLine("Modul2 Service is opened. Press <enter> to finish...");

                Console.WriteLine("InterModul Service is opened. Press <enter> to finish...");
                //DBManager.Instance.GetAllPackageTerms("pekedz"); 
                Console.ReadLine();

                host.Close();
                host1.Close();
            }
            else
            {
                Console.WriteLine("Can not connect to Modul1.");
                Console.ReadLine();
            }
        }

        private static void StartUpBase()
        {
            string executable = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string path = System.IO.Path.GetDirectoryName(executable);
            path = path.Substring(0, path.LastIndexOf("bin")) + "DB";
            AppDomain.CurrentDomain.SetData("DataDirectory", path);

            // update database
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AccessDB, Configuration>());
        }

        private static bool ConnectToModul1()
        {
            FitnessCenterNameAndServiceIp fcNameAndModul1Ip = DBManager.Instance.GetFitnessCenterNameAndModul1Ip();
            string myIpAddress = GetLocalIPAddress();
            if (fcNameAndModul1Ip == null)
            {
                Console.WriteLine("Input your Fitness Center Name: ");
                string fcName = Console.ReadLine();
                Console.WriteLine("Input Modul1 Service Ip: ");
                string modul1IpAddress = Console.ReadLine();
                ConnectClassForM1.SetModul1IpAddress(modul1IpAddress);
                if (ConnectClassForM1.clientProxy.ConnectToModul1(fcName, myIpAddress))
                {
                    DBManager.Instance.AddFitnessCenterNameAndModul1Ip(fcName, modul1IpAddress);
                    return true;
                }
                return false;
            }
            else
            {
                ConnectClassForM1.SetModul1IpAddress(fcNameAndModul1Ip.ServiceIp);
                return ConnectClassForM1.clientProxy.ConnectToModul1(fcNameAndModul1Ip.FitnessCenterName, myIpAddress);
            }
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return string.Empty;
        }
    }
}
