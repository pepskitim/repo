﻿using CommonModulLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Modul2Service
{
    public class ConnectClassForM1
    {
        public static IModul1Contract clientProxy;

        public static void SetModul1IpAddress(string ip)
        {
            clientProxy = new WCFClientForM1(new NetTcpBinding(), new EndpointAddress(new Uri("net.tcp://" + ip + ":9998/IModul1Contract")));
        }

    }
}
