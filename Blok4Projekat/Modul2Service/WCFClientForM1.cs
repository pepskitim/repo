﻿using CommonModulLib;
using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Modul2Service
{
    public class WCFClientForM1 : ChannelFactory<IModul1Contract>, IModul1Contract, IDisposable
    {
        private IModul1Contract factory;

        public WCFClientForM1(NetTcpBinding binding, EndpointAddress address)
            : base(binding, address)
        {
            factory = this.CreateChannel();
        }

        public bool ConnectToModul1(string fitnessCenterName, string modul2IpAddress)
        {
            return factory.ConnectToModul1(fitnessCenterName, modul2IpAddress);
        }

        public bool EditPersonalFCMdata(FitnessCenterManager fcMenager)
        {
            return factory.EditPersonalFCMdata(fcMenager);
        }

        public List<string> GetAllFitnessCenterByManagerName(string fcUsername)
        {
            return factory.GetAllFitnessCenterByManagerName(fcUsername);
    }

        public List<FitnessCenter> GetAllFitnessCenters()
        {
            return factory.GetAllFitnessCenters();
        }

        public List<string> GetAllHallIds(string fitnessCenterName)
        {
            return factory.GetAllHallIds(fitnessCenterName);
        }

        public List<Hall> GetAllHalls(string fitnessCenterName)
        {
            return factory.GetAllHalls(fitnessCenterName);
        }

        public FitnessCenterManager GetFitnessCenterManagerByUserName(string username)
        {
            return factory.GetFitnessCenterManagerByUserName(username);
        }

        public bool LoginFitnessCenterManager(string username, string password)
        {
            return factory.LoginFitnessCenterManager(username, password);
        }
    }
}
