﻿using CommonLib;
using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace GUIClient
{
    public class WCFClient : ChannelFactory<IContract>, IContract, IDisposable
    {
        public IContract factory;

        public WCFClient(NetTcpBinding binding, EndpointAddress address)
            : base(binding, address)
        {
            factory = this.CreateChannel();
        }

        public WCFClient() { }

        public AuthenticationAndAuthorization Login(string username, string password)
        {
            return factory.Login(username, password);
        }

        public bool AddNewEfficiencyManager(string username, string password, string firstName, string lastName, string address, string telephone)
        {
            return factory.AddNewEfficiencyManager(username, password, firstName, lastName, address, telephone);
        }

        public bool AddNewFitnessManager(string username, string password, string firstName, string lastName, string address, string telephone)
        {
            return factory.AddNewFitnessManager(username, password, firstName, lastName, address, telephone);
        }

        public bool AddNewFitnessCenter(string name, string address, string telephone, int numOfHalls, string fcManagerUsername, Time workingTime)
        {
            return factory.AddNewFitnessCenter(name, address, telephone, numOfHalls, fcManagerUsername, workingTime);
        }

        public List<string> GetFitnessCenterManagerUsernames()
        {
            return factory.GetFitnessCenterManagerUsernames();
        }

        public List<FitnessCenter> GetFitnessCenters()
        {
            return factory.GetFitnessCenters();
        }

        public bool EditFitnessCenter(string name, string address, string telephone, int numOfHalls, string fcManagerUsername)
        {
            return factory.EditFitnessCenter(name, address, telephone, numOfHalls, fcManagerUsername);
        }

        public List<string> GetFitnessCenterNames()
        {
            return factory.GetFitnessCenterNames();
        }

        public bool AddNewHall(string id, TypeOfHall type, string fitnessCenterName)
        {
            return factory.AddNewHall(id, type, fitnessCenterName);
        }

        public List<Hall> GetHallsBySelectedFitnessCenter(string fitnessCenterName)
        {
            return factory.GetHallsBySelectedFitnessCenter(fitnessCenterName);
        }

        public bool DeleteHall(string hallID)
        {
            return factory.DeleteHall(hallID);
        }


        public bool EditHall(string id, TypeOfHall hallType, string fitnessCenter)
        {
            return factory.EditHall(id, hallType, fitnessCenter);
        }

        public List<FitnessCenterManager> GetFitnessCenterManagers()
        {
            return factory.GetFitnessCenterManagers();
        }

        public bool EditEfficinencyManager(string username, string name, string lastName, string address, string telephone)
        {
            return factory.EditEfficinencyManager(username, name, lastName, address, telephone);
        }

        public EfficiencyManager GetEfficinencyManagerByUsername(string username)
        {
            return factory.GetEfficinencyManagerByUsername(username);
        }

        public List<EfficiencyManager> GetEfficiencyManagers()
        {
            return factory.GetEfficiencyManagers();
        }

        public FitnessCenter GetFitnessCenterByName(string name)
        {
            return factory.GetFitnessCenterByName(name);
        }

        public List<Coach> GetCoachesByFitnessCenterName(string fitnessCenterName)
        {
            return factory.GetCoachesByFitnessCenterName(fitnessCenterName);
        }

        public List<Term> GetTermsByFitnessCenterName(string fitnessCenterName)
        {
            return factory.GetTermsByFitnessCenterName(fitnessCenterName);
        }

        public List<string> GetAllCoachUsernames()
        {
            return factory.GetAllCoachUsernames();
        }

        public List<Term> GetTermsByCocachUsername(string coachUsername)
        {
            return factory.GetTermsByCocachUsername(coachUsername);
        }

        public List<Coach> GetCoachesWaitForJob()
        {
            return factory.GetCoachesWaitForJob();
        }

        public bool ApprovalCoachJob(Coach coach, bool approve)
        {
            return factory.ApprovalCoachJob(coach, approve);
        }

        public string GetCanceledTrainings()
        {
            return factory.GetCanceledTrainings();
        }

        public string GetCoachesActivity()
        {
            return factory.GetCoachesActivity();
        }

        public string GetFitnessCentersProfits()
        {
            return factory.GetFitnessCentersProfits();
        }

        public string GetTrainingPackagePopularity()
        {
            return factory.GetTrainingPackagePopularity();
        }

        public bool CanDeleteHall(string hallId)
        {
            return factory.CanDeleteHall(hallId);
        }

        public void Dispose()
        {
            if (factory != null)
            {
                factory = null;
            }

            this.Close();
        }
    }
}
