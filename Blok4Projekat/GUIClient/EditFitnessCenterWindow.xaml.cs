﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for EditFitnessCenterWindow.xaml
    /// </summary>
    public partial class EditFitnessCenterWindow : Window
    {
        private DataGrid fitnessCenterDataGrid;
        private ComboBox fcComboBox;
        public EditFitnessCenterWindow(ComboBox cb, DataGrid dg, FitnessCenter selectedFitnessCenter)
        {
            InitializeComponent();
            nameTextBox.Text = selectedFitnessCenter.Name;
            addressTextBox.Text = selectedFitnessCenter.Address;
            telephoneTexBox.Text = selectedFitnessCenter.Telephone;
            numOfHallsTexBox.Text = selectedFitnessCenter.NumOfHalls.ToString();
            fcManagerComboBox.ItemsSource = ConnectClass.clientProxy.GetFitnessCenterManagerUsernames();
            fcManagerComboBox.SelectedItem = selectedFitnessCenter.FcManagerUsername;
            fitnessCenterDataGrid = dg;
            fcComboBox = cb;
        }

        private void EditFitnessCenter(object sender, RoutedEventArgs e)
        {
            string name = nameTextBox.Text;
            string address = addressTextBox.Text;
            string telephone = telephoneTexBox.Text;
            int numOfHalls;
            bool numOfHallsValidNumber = int.TryParse(numOfHallsTexBox.Text, out numOfHalls);
            if (name != "" && address != "" && telephone != "" && numOfHallsValidNumber && numOfHalls > 0 && fcManagerComboBox.SelectedItem != null)
            {
                if (ConnectClass.clientProxy.EditFitnessCenter(name, address, telephone, numOfHalls, fcManagerComboBox.SelectedItem.ToString()))
                {
                    MessageBox.Show("Fitness Center successfully edited.", "Successfull", MessageBoxButton.OK, MessageBoxImage.Information);
                    fitnessCenterDataGrid.ItemsSource = ConnectClass.clientProxy.GetFitnessCenters();
                    fcComboBox.ItemsSource = ConnectClass.clientProxy.GetFitnessCenterNames();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Can not edit Fitness center with name " + name + ".", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Please fill all fields with correct data.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
