﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUIClient
{
   public class Parser
    {
        public static Dictionary<string, string> StringToDictionary(string reportString)
        {
            Dictionary<string, string> reportDictionary = new Dictionary<string, string>();

            string[] allStrings = reportString.Split(';');
            string[] retVal;

            foreach (string s in allStrings)
            {
                if (s != "")
                {
                    retVal = s.Split('=');
                    reportDictionary.Add(retVal[0], retVal[1]);
                }
                
            }

            return reportDictionary;
        }

        public static string StringForReport(string reportString)
        {
            string retVal = "";

            string[] allStrings = reportString.Split(';');
            

            foreach (string s in allStrings)
            {

                retVal += s + "\n"; 
            }

            return retVal;
        }
    }
}
