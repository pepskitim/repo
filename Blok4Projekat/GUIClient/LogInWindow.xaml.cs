﻿using CommonLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for LogInWindow.xaml
    /// </summary>
    public partial class LogInWindow : Window
    {
        public LogInWindow()
        {
            InitializeComponent();
        }

        private void logInButton_Click(object sender, RoutedEventArgs e)
        {
            string username = UsernameTextBox.Text;
            string password = passwordTextBox.Password;
            if (username != string.Empty && password != string.Empty)
            {
                username = UsernameTextBox.Text;
                AuthenticationAndAuthorization res = ConnectClass.clientProxy.Login(username, password);
                
                switch (res)
                {
                    case AuthenticationAndAuthorization.ADMIN:
                        Admin a = new Admin();
                        a.Show();
                        this.Close();
                        break;
                    case AuthenticationAndAuthorization.EFF_MANAGER:
                        EfficiencyManagerWindow w = new EfficiencyManagerWindow(username);
                        w.Show();
                        this.Close();
                        break;
                    case AuthenticationAndAuthorization.NONE:
                        MessageBox.Show("Username and password are not valid", "Error", MessageBoxButton.OK, MessageBoxImage.Stop);
                        break;
                }
            }
            else
            {
                MessageBox.Show("Username and password can not be empty.", "Error", MessageBoxButton.OK, MessageBoxImage.Stop);
            }
        }

    }
}
