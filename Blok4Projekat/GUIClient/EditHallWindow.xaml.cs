﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for EditHallWindow.xaml
    /// </summary>
    public partial class EditHallWindow : Window
    {
        public EditHallWindow(string hallID, string oldFitnessCenter, TypeOfHall oldTypeOfHall)
        {
            InitializeComponent();
            idEditHallTextBox.Text = hallID;
            fitnesCenterHallComboBox.ItemsSource = ConnectClass.clientProxy.GetFitnessCenterNames();
            fitnesCenterHallComboBox.SelectedItem = oldFitnessCenter;
            typeHallComboBox.ItemsSource = Enum.GetValues(typeof(TypeOfHall));
            typeHallComboBox.SelectedItem = oldTypeOfHall;
        }

        private void editButton_Click_1(object sender, RoutedEventArgs e)
        {
            string id = idEditHallTextBox.Text;
            if (id != ""  && typeHallComboBox.SelectedItem != null && fitnesCenterHallComboBox.SelectedItem != null)
            {
                if (ConnectClass.clientProxy.EditHall(id, (TypeOfHall)typeHallComboBox.SelectedItem, fitnesCenterHallComboBox.SelectedItem.ToString()))
                {
                    MessageBox.Show("Hall successfully edited.", "Successfull", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Can not edit Hall with ID " + id + ".", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Please fill all fields.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
