﻿using CommonLib;
using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for Admin.xaml
    /// </summary>
    public partial class Admin : Window
    {
        public Admin()
        {
            InitializeComponent();
            fitnessCenterDataGrid.ItemsSource = ConnectClass.clientProxy.GetFitnessCenters();
            fcComboBox.ItemsSource = ConnectClass.clientProxy.GetFitnessCenterNames();
            effManagersDataGrid.ItemsSource = ConnectClass.clientProxy.GetEfficiencyManagers();
            fcManagersDataGrid.ItemsSource = ConnectClass.clientProxy.GetFitnessCenterManagers();
        }

        private void AddNewEfficiencyManager(object sender, RoutedEventArgs e)
        {
            NewManagerWindow w = new NewManagerWindow(TypeOfManager.EfficiencyManager, effManagersDataGrid);
            w.Show();
        }

        private void AddNewManagerFitnessCenter(object sender, RoutedEventArgs e)
        {
            NewManagerWindow w = new NewManagerWindow(TypeOfManager.FitnessManager, fcManagersDataGrid);

            w.Show();
        }

        private void AddNewFitnessCenter(object sender, RoutedEventArgs e)
        {
            NewFitnessCenterWindow w = new NewFitnessCenterWindow(fcComboBox, fitnessCenterDataGrid);
            w.Show();
        }

        private void EditFitnessCenter(object sender, RoutedEventArgs e)
        {
            if (fitnessCenterDataGrid.SelectedItem != null)
            {
                FitnessCenter selectedFitnessCenter = (FitnessCenter)fitnessCenterDataGrid.SelectedItem;
                EditFitnessCenterWindow w = new EditFitnessCenterWindow(fcComboBox, fitnessCenterDataGrid, selectedFitnessCenter);
                w.Show();
            }
        }

        private void AddNewHall(object sender, RoutedEventArgs e)
        {
            NewHallWindow w = new NewHallWindow();
            w.Show();
            hallsDataGrid.ItemsSource = null;
        }

        private void ShowHallsBySelectedFitnessCenter(object sender, RoutedEventArgs e)
        {
            if (fcComboBox.SelectedItem != null)
            {
                hallsDataGrid.ItemsSource = ConnectClass.clientProxy.GetHallsBySelectedFitnessCenter(fcComboBox.SelectedItem.ToString());
            }
        }

        private void hallsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            editHallButton.IsEnabled = true;
            deleteHallButton.IsEnabled = true;
        }

        private void deleteHallButton_Click_1(object sender, RoutedEventArgs e)
        {
            Hall selectedHall = (Hall)hallsDataGrid.SelectedItem;
            if (selectedHall != null)
            {
                if (ConnectClass.clientProxy.CanDeleteHall(selectedHall.Id))
                {
                    if (ConnectClass.clientProxy.DeleteHall(selectedHall.Id))
                    {
                        MessageBox.Show("Hall with ID " + selectedHall.Id + " successfully deleted.", "Successfull", MessageBoxButton.OK, MessageBoxImage.Information);
                        /// hallsDataGrid.ItemsSource = null;
                        hallsDataGrid.ItemsSource = ConnectClass.clientProxy.GetHallsBySelectedFitnessCenter(fcComboBox.SelectedItem.ToString());

                    }
                    else
                    {
                        MessageBox.Show("Can not delete Hall with ID " + selectedHall.Id + ".", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Can not delete Hall with ID " + selectedHall.Id + ".", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void editHallButton_Click_1(object sender, RoutedEventArgs e)
        {
            Hall selectedHall = (Hall)hallsDataGrid.SelectedItem;
            if (selectedHall != null)
            {
                EditHallWindow w = new EditHallWindow(selectedHall.Id, selectedHall.FitnessCenterName, selectedHall.Type);
                w.Show();
                hallsDataGrid.ItemsSource = null;
            }
        }

        private void LogoutAdmin(object sender, RoutedEventArgs e)
        {
            LogInWindow l = new LogInWindow();
            l.Show();
            this.Close();
        }
    }
}
