﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for NewFitnessCenterWindow.xaml
    /// </summary>
    public partial class NewFitnessCenterWindow : Window
    {
        private DataGrid fitnessCenterDataGrid;
        private ComboBox fcComboBox;
        public NewFitnessCenterWindow(ComboBox cb, DataGrid dg)
        {
            InitializeComponent();
            fcManagerComboBox.ItemsSource = ConnectClass.clientProxy.GetFitnessCenterManagerUsernames();
            fitnessCenterDataGrid = dg;
            fcComboBox = cb;
            startHourComboBox.ItemsSource = endHourComboBox.ItemsSource = Enumerable.Range(0, 24);
            startMinuteComboBox.ItemsSource = endMinuteComboBox.ItemsSource = new short[] { 0, 10, 20, 30, 40, 50 };
        }

        private void AddNewFitnessCenter(object sender, RoutedEventArgs e)
        {
            string name = nameTextBox.Text;
            string address = addressTextBox.Text;
            string telephone = telephoneTexBox.Text;
            int numOfHalls;
            bool numOfHallsValidNumber = int.TryParse(numOfHallsTexBox.Text, out numOfHalls);
            if (name != "" && address != "" && telephone != "" && numOfHallsValidNumber && numOfHalls > 0 && fcManagerComboBox.SelectedItem != null &&
                startHourComboBox.SelectedItem != null && startMinuteComboBox.SelectedItem != null &&
                endHourComboBox.SelectedItem != null && endMinuteComboBox.SelectedItem != null)
            {
                Time workingTime = new Time(int.Parse(startHourComboBox.SelectedItem.ToString()),
                    int.Parse(startMinuteComboBox.SelectedItem.ToString()),
                    int.Parse(endHourComboBox.SelectedItem.ToString()),
                    int.Parse(endMinuteComboBox.SelectedItem.ToString()));
                if (ConnectClass.clientProxy.AddNewFitnessCenter(name, address, telephone, numOfHalls, fcManagerComboBox.SelectedItem.ToString(), workingTime))
                {
                    MessageBox.Show("New Fitness Center successfully added.", "Successfull", MessageBoxButton.OK, MessageBoxImage.Information);
                    fitnessCenterDataGrid.ItemsSource = ConnectClass.clientProxy.GetFitnessCenters();
                    fcComboBox.ItemsSource = ConnectClass.clientProxy.GetFitnessCenterNames();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Can not add new Fitness Center. Fitness Center with name " + name +
                                " already exists or internal error on serfer.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Please fill all fields with correct data.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
