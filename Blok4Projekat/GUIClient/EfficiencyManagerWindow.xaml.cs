﻿using CommonModulLib.DataModel;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for EfficiencyManagerWindow.xaml
    /// </summary>
    public partial class EfficiencyManagerWindow : System.Windows.Window
    {
        private string loginUsername;

        public EfficiencyManagerWindow(string username)
        {
            InitializeComponent();
            loginUsername = username;
            fitnessCenterDataGrid.ItemsSource = ConnectClass.clientProxy.GetFitnessCenters();
            selectionComboBox.ItemsSource = Enum.GetValues(typeof(ShowSelection));
            reportTypeComboBox.ItemsSource = Enum.GetValues(typeof(TypeOfReport));
            reportFitnessCentercomboBox.ItemsSource = ConnectClass.clientProxy.GetFitnessCenterNames();
            reportManagercomboBox.ItemsSource = ConnectClass.clientProxy.GetFitnessCenterManagerUsernames();
            EfficiencyManager manager = ConnectClass.clientProxy.GetEfficinencyManagerByUsername(loginUsername);
            profileTextBox.Text = manager.ToString();

            //ceka se implementacija od modula 2
            //TODO 

            selectionCoachComboBox.ItemsSource = ConnectClass.clientProxy.GetAllCoachUsernames();

            //TODO 
            coachRequestDataGrid.ItemsSource = ConnectClass.clientProxy.GetCoachesWaitForJob();
        }

        private void ShowSelectedType(object sender, RoutedEventArgs e)
        {
            FitnessCenter selectedFitnessCenter = (FitnessCenter)fitnessCenterDataGrid.SelectedItem;

            if (selectedFitnessCenter != null && selectionComboBox.SelectedItem != null)
            {
                ShowSelection selectedCriterion = (ShowSelection)selectionComboBox.SelectedItem;
                switch (selectedCriterion)
                {
                    case ShowSelection.Hall:
                        selectedItemDataGrid.ItemsSource = ConnectClass.clientProxy.GetHallsBySelectedFitnessCenter(selectedFitnessCenter.Name);
                        break;
                    case ShowSelection.Coach:
                        selectedItemDataGrid.ItemsSource = ConnectClass.clientProxy.GetCoachesByFitnessCenterName(selectedFitnessCenter.Name);
                        break;
                    case ShowSelection.Term:
                        selectedItemDataGrid.ItemsSource = ConnectClass.clientProxy.GetTermsByFitnessCenterName(selectedFitnessCenter.Name);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                MessageBox.Show("Please select Fitness Center and Criterion.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void showTermsButton_Click(object sender, RoutedEventArgs e)
        {
            if (selectionCoachComboBox.SelectedItem != null)
            {
                string selectedCoachUsername = selectionCoachComboBox.SelectedItem.ToString();
                termsDataGrid.ItemsSource = ConnectClass.clientProxy.GetTermsByCocachUsername(selectedCoachUsername);
            }
            else
            {
                MessageBox.Show("Please select Coach.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void ApproveCoachJob(object sender, RoutedEventArgs e)
        {
            List<Coach> temp = new List<Coach>();
            Coach selectedCoach = (Coach)coachRequestDataGrid.SelectedItem;
            if (selectedCoach != null)
            {
                if (ConnectClass.clientProxy.ApprovalCoachJob(selectedCoach, true))
                {
                    temp = (List<Coach>)coachRequestDataGrid.ItemsSource;
                    temp.Remove(selectedCoach);
                    MessageBox.Show("Coach succesfully hired.", "Successfull", MessageBoxButton.OK, MessageBoxImage.Information);
                    coachRequestDataGrid.Items.Refresh();
                }
                else
                {
                    MessageBox.Show("Coach can not be hired.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void RejectCoachJob(object sender, RoutedEventArgs e)
        {
            List<Coach> temp = new List<Coach>();
            Coach selectedCoach = (Coach)coachRequestDataGrid.SelectedItem;
            if (selectedCoach != null)
            {
                if (ConnectClass.clientProxy.ApprovalCoachJob(selectedCoach, false))
                {
                    temp = (List<Coach>)coachRequestDataGrid.ItemsSource;
                    temp.Remove(selectedCoach);
                    MessageBox.Show("Coach rejected.", "Successfull", MessageBoxButton.OK, MessageBoxImage.Information);
                    coachRequestDataGrid.Items.Refresh();
                }
                else
                {
                    MessageBox.Show("Coach can not be rejected.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Please select Coach.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ShowReport(object sender, RoutedEventArgs e)
        {
            if (reportTypeComboBox.SelectedItem != null)
            {
                TypeOfReport reportType = (TypeOfReport)reportTypeComboBox.SelectedItem;
                string report = string.Empty;

                switch (reportType)
                {
                    case TypeOfReport.CanceledTrainings:
                        report = ConnectClass.clientProxy.GetCanceledTrainings();
                        break;
                    case TypeOfReport.CoachActivity:
                        report = ConnectClass.clientProxy.GetCoachesActivity();
                        break;
                    case TypeOfReport.FitnessCenterProfit:
                        report = ConnectClass.clientProxy.GetFitnessCentersProfits();
                        break;
                    case TypeOfReport.TrainingPackagePopularity:
                        report = ConnectClass.clientProxy.GetTrainingPackagePopularity();
                        break;
                    default:
                        break;
                }
                reportTextBox.Text = Parser.StringForReport(report);
            }
            else
            {
                MessageBox.Show("Please select Report type.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void changeManagerFitnessCenterButton_Click(object sender, RoutedEventArgs e)
        {
            if (reportFitnessCentercomboBox.SelectedItem != null && reportManagercomboBox.SelectedItem != null)
            {
                string selectedFitnessCenterName = reportFitnessCentercomboBox.SelectedItem.ToString();
                string selectedFCManagerUsername = reportManagercomboBox.SelectedItem.ToString();
                FitnessCenter selectedFitnessCenter = ConnectClass.clientProxy.GetFitnessCenterByName(selectedFitnessCenterName);

                if (ConnectClass.clientProxy.EditFitnessCenter(selectedFitnessCenter.Name, selectedFitnessCenter.Address, selectedFitnessCenter.Telephone, selectedFitnessCenter.NumOfHalls, selectedFCManagerUsername))
                {
                    MessageBox.Show("Manager for Fitness Center " + selectedFitnessCenterName + " successfully changed.", "Successfull", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Can not change manager for Fitness Center " + selectedFitnessCenterName + ".", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Please select Fitness Center and Manager.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void profileEditButton_Click(object sender, RoutedEventArgs e)
        {
            EditEffManagerWindow w = new EditEffManagerWindow(loginUsername, profileTextBox);
            w.Show();
        }

        private void exportReportButton_Click(object sender, RoutedEventArgs e)
        {
            TypeOfReport reportType = (TypeOfReport)reportTypeComboBox.SelectedItem;
            string report = string.Empty;
            Dictionary<string, string> reportDictionary;
            switch (reportType)
            {
                case TypeOfReport.CanceledTrainings:
                    report = ConnectClass.clientProxy.GetCanceledTrainings();
                    break;
                case TypeOfReport.CoachActivity:
                    report = ConnectClass.clientProxy.GetCoachesActivity();
                    break;
                case TypeOfReport.FitnessCenterProfit:
                    report = ConnectClass.clientProxy.GetFitnessCentersProfits();
                    break;
                case TypeOfReport.TrainingPackagePopularity:
                    report = ConnectClass.clientProxy.GetTrainingPackagePopularity();
                    break;
                default:
                    break;

            }
            reportDictionary = Parser.StringToDictionary(report);
            convertToExcel(reportDictionary, reportType);
        }

        private void convertToExcel(Dictionary<string, string> report, TypeOfReport reportType)
        {
            Microsoft.Office.Interop.Excel.Application exapp = new Microsoft.Office.Interop.Excel.Application();
            Workbook work = exapp.Workbooks.Add(XlSheetType.xlWorksheet);
            Worksheet workSheet = (Worksheet)exapp.ActiveSheet;

            exapp.Visible = true;
            switch (reportType)
            {
                case TypeOfReport.CanceledTrainings:
                    workSheet.Cells[1, 1] = "Fitness Center";
                    workSheet.Cells[1, 2] = "Training";
                    break;
                case TypeOfReport.CoachActivity:
                    workSheet.Cells[1, 1] = "Coach";
                    workSheet.Cells[1, 2] = "Activity";
                    break;
                case TypeOfReport.FitnessCenterProfit:
                    workSheet.Cells[1, 1] = "Fitness Center";
                    workSheet.Cells[1, 2] = "Profit";
                    break;
                case TypeOfReport.TrainingPackagePopularity:
                    workSheet.Cells[1, 1] = "Training Package";
                    workSheet.Cells[1, 2] = "Num of clients";
                    break;
                default:
                    break;

            }
            int i = 2;
            foreach (var item in report)
            {
                workSheet.Cells[i, 1] = item.Key;
                workSheet.Cells[i, 2] = item.Value;
                i++;
            }


        }

        private void LogoutEffManager(object sender, RoutedEventArgs e)
        {
            LogInWindow l = new LogInWindow();
            l.Show();
            this.Close();
        }
    }

    public enum ShowSelection
    {
        Hall = 0,
        Term,
        Coach,
    }

    public enum TypeOfReport
    {
        CoachActivity,
        TrainingPackagePopularity,
        FitnessCenterProfit,
        CanceledTrainings
    }
}
