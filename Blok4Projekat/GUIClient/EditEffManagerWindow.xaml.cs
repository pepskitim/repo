﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for EditEffManagerWindow.xaml
    /// </summary>
    public partial class EditEffManagerWindow : Window
    {
        private string loginUsername;
        private TextBox profileTextBox;

        public EditEffManagerWindow(string username, TextBox tb)
        {
            InitializeComponent();
            loginUsername = username;
            profileTextBox = tb;
            EfficiencyManager manager = ConnectClass.clientProxy.GetEfficinencyManagerByUsername(loginUsername);
            EditEffManaNameTextBox.Text = manager.FirstName;
            EditEffManaLastNameTextBox.Text = manager.LastName;
            EditEffManaAddressTextBox.Text = manager.Address;
            EditEffManaTelephoneTextBox.Text = manager.Telephone;
        }

        private void EditEffManaButton_Click(object sender, RoutedEventArgs e)
        {
            string name = EditEffManaNameTextBox.Text;
            string lastName = EditEffManaLastNameTextBox.Text;
            string address = EditEffManaAddressTextBox.Text;
            string telephone = EditEffManaTelephoneTextBox.Text;

            if (name != "" && address != "" && telephone != "" && lastName != "")
            {
                if (ConnectClass.clientProxy.EditEfficinencyManager(loginUsername, name, lastName, address, telephone))
                {
                    MessageBox.Show("EfficiencyManager succesfully edited.", "Successfull", MessageBoxButton.OK, MessageBoxImage.Information);
                    EfficiencyManager manager = ConnectClass.clientProxy.GetEfficinencyManagerByUsername(loginUsername);
                    profileTextBox.Text = manager.ToString();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Can not edit Efficiency Manager", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Please fill all fields.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
