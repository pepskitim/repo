﻿using CommonLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for NewEfficiencyManagerWindow.xaml
    /// </summary>
    public partial class NewManagerWindow : Window
    {
        private DataGrid managersDataGrid;

        public NewManagerWindow(TypeOfManager manager, DataGrid dg)
        {       
            InitializeComponent();
            managersDataGrid = dg;
            this.managerType.Text = manager.ToString();
        }

        private void addManagerbutton_Click_1(object sender, RoutedEventArgs e)
        {
            string firstName = firstNameTextBox.Text;
            string lastName = lastNameTextBox.Text;
            string address = addressTextBox.Text;
            string telephone = telephoneNumberTextBox.Text;
            string username = usernameTextBox.Text;
            string password = passwordTextBox.Password;
            if (firstName != string.Empty && lastName != string.Empty && address != string.Empty && telephone != string.Empty && username != string.Empty && password != string.Empty)
            {
                switch ((TypeOfManager)Enum.Parse(typeof(TypeOfManager), managerType.Text))
                {
                    case TypeOfManager.EfficiencyManager:
                        if (ConnectClass.clientProxy.AddNewEfficiencyManager(username, password, firstName, lastName, address, telephone))
                        {
                            MessageBox.Show("New Efficiency Manager successfully added.", "Successfull", MessageBoxButton.OK, MessageBoxImage.Information);
                            managersDataGrid.ItemsSource = ConnectClass.clientProxy.GetEfficiencyManagers();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Can not add new Efficiency Manager. Efficiency Manager with username " + username + 
                                " already exists or internal error on serfer.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        break;

                    case TypeOfManager.FitnessManager:
                        if (ConnectClass.clientProxy.AddNewFitnessManager(username, password, firstName, lastName, address, telephone))
                        {
                            MessageBox.Show("New Fitness Manager successfully added", "Successfull", MessageBoxButton.OK, MessageBoxImage.Information);
                            managersDataGrid.ItemsSource = ConnectClass.clientProxy.GetFitnessCenterManagers();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Can not add new Fitness Manager. Fitness Manager with username " + username +
                                " already exists or internal error on serfer.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        break;
                    default:
                        break;
                }

            }
            else
            {
                MessageBox.Show("Please fill all fields with correct data.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
