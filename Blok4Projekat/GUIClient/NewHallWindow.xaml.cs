﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CommonModulLib.DataModel;

namespace GUIClient
{
    /// <summary>
    /// Interaction logic for NewHallWindow.xaml
    /// </summary>
    public partial class NewHallWindow : Window
    {
        public NewHallWindow()
        {
            InitializeComponent();
            fcComboBox.ItemsSource = ConnectClass.clientProxy.GetFitnessCenterNames();
            typeComboBox.ItemsSource = Enum.GetValues(typeof(TypeOfHall));
        }

        private void AddNewHall(object sender, RoutedEventArgs e)
        {
            string id = idTextBox.Text;
            if (id != "" && fcComboBox.SelectedItem != null && typeComboBox.SelectedItem != null)
            {
                if (ConnectClass.clientProxy.AddNewHall(id, (TypeOfHall)typeComboBox.SelectedItem, fcComboBox.SelectedItem.ToString()))
                {
                    MessageBox.Show("New Hall successfully added.", "Successfull", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Can not add new Hall. Hall with ID " + id +
                                " already exists, selected Fitness Center reached Hall number limit or" +
                                "internal error on serfer.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Please fill all fields.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
