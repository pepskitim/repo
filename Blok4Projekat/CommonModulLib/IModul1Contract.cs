﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib
{
    [ServiceContract]
    public interface IModul1Contract
    {
        [OperationContract]
        List<Hall> GetAllHalls(string fitnessCenterName); // pokupi sve hale jer mi trebaju za prikaz u menadzeru fc
        [OperationContract]
        List<FitnessCenter> GetAllFitnessCenters(); 
        [OperationContract]
        bool EditPersonalFCMdata(FitnessCenterManager fcMenager); // za slanje editovanog objekta jer je snimljen u vasoj bazi
        [OperationContract]
        List<string> GetAllFitnessCenterByManagerName(string coachUsername);
        [OperationContract]
        List<string> GetAllHallIds(string fitnessCenterName);
        [OperationContract]
        bool LoginFitnessCenterManager(string username, string password);
        [OperationContract]
        FitnessCenterManager GetFitnessCenterManagerByUserName(string username);

        [OperationContract]
        bool ConnectToModul1(string fitnessCenterName, string modul2IpAddress);
    }
}
