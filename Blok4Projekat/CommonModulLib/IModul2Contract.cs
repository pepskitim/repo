﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib
{
    [ServiceContract]
    public interface IModul2Contract
    {
        [OperationContract]
        List<Coach> GetCoachesByFitnessCenterName(string fitnessCenterName);

        [OperationContract]
        List<Term> GetTermsByFitnessCenterName(string fitnessCenterName);

        [OperationContract]
        List<string> GetAllCoachUsernames();

        [OperationContract]
        List<Term> GetTermsByCocachUsername(string coachUsername);

        [OperationContract]
        List<Coach> GetCoachesWaitForJob();

        [OperationContract]
        bool ApprovalCoachJob(Coach coach, bool approve);

        [OperationContract]
        string GetCanceledTrainings();

        [OperationContract]
        string GetCoachesActivity();

        [OperationContract]
        string GetFitnessCentersProfits();

        [OperationContract]
        string GetTrainingPackagePopularity();

        [OperationContract]
        bool CanDeleteHall(string hallId);
    }
}
