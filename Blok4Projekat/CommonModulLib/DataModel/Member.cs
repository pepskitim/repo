﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib.DataModel
{
    [DataContract]
    public class Member : User
    {
        [DataMember]
        public double Height { get; set; }
        [DataMember]
        public double Weight { get; set; }
        [DataMember]
        public DateTime DateOfBirth { get; set; }
        public Member() : base() { }
        public Member(string username, string password, string firstName, string lastName, string address, double height, double weight, DateTime dateOfBirth) :
            base(username, password, firstName, lastName, address)
        {
            this.Height = height;
            this.Weight = weight;
            this.DateOfBirth = dateOfBirth;
        }
    }
}
