﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib.DataModel
{
    [DataContract]
    public class TrainingPackage
    {
        [DataMember]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [DataMember]
        public List<string> Terms { get; set; }
        [DataMember]
        public double MonthPrice { get; set; }
        [DataMember]
        public string PackageName { get; set; }
        public TrainingPackage() { }
        public TrainingPackage(List<string> terms, double monthPrice, string packageName)
        {
            this.Terms = terms;
            this.MonthPrice = monthPrice;
            this.PackageName = packageName;
        }

        public override string ToString()
        {
            return this.PackageName + "(Price: " + this.MonthPrice.ToString() + ")";
        }
    }
}
