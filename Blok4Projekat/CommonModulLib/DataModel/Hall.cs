﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib.DataModel
{
    [DataContract]
    public class Hall
    {
        [Key]
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public TypeOfHall Type { get; set; }

        [DataMember]
        public string FitnessCenterName { get; set; }

        public Hall() { }
        public Hall(string id, TypeOfHall type, string fitnessCenterName)
        {
            this.Id = id;
            this.Type = type;
            this.FitnessCenterName = fitnessCenterName;
            
        }
    }

    public enum TypeOfHall
    {
        FITNESS = 0,
        GYM = 1,
        YOGA = 2,
        NONE = 3
    }
}
