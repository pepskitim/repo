﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib.DataModel
{
    [DataContract]
    public class FitnessCenterManager : User
    {
        [DataMember]
        public string Telephone { get; set; }
        public FitnessCenterManager(string username, string password, string firstName, string lastName, string address, string telephone) :
            base(username, password, firstName, lastName, address)
        {
            this.Telephone = telephone;
        }

        public FitnessCenterManager() : base() { }
    }
}
