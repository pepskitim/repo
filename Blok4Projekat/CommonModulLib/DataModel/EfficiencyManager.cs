﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib.DataModel
{
    [DataContract]
    public class EfficiencyManager : User
    {
        [DataMember]
        public string Telephone { get; set; }
        public EfficiencyManager(string username, string password, string firstName, string lastName, string address, string telephone) :
            base(username, password, firstName, lastName, address)
        {
            this.Telephone = telephone;
        }
        public EfficiencyManager() : base() { }

        public override string ToString()
        {
            string retVal = "";
            retVal += "Username: " + base.Username + "\n";
            retVal += "First name: " + base.FirstName + " \n";
            retVal += "Last name: " + base.LastName + " \n";
            retVal += "Address: " + base.Address + " \n";
            retVal += "Telephone: " + Telephone + " \n";
            return retVal;
        }
    }
}
