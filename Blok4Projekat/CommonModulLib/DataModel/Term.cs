﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib.DataModel
{
    [DataContract]
    public class Term
    {
        [DataMember]
        [Key]
        public string Title { get; set; }
        [DataMember]
        public string Day { get; set; }
        [DataMember]
        public Time TimeRange { get; set; }
        [DataMember]
        public string ApointedHall { get; set; }
        [DataMember]
        public TrainingLevel TrainingLvl { get; set; }
        [DataMember]
        public string ApointedCoach { get; set; }
        [DataMember]
        public double Price { get; set; }

        public Term() { }

        public Term(string title, string day, Time timeRange, string apointedHall, TrainingLevel trainingLvl, string apointedCoach, double price)
        {
            this.Title = title;
            this.Day = day;
            this.TimeRange = timeRange;
            this.ApointedHall = apointedHall;
            this.TrainingLvl = trainingLvl;
            this.ApointedCoach = apointedCoach;
            this.Price = price;
        }

        public enum TrainingLevel
        {
            BEGGINER = 0,
            INTERMEDIATE = 1,
            ADVANCED = 2,
            NONE = 3
        }

        //public override string ToString()
        //{
        //    return this.Title + " Day: " + this.Day + " " + this.TimeRange + " Hall: " + this.ApointedHall + " Level: " + this.TrainingLvl + " Coach: " + this.ApointedCoach + " Price: " + this.Price.ToString();
        //}
    }
}
