﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib.DataModel
{
    [DataContract]
    public class Time
    {
        [DataMember]
        public int StartingHour { get; set; }
        [DataMember]
        public int StartingMinute { get; set; }
        [DataMember]
        public int EndingHour { get; set; }
        [DataMember]
        public int EndingMinute { get; set; }
        public Time() { }
        public Time(int startingHour, int startingMinute, int endingHour, int endingMinute)
        {
            this.StartingHour = startingHour;
            this.StartingMinute = startingMinute;
            this.EndingHour = endingHour;
            this.EndingMinute = endingMinute;
        }

        public override string ToString()
        {
            return this.StartingHour.ToString() + ":" + this.StartingMinute.ToString() + " - " + this.EndingHour.ToString() + ":" + this.EndingMinute.ToString();
        }
    }
}
