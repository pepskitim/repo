﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib.DataModel
{
    [DataContract]
    public abstract class User
    {
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        [Key]
        public string Username { get; set; }
        //[IgnoreDataMember]
        [DataMember]
        public string Password { get; set; }

        public User(string username, string password, string firstName, string lastName, string address)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address = address;
            this.Username = username;
            this.Password = password;
        }

        public User() { }
    }
}
