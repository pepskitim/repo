﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib.DataModel
{
    public class FitnessCenterNameAndServiceIp
    {
        [DataMember]
        [Key]
        public string FitnessCenterName { get; set; }

        [DataMember]
        public string ServiceIp { get; set; }

        public FitnessCenterNameAndServiceIp() { }

        public FitnessCenterNameAndServiceIp(string fitnessCenterName, string serviceIp)
        {
            this.FitnessCenterName = fitnessCenterName;
            this.ServiceIp = serviceIp;
        }
    }
}
