﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib.DataModel
{
    [DataContract]
    public class FitnessCenter
    {
        [DataMember]
        [Key]
        public string Name { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Telephone { get; set; }
        [DataMember]
        public int NumOfHalls { get; set; }
        [DataMember]
        public string FcManagerUsername { get; set; }
        [DataMember]
        public Time WorkingTime { get; set; }

        public FitnessCenter(string name, string address, string telephone, int numOfHalls, string fcManagerUsername, Time workingTime)
        {
            this.Name = name;
            this.Address = address;
            this.Telephone = telephone;
            this.NumOfHalls = numOfHalls;
            this.FcManagerUsername = fcManagerUsername;
            this.WorkingTime = workingTime;
        }

        public FitnessCenter() { }
    }
}
