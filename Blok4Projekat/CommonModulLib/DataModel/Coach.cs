﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonModulLib.DataModel
{
    [DataContract]
    public class Coach : User
    {
        [DataMember]
        public string UniversityDegree { get; set; }
        [DataMember]
        public DateTime DateOfBirth { get; set; }
        [DataMember]
        public bool Employed { get; set; }
        [DataMember]
        public string FitnessCenter { get; set; }

        public Coach() : base() { }
        public Coach(string username, string password, string name, string lastName, string address, string universityDegree, DateTime dateOfBirth, string fitnessCenter) :
            base(username, password, name, lastName, address)
        {

            this.DateOfBirth = dateOfBirth;
            this.UniversityDegree = universityDegree;
            this.FitnessCenter = fitnessCenter;
        }

        public override string ToString()
        {
            return this.Username;
        }
    }
}
