﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibM2
{
    public class Coach : Person
    {
        public string UniversityDegree { get; set; }

        public Coach(string name, string lastName, string address, DateTime dateOfBirth, string universityDegree)
        {
            this.Name = name;
            this.LastName = lastName;
            this.Address = address;
            this.DateOfBirth = dateOfBirth;
            this.UniversityDegree = universityDegree;
        }
    }
}
