﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibM2
{
    public class Member : Person
    {
        public double Height { get; set; }
        public double Weight { get; set; }
        public Member(string name, string lastName, string address, DateTime dateOfBirth, double heigth, double weight)
        {
            this.Name = name;
            this.LastName = lastName;
            this.Address = address;
            this.DateOfBirth = dateOfBirth;
            this.Weight = weight;
            this.Height = heigth;
        }
    }
}
