﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibM2
{
    [ServiceContract]
    public interface ILogin
    {
        [OperationContract]
        AuthenticationAndAuthorization Login(string username, string password);



    }
    public enum AuthenticationAndAuthorization
    {
        FITNESS_CENTER_MANAGER = 0,
        COACH = 1,
        MEMBER = 2,
        NONE = 3
    }
}
