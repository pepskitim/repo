﻿using CommonModulLib;
using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibM2
{
    [ServiceContract]
    public interface IdbContract : IModul1Contract
    {
        [OperationContract]
        AuthenticationAndAuthorization Login(string username, string password);
        [OperationContract]
        bool AddNewCoach(string username, string password, string firstName, string lastName, string address, string universityDegree, DateTime dateOfBirth, string fitnessCenter);
        [OperationContract]
        bool DeleteCoach(string username);
        [OperationContract]
        bool UpdateCoach(Coach coach);
        [OperationContract]
        Coach GetCoach(string username);
        [OperationContract]
        List<Coach> GetAllCoaches();
        [OperationContract]
        List<Term> GetTermsByCocachUsername(string coachUsername);

        [OperationContract]
        bool AddNewTrainingPackage(List<string> termTitles, double monthPrice, string packageName);

        [OperationContract]
        bool AddNewMember(string username, string password, string firstName, string lastName, string address, double height, double weight, DateTime dateOfBirth);
     
        [OperationContract]
        bool DeleteMember(string username);
        [OperationContract]
        bool UpdateMember(Member member);
        [OperationContract]
        Member GetMember(string username);

        [OperationContract]
        bool AddNewTerm(string name, string day, int startingHour, int startingMinute, int endingHour, int endingMinute, string hall, Term.TrainingLevel trainingLevel, string coach, double price);
        [OperationContract]
        List<Term> GetAllTerms();
        [OperationContract]
        bool UpdateTerm(Term term);
        [OperationContract]
        bool DeleteTerm(string title);
        [OperationContract]
        Term GetTerm(string title);

        [OperationContract]
        bool DeletePackage(int id);
        [OperationContract]
        bool UpdatePackage(TrainingPackage package);

        [OperationContract]
        List<TrainingPackage> GetAllTraininingPackages();
        [OperationContract]
        List<string> GetAllPackageTerms(string packageName);

        [OperationContract]
        List<Term> GetTermsByManagerUsername(string managerUserName);
        [OperationContract]
        List<string> GetNotificationsForMember(string memberUsername);

        [OperationContract]
        List<TrainingPackage> GetAllTraininingPackagesForManagerUsername(string managerUsername);

        #region MemberPackageRelationshipManager
        [OperationContract]
        List<MemberPackageRel> GetAllMPRels();
        [OperationContract]
        List<Member> GetMembersForGivenPackage(string packageName);
        [OperationContract]
        List<TrainingPackage> GetPackagesForGivenMember(string memberName);
        [OperationContract]
        bool SetMPRel(MemberPackageRel mprel);
        [OperationContract]
        bool PayMPRel(string memberName, string packageName);
        [OperationContract]
        bool DeleteMPRel(MemberPackageRel mprel);
        #endregion

        #region MemberTermRelationshipManager
        [OperationContract]
        List<MemberTermRel> GetAllMTRels();
        [OperationContract]
        List<Member> GetMembersForGivenTerm(string termName);
        [OperationContract]
        List<Term> GetTermsForGivenMember(string memberName);
        [OperationContract]
        bool SetMTRel(MemberTermRel mtrel);
        [OperationContract]
        bool PayMTRel(string memberName, string termName);
        [OperationContract]
        bool DeleteMTRel(MemberTermRel mtrel);
        #endregion

        #region CanceledTraining

        [OperationContract]
        bool SetCanceledTraining(CanceledTraining ctObject);

        #endregion

        #region NotificationsForMembers
        [OperationContract]
        bool NotifyMembersForTermChange(string termName, string notification);
        [OperationContract]
        bool NotifyMembersForPackageChange(string packageName, string notification);
        #endregion
    }

    public enum AuthenticationAndAuthorization
    {
        FITNESS_CENTER_MANAGER = 0,
        COACH = 1,
        MEMBER = 2,
        NONE = 3
    }
}
