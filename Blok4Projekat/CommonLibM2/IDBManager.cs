﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibM2
{
    public interface IDBManager
    {
        #region CoachManager
        [OperationContract]
        Coach GetCoach(string username);
        [OperationContract]
        bool SetCoach(string username, string password, string firstName, string lastName, string address, string universityDegree, DateTime dateOfBirth, string fitnessCenter);
        [OperationContract]
        bool UpdateCoach(Coach coach);
        [OperationContract]
        bool DeleteCoach(string username);
        [OperationContract]
        List<Coach> GetAllCoaches();
        #endregion

        #region MemberManager
        [OperationContract]
        Member GetMember(string username);
        [OperationContract]
        bool SetMember(string username, string password, string firstName, string lastName, string address, double height, double weight, DateTime dateOfBirth);
        [OperationContract]
        bool UpdateMember(Member member);
        [OperationContract]
        bool DeleteMember(string username);
        #endregion

        #region Term
        [OperationContract]
        Term GetTerm(string username);
        [OperationContract]
        bool SetTerm(string name, string day, int startingHour, int startingMinute, int endingHour, int endingMinute, string hall, Term.TrainingLevel trainingLevel, string coach, double price);
        [OperationContract]
        bool UpdateTerm(Term term);
        [OperationContract]
        bool DeleteTerm(string title);
        [OperationContract]
        List<Term> GetAllTerms();
        [OperationContract]
        List<Term> GetTermsByCocachUsername(string coachUsername);
        #endregion

        #region TrainingPackage
        [OperationContract]
        bool SetTrainingPackage(List<string> termTitles, double monthPrice, string packageName);
        [OperationContract]
        TrainingPackage GetTrainingPackage(int id);
        [OperationContract]
        bool UpdateTrainingPackage(TrainingPackage trainingPackage);
        [OperationContract]
        bool DeleteTrainingPackage(int id);
        [OperationContract]
        List<TrainingPackage> GetAllTPackages();
        [OperationContract]
        List<string> GetAllPackageTerms(string packageName);
        #endregion

        #region MemberPackageRelationshipManager
        [OperationContract]
        List<MemberPackageRel> GetAllMPRels();
        [OperationContract]
        List<Member> GetMembersForGivenPackage(string packageName);
        [OperationContract]
        List<TrainingPackage> GetPackagesForGivenMember(string memberName);
        [OperationContract]
        bool SetMPRel(MemberPackageRel mprel);
        [OperationContract]
        bool PayMPRel(string memberName, string packageName);
        [OperationContract]
        bool DeleteMPRel(MemberPackageRel mprel);
        #endregion

        #region MemberTermRelationshipManager
        [OperationContract]
        List<MemberTermRel> GetAllMTRels();
        [OperationContract]
        List<Member> GetMembersForGivenTerm(string termName);
        [OperationContract]
        List<Term> GetTermsForGivenMember(string memberName);
        [OperationContract]
        bool SetMTRel(MemberTermRel mtrel);
        [OperationContract]
        bool PayMTRel(string memberName, string termName);
        [OperationContract]
        bool DeleteMTRel(MemberTermRel mtrel);
        #endregion

        #region CanceledTraining
        
        [OperationContract]
        bool SetCanceledTraining(CanceledTraining ctObject);

        #endregion

        #region Notifications
        [OperationContract]
        bool NotifyMembersForTermChange(string termName, string notification);
        [OperationContract]
        bool NotifyMembersForPackageChange(string packageName, string notification);
        #endregion

        [OperationContract]
        AuthenticationAndAuthorization Login(string username, string password);

        [OperationContract]
        List<Term> GetTermsByManagerUsername(string managerUserName);
        [OperationContract]
        List<string> GetNotificationsForMember(string memberUsername);

        List<TrainingPackage> GetAllTraininingPackagesForManagerUsername(string managerUsername);

        FitnessCenterNameAndServiceIp GetFitnessCenterNameAndModul1Ip();
        void AddFitnessCenterNameAndModul1Ip(string fcName, string modul1IpAddress);
    }
}
