﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIclientM2
{
    /// <summary>
    /// Interaction logic for CreatePackageWindow.xaml
    /// </summary>
    public partial class CreatePackageWindow : Window
    {
        private ListBox myListBox = new ListBox();
        public CreatePackageWindow(ListBox listBox)
        {
            InitializeComponent();
            myListBox = listBox;
            //dataGridTerms.ItemsSource = ConnectClassM2.clientProxy.GetTermsByManagerUsername(LoginWindow.Username); //posle izmjene modela dirao
            dataGridTerms.ItemsSource = ConnectClassM2.clientProxy.GetAllTerms();
        }

        private void buttonCreatePackage_Click(object sender, RoutedEventArgs e)
        {
            string packagePrice = textBoxPrice.Text;
            string packageTitle = textBoxTitle.Text;

            if (packagePrice.Equals(string.Empty) || packageTitle.Equals(string.Empty) || dataGridTerms.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please fill the price box and select terms.");
            }
            else
            {
                double parsedValue;
                if (!double.TryParse(packagePrice, out parsedValue))
                {
                    MessageBox.Show("This is a number only field");
                    return;
                }
                  
                var selectedTerms = dataGridTerms.SelectedItems.Cast<Term>().ToList();
                List<string> termTitles = new List<string>();

                foreach (var item in selectedTerms)
                {
                    termTitles.Add(item.Title);
                }

                if (ConnectClassM2.clientProxy.AddNewTrainingPackage(termTitles, parsedValue, packageTitle))
                {
                    myListBox.ItemsSource = ConnectClassM2.clientProxy.GetAllTraininingPackages();
                    myListBox.Items.Refresh();
                    MessageBox.Show("Package sucessfuly created.");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Package was not created!");
                }

            }
           
        }
    }
}
