﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIclientM2
{
    /// <summary>
    /// Interaction logic for EditPackageWindow.xaml
    /// </summary>
    public partial class EditPackageWindow : Window
    {
        private TrainingPackage trainingPackage;
        private string myUsername = string.Empty;
        private DataGrid dataGridFromMainWindow = new DataGrid();
        private ListBox listBoxFromMainWindow = new ListBox();

        public EditPackageWindow(TrainingPackage package, DataGrid dataGrid, ListBox listBox, string username)
        {
            InitializeComponent();
            trainingPackage = package;
            myUsername = username;
            listBoxFromMainWindow = listBox;
            dataGridFromMainWindow = dataGrid;
            textBoxPrice.Text = package.MonthPrice.ToString();
            textBoxTitle.Text = package.PackageName;
            dataGridTerms.ItemsSource = ConnectClassM2.clientProxy.GetAllTerms();  
        }

        private void buttonEditPackage_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridTerms.SelectedItems.Count != 0)
            {
                double parsedValue;
                if (!double.TryParse(textBoxPrice.Text, out parsedValue))
                {
                    MessageBox.Show("Price is a number only field");
                    return;
                }

                var selectedTerms = dataGridTerms.SelectedItems.Cast<Term>().ToList();
                List<string> termTitles = new List<string>();

                foreach (var item in selectedTerms)
                {
                    termTitles.Add(item.Title);
                }
                    
                trainingPackage.Terms = termTitles;
                trainingPackage.MonthPrice = parsedValue;
                trainingPackage.PackageName = textBoxTitle.Text;

                if (ConnectClassM2.clientProxy.UpdatePackage(trainingPackage))
                {
                    dataGridFromMainWindow.ItemsSource = null;
                    dataGridFromMainWindow.Items.Refresh();                
                    listBoxFromMainWindow.ItemsSource = ConnectClassM2.clientProxy.GetAllTraininingPackagesForManagerUsername(LoginWindow.Username);
                    listBoxFromMainWindow.SelectedItem = null;
                    this.Close();
                    MessageBox.Show("Succesfuly edited training package.");
                    string notification = string.Empty;
                    notification = "Package " + trainingPackage.PackageName + " has been updated to values: Price " + trainingPackage.MonthPrice;

                    if (!ConnectClassM2.clientProxy.NotifyMembersForPackageChange(trainingPackage.PackageName, notification))
                    {
                        MessageBox.Show("Failed to push notification for training package change to database.");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Failed to edit training package.");
                }
            }
            else
            {
                MessageBox.Show("Please select terms.");
            }         
        }
    }
}
