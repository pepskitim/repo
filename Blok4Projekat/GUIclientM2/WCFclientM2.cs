﻿using CommonLibM2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using CommonModulLib.DataModel;

namespace GUIclientM2
{
   
    public class WCFclientM2 : ChannelFactory<IdbContract>, IdbContract, IDisposable
    {
        public IdbContract factory;

        public WCFclientM2() { }

        public WCFclientM2(NetTcpBinding binding, EndpointAddress address)
            : base(binding, address)
        {
            factory = this.CreateChannel();
        }

        public bool AddNewCoach(string username, string password, string firstName, string lastName, string address, string universityDegree, DateTime dateOfBirth, string fitnessCenter) 
        {
            return factory.AddNewCoach(username, password, firstName, lastName, address, universityDegree, dateOfBirth, fitnessCenter); 
        }
       
        public bool AddNewMember(string username, string password, string firstName, string lastName, string address, double height, double weight, DateTime dateOfBirth)
        {
            return factory.AddNewMember(username, password, firstName, lastName, address, height, weight, dateOfBirth);
        }

        public bool AddNewTerm(string name, string day, int startingHour, int startingMinute, int endingHour, int endingMinute, string hall, Term.TrainingLevel trainingLevel, string coach, double price)
        {
            return factory.AddNewTerm(name, day, startingHour, startingMinute, endingHour, endingMinute, hall, trainingLevel, coach, price);
        }

        public bool AddNewTrainingPackage(List<string> termTitles, double monthPrice, string packageName)
        {
            return factory.AddNewTrainingPackage(termTitles, monthPrice, packageName);
        }

        public bool DeleteCoach(string username)
        {
            return factory.DeleteCoach(username);
        }
   
        public bool DeleteMember(string username)
        {
            return factory.DeleteMember(username);
        }

        public bool DeleteTerm(string title)
        {
            return factory.DeleteTerm(title);
        }

        public bool EditPersonalFCMdata(FitnessCenterManager fcMenager)
        {
            return factory.EditPersonalFCMdata(fcMenager);
        }

        public List<Coach> GetAllCoaches()
        {
            return factory.GetAllCoaches();
        }

        public List<string> GetAllFitnessCenterByManagerName(string fcUsername)
        {
            return factory.GetAllFitnessCenterByManagerName(fcUsername);
        }

        public List<FitnessCenter> GetAllFitnessCenters()
        {
            return factory.GetAllFitnessCenters();
        }

        public List<string> GetAllHallIds(string fitnessCenterName)
        {
            return factory.GetAllHallIds(fitnessCenterName);
        }

        public List<Hall> GetAllHalls(string fitnessCenterName)
        {
            return factory.GetAllHalls(fitnessCenterName);
        }

        public List<string> GetAllPackageTerms(string packageName)
        {
            return factory.GetAllPackageTerms(packageName);
        }

        public List<Term> GetAllTerms()
        {
            return factory.GetAllTerms();
        }

        public List<TrainingPackage> GetAllTraininingPackages()
        {
            return factory.GetAllTraininingPackages();
        }

        public Coach GetCoach(string username)
        {
            return factory.GetCoach(username);
        }

        public Member GetMember(string username)
        {
            return factory.GetMember(username);
        }

        public List<Term> GetTermsByCocachUsername(string coachUsername)
        {
            return factory.GetTermsByCocachUsername(coachUsername);
        }

        //public Member GetMember(string username)
        //{
        //    return factory.GetMember(username);
        //}
        public AuthenticationAndAuthorization Login(string username, string password)
        {
            return factory.Login(username, password);
        }

        public bool LoginFitnessCenterManager(string username, string password)
        {
            return factory.LoginFitnessCenterManager(username, password);
        }

        public bool UpdateCoach(Coach coach)
        {
            return factory.UpdateCoach(coach);
        }

        public bool UpdateMember(Member member)
        {
            return factory.UpdateMember(member);
        }

        public bool UpdateTerm(Term term)
        {
            return factory.UpdateTerm(term);
        }

        public FitnessCenterManager GetFitnessCenterManagerByUserName(string username)
        {
            return factory.GetFitnessCenterManagerByUserName(username);
        }

        #region MemberPackageRelationshipManager
        public List<MemberPackageRel> GetAllMPRels()
        {
            //throw new NotImplementedException();
            return factory.GetAllMPRels();
        }

        public List<Member> GetMembersForGivenPackage(string packageName)
        {
            return factory.GetMembersForGivenPackage(packageName);
        }

        public List<TrainingPackage> GetPackagesForGivenMember(string memberName)
        {
            throw new NotImplementedException();
        }

        public bool SetMPRel(MemberPackageRel mprel)
        {
            return factory.SetMPRel(mprel);
        }

        public bool DeleteMPRel(MemberPackageRel mprel)
        {
            return factory.DeleteMPRel(mprel);
        }
        #endregion

        #region MemberTermRelationshipManager
        public List<MemberTermRel> GetAllMTRels()
        {
            return factory.GetAllMTRels();
        }

        public List<Member> GetMembersForGivenTerm(string termName)
        {
            return factory.GetMembersForGivenTerm(termName);
        }

        public List<Term> GetTermsForGivenMember(string memberName)
        {
            return factory.GetTermsForGivenMember(memberName);
        }

        public bool SetMTRel(MemberTermRel mtrel)
        {
            return factory.SetMTRel(mtrel);
        }

        public bool DeleteMTRel(MemberTermRel mtrel)
        {
            return factory.DeleteMTRel(mtrel);
        }

        public bool DeletePackage(int id)
        {
            return factory.DeletePackage(id);
        }

        public bool PayMPRel(string memberName, string packageName)
        {
            return factory.PayMPRel(memberName, packageName);
        }

        public bool PayMTRel(string memberName, string termName)
        {
            return factory.PayMTRel(memberName, termName);
        }

        #endregion

        public bool SetCanceledTraining(CanceledTraining ctObject)
        {
            return factory.SetCanceledTraining(ctObject);
        }

        public bool UpdatePackage(TrainingPackage package)
        {
            return factory.UpdatePackage(package);
        }

        public Term GetTerm(string title)
        {
            return factory.GetTerm(title);
        }

        public List<Term> GetTermsByManagerUsername(string managerUserName)
        {
            return factory.GetTermsByManagerUsername(managerUserName);
        }

        public List<string> GetNotificationsForMember(string memberUsername)
        {
            return factory.GetNotificationsForMember(memberUsername);
        }

        public List<TrainingPackage> GetAllTraininingPackagesForManagerUsername(string managerUsername)
        {
            return factory.GetAllTraininingPackagesForManagerUsername(managerUsername);
        }

        public bool ConnectToModul1(string fitnessCenterName, string modul2IpAddress)
        {
            throw new NotImplementedException();
        }

        public bool NotifyMembersForTermChange(string termName, string notification)
        {
            return factory.NotifyMembersForTermChange(termName, notification);
        }

        public bool NotifyMembersForPackageChange(string packageName, string notification)
        {
            return factory.NotifyMembersForPackageChange(packageName, notification);
        }
    }
}
