﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIclientM2
{
    /// <summary>
    /// Interaction logic for CoachWindow.xaml
    /// </summary>
    public partial class CoachWindow : Window
    {
        private List<Term> termList = new List<Term>();
        private string myUsername = string.Empty;

        public CoachWindow(string username)
        {
            InitializeComponent();
            myUsername = username;
            termList = ConnectClassM2.clientProxy.GetTermsByCocachUsername(LoginWindow.Username);
            dataGridTerm.ItemsSource = termList;    
        }

        private void buttonAddTerm_Click(object sender, RoutedEventArgs e)
        {
            TermCreateWindow termCreateWindow = new TermCreateWindow(dataGridTerm, LoginWindow.Username);
            termCreateWindow.Show();
        }

        private void buttonEditViewData_Click(object sender, RoutedEventArgs e)
        {
            CoachPersonalWindow coachPerWindow = new CoachPersonalWindow(LoginWindow.Username);
            coachPerWindow.Show();

            Coach coach = ConnectClassM2.clientProxy.GetCoach(LoginWindow.Username);
            coachPerWindow.textBoxName.Text = coach.FirstName;
            coachPerWindow.textBoxLastname.Text = coach.LastName;
            coachPerWindow.textBoxAddress.Text = coach.Address;
            coachPerWindow.dateTimePicker.SelectedDate = coach.DateOfBirth;
            coachPerWindow.textBoxUsername.Text = coach.Username;
            coachPerWindow.textBoxPassword.Text = coach.Password;
            coachPerWindow.textBoxDegree.Text = coach.UniversityDegree;
            coachPerWindow.textBoxFitnessCenter.Text = coach.FitnessCenter;
        }

        private void buttonEditTerm_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridTerm.SelectedItems.Count != 1)
            {
                MessageBox.Show("Please select one term you would like to edit.");
            }
            else
            {
                Term selectedTerm = (Term)dataGridTerm.SelectedItem;
                TermEditWindow tEditWindow = new TermEditWindow(selectedTerm, LoginWindow.Username, dataGridTerm);
                tEditWindow.Show();
            }

        }

        private void buttonDelTerm_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridTerm.SelectedItems.Count != 1)
            {
                MessageBox.Show("Please select one term you would like to delete.");
                return;
            }

            Term selectedTerm = (Term)dataGridTerm.SelectedItem;

            if (ConnectClassM2.clientProxy.GetMembersForGivenTerm(selectedTerm.Title).Count > 0)
            {
                MessageBox.Show("Unable to delete term because there are members attacthed to it.");
                return;
            }

            if (ConnectClassM2.clientProxy.DeleteTerm(selectedTerm.Title))
            {
                dataGridTerm.ItemsSource = ConnectClassM2.clientProxy.GetTermsByCocachUsername(myUsername);
                dataGridTerm.Items.Refresh();
                MessageBox.Show("Succesfuly deleted package.");
            }
            else
            {
                MessageBox.Show("Failed to delete package.");
            }

        }

        private void Logout(object sender, RoutedEventArgs e)
        {
            LoginWindow l = new LoginWindow();
            l.Show();
            this.Close();
        }
    }
}
