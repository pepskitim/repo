﻿using CommonLibM2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CommonModulLib.DataModel;

namespace GUIclientM2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<TrainingPackage> trainPackagesList = new List<TrainingPackage>();
        private List<Term> termList = ConnectClassM2.clientProxy.GetAllTerms();
        private string myUsername = string.Empty;

        public MainWindow(string username)
        {
            InitializeComponent();
            myUsername = username;
            listBoxPackages.ItemsSource = ConnectClassM2.clientProxy.GetAllTraininingPackagesForManagerUsername(LoginWindow.Username);
            comboBoxFitnessCenter.ItemsSource = ConnectClassM2.clientProxy.GetAllFitnessCenterByManagerName(LoginWindow.Username);
        }

        #region radioButtons
        private void radioButtonCoach_Checked(object sender, RoutedEventArgs e)
        {
            textBoxDegree.Visibility = Visibility.Visible;
            label7.Visibility = Visibility.Visible;
            //label55.Visibility = Visibility.Visible;
            //comboBoxFitnessCenter.Visibility = Visibility.Visible;
            textBoxHeight.Visibility = Visibility.Hidden;
            textBoxWeight.Visibility = Visibility.Hidden;
            label8.Visibility = Visibility.Hidden;
            label9.Visibility = Visibility.Hidden;
        }

        private void radioButtonMember_Checked(object sender, RoutedEventArgs e)
        {
            //label55.Visibility = Visibility.Hidden;
            //comboBoxFitnessCenter.Visibility = Visibility.Hidden;
            textBoxDegree.Visibility = Visibility.Hidden;
            label7.Visibility = Visibility.Hidden;
            label8.Visibility = Visibility.Visible;
            label9.Visibility = Visibility.Visible;
            textBoxHeight.Visibility = Visibility.Visible;
            textBoxWeight.Visibility = Visibility.Visible;
        }
        #endregion 

        private void buttonCreateAcc_Click(object sender, RoutedEventArgs e)
        {
            if (radioButtonCoach.IsChecked.Value)
            {
                if (textBoxName.Text.Equals("") || textBoxLastname.Text.Equals("") || !dateTimePicker.SelectedDate.HasValue || textBoxAddress.Text.Equals(""))
                {
                    MessageBox.Show("Please fill the required fields!");
                    ValidationHelper(true);
                }
                else
                {
                    string fitnessCenter = comboBoxFitnessCenter.SelectedItem.ToString();

                    if (ConnectClassM2.clientProxy.AddNewCoach(textBoxUsername.Text, textBoxPassword.Text, textBoxName.Text, textBoxLastname.Text, textBoxAddress.Text, textBoxDegree.Text, dateTimePicker.SelectedDate.Value, fitnessCenter))
                    {
                        ValidationHelper(false);
                        MessageBox.Show("Coach account sucessfuly created.");
                    }
                    else
                    {
                        MessageBox.Show("Coach account was not created.");
                    }
                }
            }
            else
            {
                if (textBoxName.Text.Equals("") || textBoxLastname.Text.Equals("") || !dateTimePicker.SelectedDate.HasValue || textBoxAddress.Text.Equals(""))
                {
                    MessageBox.Show("Please fill the required fields!");
                    ValidationHelper(true);
                }
                else
                {
                    if (ConnectClassM2.clientProxy.AddNewMember(textBoxUsername.Text, textBoxPassword.Text, textBoxName.Text, textBoxLastname.Text, textBoxAddress.Text, Convert.ToDouble(textBoxHeight.Text), Convert.ToDouble(textBoxWeight.Text), dateTimePicker.SelectedDate.Value))
                    {
                        ValidationHelper(false);
                        MessageBox.Show("Member account sucessfuly added.");
                    }
                    else
                    {
                        MessageBox.Show("Member account was not added.");
                    }
                }
            }
        }

        private void buttonAddPackage_Click(object sender, RoutedEventArgs e)
        {
            CreatePackageWindow createPackWindow = new CreatePackageWindow(listBoxPackages);
            createPackWindow.Show();
        }

        private void buttonEditViewData_Click(object sender, RoutedEventArgs e)
        {
            FCMngPersonalWindow fcMngPersonalWindow = new FCMngPersonalWindow(myUsername);
            fcMngPersonalWindow.Show();
        }

        private void ValidationHelper(bool show)
        {
            if (show)
            {
                labelW1.Visibility = Visibility.Visible;
                labelW2.Visibility = Visibility.Visible;
                labelW3.Visibility = Visibility.Visible;
                labelW4.Visibility = Visibility.Visible;
            }
            else
            {
                labelW1.Visibility = Visibility.Hidden;
                labelW2.Visibility = Visibility.Hidden;
                labelW3.Visibility = Visibility.Hidden;
                labelW4.Visibility = Visibility.Hidden;
            }
        }

        private void listBoxPackages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBoxPackages.SelectedItem == null)
            {
                return;
            }

            var selected = (TrainingPackage)listBoxPackages.SelectedItem;
            List<string> neededTerms = ConnectClassM2.clientProxy.GetAllPackageTerms(selected.PackageName);
            List<Term> termsToShowList = new List<Term>();

            foreach (var term in termList)
            {
                foreach (var str in neededTerms)
                {
                    if (term.Title.Equals(str))
                    {
                        termsToShowList.Add(term);
                    }
                }
            }
            dataGridTerms.ItemsSource = termsToShowList;
            dataGridTerms.Items.Refresh();
        }

        private void buttonEditPackage_Click(object sender, RoutedEventArgs e)
        {
            TrainingPackage package = (TrainingPackage)listBoxPackages.SelectedItem;
            EditPackageWindow win = new EditPackageWindow(package, dataGridTerms, listBoxPackages, myUsername);
            win.Show();
        }

        private void buttonDelPackage_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxPackages.SelectedItems.Count != 1)
            {
                MessageBox.Show("Please select training package you want to delete.");
                return;
            }

            TrainingPackage package = (TrainingPackage)listBoxPackages.SelectedItem;

            if (ConnectClassM2.clientProxy.GetMembersForGivenPackage(package.PackageName).Count > 0)
            {
                MessageBox.Show("Unable to delete training package because there are members attacthed to it.");
                return;
            }

            if (ConnectClassM2.clientProxy.DeletePackage(package.Id))
            {
                listBoxPackages.ItemsSource = ConnectClassM2.clientProxy.GetAllTraininingPackages();
                listBoxPackages.Items.Refresh();
                dataGridTerms.ItemsSource = null;
                dataGridTerms.Items.Refresh();
                MessageBox.Show("Package succesfuly deleted.");
            }
            else
            {
                MessageBox.Show("Failed to delete package");
            }
        }

        private void Logout(object sender, RoutedEventArgs e)
        {
            LoginWindow l = new LoginWindow();
            l.Show();
            this.Close();
        }

        private void comboBoxFitnessCenter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dataGridHalls.ItemsSource = ConnectClassM2.clientProxy.GetAllHalls(comboBoxFitnessCenter.SelectedItem.ToString());
            dataGridHalls.Items.Refresh();
        }
    }
}
