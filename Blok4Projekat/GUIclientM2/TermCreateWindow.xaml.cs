﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIclientM2
{
    /// <summary>
    /// Interaction logic for TermCreateWindow.xaml
    /// </summary>
    public partial class TermCreateWindow : Window
    {
        private DataGrid myDataGrid = new DataGrid();
        private string myUsername = string.Empty;

        public TermCreateWindow(DataGrid dataGrid, string username)
        {
            InitializeComponent();
            myDataGrid = dataGrid;
            myUsername = username;
            comboBoxDay.ItemsSource = new List<string>() { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" };
            comboBoxTrainLvl.ItemsSource = Enum.GetValues(typeof(Term.TrainingLevel)).Cast<Term.TrainingLevel>();
            Coach coach = ConnectClassM2.clientProxy.GetCoach(LoginWindow.Username);
            List<string> fintessCenterHalls = ConnectClassM2.clientProxy.GetAllHallIds(coach.FitnessCenter);
            comboBoxHall.ItemsSource = fintessCenterHalls;
        }

        private void buttonCreateTerm_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxName.Text.Equals(string.Empty) || comboBoxDay.SelectedItem == null || textBoxStartHour.Text.Equals(string.Empty)
                || textBoxStartMin.Text.Equals(string.Empty) || textBoxEndHour.Text.Equals(string.Empty) || textBoxEndMin.Text.Equals(string.Empty)
                || comboBoxTrainLvl.SelectedItem == null || textBoxPrice.Text.Equals(string.Empty) || comboBoxHall.SelectedItem == null) 
            {
                MessageBox.Show("Please fill all fields!");
            }
            else
            {
                if (Convert.ToInt32(textBoxStartHour.Text) > Convert.ToInt32(textBoxEndHour.Text))
                {
                    labelTimeFormat.Visibility = Visibility.Visible;
                }
                else
                {
                    if (ConnectClassM2.clientProxy.AddNewTerm(textBoxName.Text, comboBoxDay.SelectedItem.ToString(), Convert.ToInt32(textBoxStartHour.Text),
                         Convert.ToInt32(textBoxStartMin.Text), Convert.ToInt32(textBoxEndHour.Text), Convert.ToInt32(textBoxEndMin.Text),
                 comboBoxHall.SelectedItem.ToString(), (Term.TrainingLevel)comboBoxTrainLvl.SelectedItem, LoginWindow.Username, Convert.ToDouble(textBoxPrice.Text)))
                    {
                        MessageBox.Show("Term sucessfuly created.");
                        myDataGrid.ItemsSource = ConnectClassM2.clientProxy.GetTermsByCocachUsername(myUsername);
                        myDataGrid.Items.Refresh();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Term was not created!");
                    }
                }
            }
          
        }
    }
}
