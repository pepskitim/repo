﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIclientM2
{
    /// <summary>
    /// Interaction logic for FCMngPersonalWindow.xaml
    /// </summary>
    public partial class FCMngPersonalWindow : Window
    {
        private string myUsername = string.Empty;

        public FCMngPersonalWindow(string username)
        {
            InitializeComponent();
            myUsername = username;
            FitnessCenterManager fcManager = ConnectClassM2.clientProxy.GetFitnessCenterManagerByUserName(myUsername);

            textBoxName.Text = fcManager.FirstName;
            textBoxLastname.Text = fcManager.LastName;
            textBoxAddress.Text = fcManager.Address;
            textBoxTelephone.Text = fcManager.Telephone;
            textBoxUsername.Text = myUsername;
            textBoxPassword.Text = fcManager.Password;
        }

        private void buttonSaveChanges_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxPassword.Text.Equals(string.Empty) || textBoxName.Text.Equals(string.Empty) || textBoxLastname.Text.Equals(string.Empty)
                || textBoxAddress.Text.Equals(string.Empty) || textBoxTelephone.Text.Equals(string.Empty))
            {
                MessageBox.Show("Please fill required fields.");
            }
            else
            {
                FitnessCenterManager fcManager = new FitnessCenterManager(myUsername, textBoxPassword.Text, textBoxName.Text, textBoxLastname.Text, textBoxAddress.Text, textBoxTelephone.Text);

                if (ConnectClassM2.clientProxy.EditPersonalFCMdata(fcManager))
                {
                    MessageBox.Show("Succesfuly edited personal data.");
                }
                else
                {
                    MessageBox.Show("Failed to edit personal data.");
                }
            }
            
        }
    }
}
