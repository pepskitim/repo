﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CommonLibM2;

namespace GUIclientM2
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private static string username;
        public static string Username
        {
            get
            {
                return username;
            }
        }
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void button_ClickLogin(object sender, RoutedEventArgs e)
        {
            AuthenticationAndAuthorization userType = ConnectClassM2.clientProxy.Login(textBoxUsername.Text, textBoxPassword.Text);
            username = textBoxUsername.Text;

            switch (userType)
            {
                case AuthenticationAndAuthorization.FITNESS_CENTER_MANAGER:
                    MainWindow fcMenagerWindow = new MainWindow(LoginWindow.Username);
                    fcMenagerWindow.Show();
                    this.Close();
                    break;
                case AuthenticationAndAuthorization.COACH:
                    CoachWindow coachWindow = new CoachWindow(LoginWindow.Username);
                    coachWindow.Show();
                    this.Close();
                    break;
                case AuthenticationAndAuthorization.MEMBER:
                    MemberWindow memberWindow = new MemberWindow(LoginWindow.Username);
                    memberWindow.Show();
                    this.Close();
                    break;
                case AuthenticationAndAuthorization.NONE:
                    MessageBox.Show("User does not exist.");
                    break;
            }
          
        }
    }
}
