﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIclientM2
{
    /// <summary>
    /// Interaction logic for CoachPersonalWindow.xaml
    /// </summary>
    public partial class CoachPersonalWindow : Window
    {
        private string myUsername = string.Empty;
        public CoachPersonalWindow(string username)
        {
            InitializeComponent();
            myUsername = username;
        }

        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxName.Text.Equals("") || textBoxLastname.Text.Equals("") || !dateTimePicker.SelectedDate.HasValue || textBoxAddress.Text.Equals(""))
            {
                MessageBox.Show("Please fill the required fields!");
            }
            else
            {
                Coach myCoach = ConnectClassM2.clientProxy.GetCoach(myUsername);
                Coach coach = new Coach(textBoxUsername.Text, textBoxPassword.Text, textBoxName.Text, textBoxLastname.Text, textBoxAddress.Text,
                    textBoxDegree.Text, dateTimePicker.SelectedDate.Value, myCoach.FitnessCenter);

                if (ConnectClassM2.clientProxy.UpdateCoach(coach))
                {
                    MessageBox.Show("Coach's deteails have been updated.");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Failed to edit coach's personal data.");
                }
            }
        }
    }
}
