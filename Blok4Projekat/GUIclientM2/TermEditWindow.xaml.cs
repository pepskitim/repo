﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIclientM2
{
    /// <summary>
    /// Interaction logic for TermEditWindow.xaml
    /// </summary>
    public partial class TermEditWindow : Window
    {
        private Term termToEdit = new Term();
        private string myUsername = string.Empty;
        private DataGrid myDataGrid = new DataGrid();

        public TermEditWindow(Term term, string username, DataGrid dataGrid)
        {
            InitializeComponent();
            termToEdit = term;
            myUsername = username;
            myDataGrid = dataGrid;
            comboBoxDay.ItemsSource = new List<string>() { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" };
            comboBoxTrainLvl.ItemsSource = Enum.GetValues(typeof(Term.TrainingLevel)).Cast<Term.TrainingLevel>();

            Coach myCoach = ConnectClassM2.clientProxy.GetCoach(myUsername);
            comboBoxHall.ItemsSource = ConnectClassM2.clientProxy.GetAllHallIds(myCoach.FitnessCenter);

            textBoxTitle.Text = termToEdit.Title;
            comboBoxDay.SelectedItem = termToEdit.Day;
            textBoxStartHour.Text = termToEdit.TimeRange.StartingHour.ToString();
            textBoxStartMin.Text = termToEdit.TimeRange.StartingMinute.ToString();
            textBoxEndHour.Text = termToEdit.TimeRange.EndingHour.ToString();
            textBoxEndMin.Text = termToEdit.TimeRange.EndingMinute.ToString();
            comboBoxHall.SelectedItem = termToEdit.ApointedHall;
            textBoxPrice.Text = termToEdit.Price.ToString();
            comboBoxTrainLvl.SelectedItem = termToEdit.TrainingLvl;
        }

        private void buttonEditTerm_Click(object sender, RoutedEventArgs e)
        {            
            Time time = new Time(Convert.ToInt32(textBoxStartHour.Text), Convert.ToInt32(textBoxStartMin.Text), Convert.ToInt32(textBoxEndHour.Text), Convert.ToInt32(textBoxEndMin.Text));
            Coach myCoach = ConnectClassM2.clientProxy.GetCoach(myUsername);
            comboBoxHall.ItemsSource = ConnectClassM2.clientProxy.GetAllHallIds(myCoach.FitnessCenter);

            Term termToSend = new Term(textBoxTitle.Text, comboBoxDay.SelectedItem.ToString(), time, comboBoxHall.SelectedItem.ToString(), (Term.TrainingLevel)comboBoxTrainLvl.SelectedItem, 
                        myUsername, Convert.ToDouble(textBoxPrice.Text));

            if (ConnectClassM2.clientProxy.UpdateTerm(termToSend))
            {
                myDataGrid.ItemsSource = ConnectClassM2.clientProxy.GetTermsByCocachUsername(myUsername);
                myDataGrid.Items.Refresh();
                MessageBox.Show("Term sucessfuly edited.");
                string notification = string.Empty;
                notification = "Term " + termToSend.Title + " has been updated to values: Price " + termToSend.Price + " Day: " + termToSend.Day + " Time range, from " + termToSend.TimeRange.StartingHour + ":"
                    + termToSend.TimeRange.StartingHour + " - " + termToSend.TimeRange.EndingHour + ":" + termToSend.TimeRange.EndingMinute + " Level: " + termToSend.TrainingLvl + " Coach: " + termToSend.ApointedCoach
                    + " Hall: " + termToSend.ApointedHall;
               // ConnectClassM2.clientProxy.GetMembersForGivenTerm(termToSend.Title, notification);
                if (!ConnectClassM2.clientProxy.NotifyMembersForTermChange(termToSend.Title, notification))
                {
                    MessageBox.Show("Failed to push notification for term change to database.");
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("Term edit failed!");
            }
            
        }
    }
}
