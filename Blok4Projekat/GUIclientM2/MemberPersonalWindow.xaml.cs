﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIclientM2
{
    /// <summary>
    /// Interaction logic for MemberPersonalWindow.xaml
    /// </summary>
    public partial class MemberPersonalWindow : Window
    {
        public MemberPersonalWindow()
        {
            InitializeComponent();
        }

        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxName.Text.Equals("") || textBoxLastname.Text.Equals("") || !dateTimePicker.SelectedDate.HasValue || textBoxAddress.Text.Equals(""))
            {
                MessageBox.Show("Please fill the required fields!");
            }
            else
            {
                Member coach = new Member(textBoxUsername.Text, textBoxPassword.Text, textBoxName.Text, textBoxLastname.Text, textBoxAddress.Text, Convert.ToDouble(textBoxHeight.Text), Convert.ToDouble(textBoxWeight.Text), dateTimePicker.SelectedDate.Value);
                if (ConnectClassM2.clientProxy.UpdateMember(coach))
                {
                    MessageBox.Show("Member succesfully updated.");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Member can not be updated.");
                }
            }
        }
    }
}
