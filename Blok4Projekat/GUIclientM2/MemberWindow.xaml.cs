﻿using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIclientM2
{
    /// <summary>
    /// Interaction logic for MemberWindow.xaml
    /// </summary>
    public partial class MemberWindow : Window
    {
        private List<Term> termList = new List<Term>();
        private List<TrainingPackage> trainPackagesList = new List<TrainingPackage>();
        private string myUsername = string.Empty;

        public MemberWindow(string username)
        {
            InitializeComponent();
            myUsername = username;
            List<string> stringNotifications = ConnectClassM2.clientProxy.GetNotificationsForMember(username);
            string toFillTextbox = string.Empty;

            foreach (var notification in stringNotifications)
            {
                toFillTextbox += notification + "\n*********\n";
            }

            if (DateTime.Now.Day == 28)
            {
                var unpaidTerms = ConnectClassM2.clientProxy.GetAllMTRels().Where(
                                                t => t.memberName.Equals(myUsername)
                                                && t.Paid == false
                                                ).Select(u => u.termTitle).ToList();

                var unpaidPackages = ConnectClassM2.clientProxy.GetAllMPRels().Where(
                                                t => t.memberName.Equals(myUsername)
                                                && t.Paid == false
                                                ).Select(u => u.packageName).ToList();

                toFillTextbox += "\nPlease finish your payments for: \n";

                foreach (var item in unpaidTerms)
                {
                    toFillTextbox += item + " \n";
                }

                foreach (var item in unpaidPackages)
                {
                    toFillTextbox += item + " \n";
                }

            }

            textBoxNotif.Text = toFillTextbox;
            comboBoxCoach.ItemsSource = ConnectClassM2.clientProxy.GetAllCoaches();
            comboBoxDay.ItemsSource = new List<string>() { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" };
            comboBoxStartHour.ItemsSource = comboBoxEndHour.ItemsSource = Enumerable.Range(7, 21);
            termList = ConnectClassM2.clientProxy.GetAllTerms();
            listBoxPackages.ItemsSource = ConnectClassM2.clientProxy.GetAllTraininingPackages();
            listBoxOfUnpaidTerms.ItemsSource = ConnectClassM2.clientProxy.GetAllMTRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                && t.Paid == false
                                                ).Select(u => u.termTitle).ToList();
            listBoxOfUnpaidPackages.ItemsSource = ConnectClassM2.clientProxy.GetAllMPRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                && t.Paid == false
                                                ).Select(u => u.packageName).ToList();
            listBoxOfActiveTerms.ItemsSource = ConnectClassM2.clientProxy.GetAllMTRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                && CanGetCanceled(ConnectClassM2.clientProxy.GetTerm(t.termTitle)) == true
                                                ).Select(u => u.termTitle).ToList();
            listBoxOfActivePackages.ItemsSource = ConnectClassM2.clientProxy.GetAllMPRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                ).Select(u => u.packageName).ToList();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (comboBoxCoach.SelectedItem != null && comboBoxDay.SelectedItem != null && comboBoxStartHour.SelectedItem != null && comboBoxEndHour.SelectedItem != null)
            {
                dataGridTerms.Items.Refresh();
                dataGridTerms.ItemsSource = termList.Where(t => t.ApointedCoach.Equals(comboBoxCoach.SelectedItem.ToString())
                                              && t.Day.Equals(comboBoxDay.SelectedItem.ToString())
                                              && t.TimeRange.StartingHour >= Int32.Parse(comboBoxStartHour.SelectedItem.ToString())
                                              && t.TimeRange.EndingHour <= Int32.Parse(comboBoxEndHour.SelectedItem.ToString())).ToList();
            }
        }

        private void buttonEditViewData_Click(object sender, RoutedEventArgs e)
        {
            MemberPersonalWindow memberPerWindow = new MemberPersonalWindow();
            memberPerWindow.Show();

            Member member = ConnectClassM2.clientProxy.GetMember(LoginWindow.Username);
            memberPerWindow.textBoxName.Text = member.FirstName;
            memberPerWindow.textBoxLastname.Text = member.LastName;
            memberPerWindow.textBoxAddress.Text = member.Address;
            memberPerWindow.dateTimePicker.DisplayDate = member.DateOfBirth;
            memberPerWindow.textBoxUsername.Text = member.Username;
            memberPerWindow.textBoxPassword.Text = member.Password;
            memberPerWindow.textBoxWeight.Text = member.Weight.ToString();
            memberPerWindow.textBoxHeight.Text = member.Height.ToString();
        }

        private void buttonChooseTraining_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridTerms.SelectedItem != null)
            {
                Term chosenTerm = new Term();
                chosenTerm = (Term)dataGridTerms.SelectedItem;
                MemberTermRel mtrel = new MemberTermRel();
                mtrel.memberName = LoginWindow.Username;
                mtrel.termTitle = chosenTerm.Title;
                mtrel.Paid = false;
                if (ConnectClassM2.clientProxy.SetMTRel(mtrel))
                {
                    MessageBox.Show("Training successfully added.");
                    listBoxOfActiveTerms.Items.Refresh();
                    listBoxOfActiveTerms.ItemsSource = ConnectClassM2.clientProxy.GetAllMTRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                && CanGetCanceled(ConnectClassM2.clientProxy.GetTerm(t.termTitle)) == true
                                                ).Select(u => u.termTitle).ToList();
                    listBoxOfActivePackages.Items.Refresh();
                    listBoxOfActivePackages.ItemsSource = ConnectClassM2.clientProxy.GetAllMPRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                ).Select(u => u.packageName).ToList();
                    listBoxOfUnpaidTerms.Items.Refresh();
                    listBoxOfUnpaidTerms.ItemsSource = ConnectClassM2.clientProxy.GetAllMTRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                && t.Paid == false
                                                ).Select(u => u.termTitle).ToList();
                    listBoxOfUnpaidPackages.Items.Refresh();
                    listBoxOfUnpaidPackages.ItemsSource = ConnectClassM2.clientProxy.GetAllMPRels().Where(
                                                        t => t.memberName.Equals(LoginWindow.Username)
                                                        && t.Paid == false
                                                        ).Select(u => u.packageName).ToList();
                }
                else
                {
                    MessageBox.Show("Training was not added.");
                }
            }
        }
        private void listBoxPackages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            fillDataGrid();
        }
        private void fillDataGrid()
        {
            dataGridTerms1.Items.Refresh();
            var selected = (TrainingPackage)listBoxPackages.SelectedItem;
            List<string> neededTerms = ConnectClassM2.clientProxy.GetAllPackageTerms(selected.PackageName);
            List<Term> termsToShowList = new List<Term>();

            foreach (var term in termList)
            {
                foreach (var str in neededTerms)
                {
                    if (term.Title.Equals(str))
                    {
                        termsToShowList.Add(term);
                    }
                }
            }
            dataGridTerms1.ItemsSource = termsToShowList;
        }

        private void buttonChoosePackage_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxPackages.SelectedItem != null)
            {
                TrainingPackage chosenPackage = new TrainingPackage();
                chosenPackage = (TrainingPackage)listBoxPackages.SelectedItem;
                MemberPackageRel mprel = new MemberPackageRel();
                mprel.memberName = LoginWindow.Username;
                mprel.packageName = chosenPackage.PackageName;
                mprel.Paid = false;
                if (ConnectClassM2.clientProxy.SetMPRel(mprel))
                {
                    MessageBox.Show("Training successfully added.");
                    listBoxOfActivePackages.Items.Refresh();
                    listBoxOfActivePackages.ItemsSource = ConnectClassM2.clientProxy.GetAllMPRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                ).Select(u => u.packageName).ToList();
                    listBoxOfUnpaidPackages.Items.Refresh();
                    listBoxOfUnpaidPackages.ItemsSource = ConnectClassM2.clientProxy.GetAllMPRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                && t.Paid == false
                                                ).Select(u => u.packageName).ToList();
                }
                else
                {
                    MessageBox.Show("Training was not added.");
                }
            }
        }

        private void buttonPayTraining_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxOfUnpaidTerms.SelectedItem != null)
            {
                if (ConnectClassM2.clientProxy.PayMTRel(LoginWindow.Username, listBoxOfUnpaidTerms.SelectedItem.ToString()))
                {
                    MessageBox.Show("Training successfully paid.");
                    listBoxOfUnpaidTerms.Items.Refresh();
                    listBoxOfUnpaidTerms.ItemsSource = ConnectClassM2.clientProxy.GetAllMTRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                && t.Paid == false
                                                ).Select(u => u.termTitle).ToList();
                }
                else
                {
                    MessageBox.Show("Training was not paid.");
                }
            }

            if (listBoxOfUnpaidPackages.SelectedItem != null)
            {
                if (ConnectClassM2.clientProxy.PayMPRel(LoginWindow.Username, listBoxOfUnpaidPackages.SelectedItem.ToString()))
                {
                    MessageBox.Show("Training successfully paid.");
                    listBoxOfUnpaidPackages.Items.Refresh();
                    listBoxOfUnpaidPackages.ItemsSource = ConnectClassM2.clientProxy.GetAllMPRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                && t.Paid == false
                                                ).Select(u => u.packageName).ToList();
                }
                else
                {
                    MessageBox.Show("Training was not paid.");
                }
            }
        }

        private void buttonCancelTraining_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxOfActiveTerms.SelectedItem != null)
            {
                CanceledTraining canceledTraining = new CanceledTraining();
                canceledTraining.Training = listBoxOfActiveTerms.SelectedItem.ToString();
                List<Coach> coaches = new List<Coach>();
                coaches = ConnectClassM2.clientProxy.GetAllCoaches();
                foreach (var item in coaches)
                {
                    canceledTraining.FitnessCenter = item.FitnessCenter;
                    break;
                }
                if (ConnectClassM2.clientProxy.SetCanceledTraining(canceledTraining))
                {
                    List<MemberTermRel> mtrels = ConnectClassM2.clientProxy.GetAllMTRels();
                    foreach (var item in mtrels)
                    {
                        if (item.memberName.Equals(LoginWindow.Username) && item.termTitle.Equals(canceledTraining.Training))
                        {
                            ConnectClassM2.clientProxy.DeleteMTRel(item);
                            break;
                        }
                    }
                    MessageBox.Show("Training successfully canceled.");
                    listBoxOfActiveTerms.Items.Refresh();
                    listBoxOfActiveTerms.ItemsSource = ConnectClassM2.clientProxy.GetAllMTRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                && CanGetCanceled(ConnectClassM2.clientProxy.GetTerm(t.termTitle)) == true
                                                ).Select(u => u.termTitle).ToList();
                    listBoxOfUnpaidTerms.Items.Refresh();
                    listBoxOfUnpaidTerms.ItemsSource = ConnectClassM2.clientProxy.GetAllMTRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                && t.Paid == false
                                                ).Select(u => u.termTitle).ToList();
                }
                else
                {
                    MessageBox.Show("Training was not canceled.");
                }
            }
            if (listBoxOfActivePackages.SelectedItem != null)
            {
                CanceledTraining canceledTraining = new CanceledTraining();
                canceledTraining.Training = listBoxOfActivePackages.SelectedItem.ToString();
                List<Coach> coaches = new List<Coach>();
                coaches = ConnectClassM2.clientProxy.GetAllCoaches();
                foreach (var item in coaches)
                {
                    canceledTraining.FitnessCenter = item.FitnessCenter;
                    break;
                }
                if (ConnectClassM2.clientProxy.SetCanceledTraining(canceledTraining))
                {
                    List<MemberPackageRel> mprels = ConnectClassM2.clientProxy.GetAllMPRels();
                    foreach (var item in mprels)
                    {
                        if (item.memberName.Equals(LoginWindow.Username) && item.packageName.Equals(canceledTraining.Training))
                        {
                            ConnectClassM2.clientProxy.DeleteMPRel(item);
                            break;
                        }
                    }
                    MessageBox.Show("Training successfully canceled.");
                    listBoxOfActivePackages.Items.Refresh();
                    listBoxOfActivePackages.ItemsSource = ConnectClassM2.clientProxy.GetAllMPRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                ).Select(u => u.packageName).ToList();
                    listBoxOfUnpaidPackages.Items.Refresh();
                    listBoxOfUnpaidPackages.ItemsSource = ConnectClassM2.clientProxy.GetAllMPRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                && t.Paid == false
                                                ).Select(u => u.packageName).ToList();
                }
                else
                {
                    MessageBox.Show("Training was not canceled.");
                }
            }
        }

        private bool CanGetCanceled(Term term)
        {
            int today = 0;
            int termDay = 0;
            DateTime dt = DateTime.Now;

            switch (dt.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    today = -1;
                    break;
                case DayOfWeek.Monday:
                    today = 1;
                    break;
                case DayOfWeek.Tuesday:
                    today = 2;
                    break;
                case DayOfWeek.Wednesday:
                    today = 3;
                    break;
                case DayOfWeek.Thursday:
                    today = 4;
                    break;
                case DayOfWeek.Friday:
                    today = 5;
                    break;
                case DayOfWeek.Saturday:
                    today = -1;
                    break;
                default:
                    break;
            }
            switch (term.Day)
            {
                case "Monday":
                    termDay = 1;
                    break;
                case "Tuesday":
                    termDay = 2;
                    break;
                case "Wednesday":
                    termDay = 3;
                    break;
                case "Thursday":
                    termDay = 4;
                    break;
                case "Friday":
                    termDay = 5;
                    break;
                default:
                    break;
            }
            if (termDay > today)
            {
                return true;
            }
            else if (termDay == today)
            {
                if (term.TimeRange.StartingHour > dt.Hour)
                {
                    return true;
                }
                else if (term.TimeRange.StartingHour == dt.Hour)
                {
                    if (term.TimeRange.StartingMinute > dt.Minute)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        
        private void tabItemHistory_Clicked(object sender, RoutedEventArgs e)
        {
            listBoxOfHistoryTerms.Items.Refresh();
            listBoxOfHistoryTerms.ItemsSource = ConnectClassM2.clientProxy.GetAllMTRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                && CanGetCanceled(ConnectClassM2.clientProxy.GetTerm(t.termTitle)) == false
                                                ).Select(u => u.termTitle).ToList();
            List<string> packages = new List<string>();
            List<List<string>> terms = new List<List<string>>();
            List<string> termToView = new List<string>();
            packages = ConnectClassM2.clientProxy.GetAllMPRels().Where(
                                                t => t.memberName.Equals(LoginWindow.Username)
                                                ).Select(u => u.packageName).ToList();
            foreach (var item in packages)
            {
                terms.Add(ConnectClassM2.clientProxy.GetAllPackageTerms(item));
            }
            foreach (var item in terms)
            {
                foreach (var item1 in item)
                {
                    if (!CanGetCanceled(ConnectClassM2.clientProxy.GetTerm(item1)))
                    {
                        termToView.Add(item1);
                    }
                }
            }
            listBoxOfHistoryPackages.Items.Refresh();
            listBoxOfHistoryPackages.ItemsSource = termToView;
        }

        private void LogoutMember(object sender, RoutedEventArgs e)
        {
            LoginWindow l = new LoginWindow();
            l.Show();
            this.Close();
        }
    }
}
