﻿using CommonModulLib;
using CommonModulLib.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib
{
    [ServiceContract]
    public interface IContract : IModul2Contract
    {
        [OperationContract]
        AuthenticationAndAuthorization Login(string username, string password);

        [OperationContract]
        bool AddNewEfficiencyManager(string username, string password, string firstName, string lastName, string address, string telephone);

        [OperationContract]
        bool AddNewFitnessManager(string username, string password, string firstName, string lastName, string address, string telephone);

        [OperationContract]
        bool AddNewFitnessCenter(string name, string address, string telephone, int numOfHalls, string fcManagerUsername, Time workingTime);

        [OperationContract]
        List<string> GetFitnessCenterManagerUsernames();

        [OperationContract]
        List<FitnessCenter> GetFitnessCenters();

        [OperationContract]
        bool EditFitnessCenter(string name, string address, string telephone, int numOfHalls, string fcManagerUsername);

        [OperationContract]
        List<string> GetFitnessCenterNames();

        [OperationContract]
        bool AddNewHall(string id, TypeOfHall type, string fitnessCenterName);

        [OperationContract]
        List<Hall> GetHallsBySelectedFitnessCenter(string fitnessCenterName);

        [OperationContract]
        bool DeleteHall(string hallID);

        [OperationContract]
        bool EditHall(string id, TypeOfHall hallType, string fitnessCenter);

        [OperationContract]
        List<FitnessCenterManager> GetFitnessCenterManagers();

        [OperationContract]
        bool EditEfficinencyManager(string username, string name, string lastName, string address, string telephone);

        [OperationContract]
        EfficiencyManager GetEfficinencyManagerByUsername(string username);

        [OperationContract]
        List<EfficiencyManager> GetEfficiencyManagers();

        [OperationContract]
        FitnessCenter GetFitnessCenterByName(string name);

    }

    public enum AuthenticationAndAuthorization
    {
        ADMIN = 0,
        EFF_MANAGER = 1,
        NONE = 2
    }

    public enum TypeOfManager
    {
        EfficiencyManager = 0,
        FitnessManager
    }
}
